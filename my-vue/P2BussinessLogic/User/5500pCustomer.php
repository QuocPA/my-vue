<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5500Customer************************             
        // p5500Customer(FullName,Born,Address,Phone)
                                                                                 
        // Get all data from p5500Customer                                      
        case 5500: {                                                              
                $Customer = new CustomerDA();
                $sql = $Customer->CustomerDataAccess("5500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5500Customer                                         
        case 5501: {                                                              
                $Customer = new CustomerDA();
                $sql = $Customer->CustomerDataAccess("5501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5500Customer                                            
        case 5502: {                                                              
                $Customer = new CustomerDA();
                $sql = $Customer->CustomerDataAccess("5502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5500Customer                                         
        case 5503: {                                                              
                $Customer = new CustomerDA();
                $sql = $Customer->CustomerDataAccess("5503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5500Customer                                      
        case 5504: {                                                              
                $Customer = new CustomerDA();
                $sql = $Customer->CustomerDataAccess("5504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5500Customer    
        case 5505: {                                                              
                $Customer = new CustomerDA();
                $sql = $Customer->CustomerDataAccess("5505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5500Customer                                   
        case 5506: {                                                              
                $Customer = new CustomerDA();
                $sql = $Customer->CustomerDataAccess("5506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
