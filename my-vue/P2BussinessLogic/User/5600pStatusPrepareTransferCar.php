<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5600StatusPrepareTransferCar************************             
        // p5600StatusPrepareTransferCar(Name)
                                                                                 
        // Get all data from p5600StatusPrepareTransferCar                                      
        case 5600: {                                                              
                $StatusPrepareTransferCar = new StatusPrepareTransferCarDA();
                $sql = $StatusPrepareTransferCar->StatusPrepareTransferCarDataAccess("5600", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5600StatusPrepareTransferCar                                         
        case 5601: {                                                              
                $StatusPrepareTransferCar = new StatusPrepareTransferCarDA();
                $sql = $StatusPrepareTransferCar->StatusPrepareTransferCarDataAccess("5601", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5600StatusPrepareTransferCar                                            
        case 5602: {                                                              
                $StatusPrepareTransferCar = new StatusPrepareTransferCarDA();
                $sql = $StatusPrepareTransferCar->StatusPrepareTransferCarDataAccess("5602", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5600StatusPrepareTransferCar                                         
        case 5603: {                                                              
                $StatusPrepareTransferCar = new StatusPrepareTransferCarDA();
                $sql = $StatusPrepareTransferCar->StatusPrepareTransferCarDataAccess("5603", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5600StatusPrepareTransferCar                                      
        case 5604: {                                                              
                $StatusPrepareTransferCar = new StatusPrepareTransferCarDA();
                $sql = $StatusPrepareTransferCar->StatusPrepareTransferCarDataAccess("5604", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5600StatusPrepareTransferCar    
        case 5605: {                                                              
                $StatusPrepareTransferCar = new StatusPrepareTransferCarDA();
                $sql = $StatusPrepareTransferCar->StatusPrepareTransferCarDataAccess("5605", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5600StatusPrepareTransferCar                                   
        case 5606: {                                                              
                $StatusPrepareTransferCar = new StatusPrepareTransferCarDA();
                $sql = $StatusPrepareTransferCar->StatusPrepareTransferCarDataAccess("5606", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
