<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6200RegistrationCar************************             
        // p6200RegistrationCar(FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited)
                                                                                 
        // Get all data from p6200RegistrationCar                                      
        case 6200: {                                                              
                $RegistrationCar = new RegistrationCarDA();
                $sql = $RegistrationCar->RegistrationCarDataAccess("6200", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6200RegistrationCar                                         
        case 6201: {                                                              
                $RegistrationCar = new RegistrationCarDA();
                $sql = $RegistrationCar->RegistrationCarDataAccess("6201", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6200RegistrationCar                                            
        case 6202: {                                                              
                $RegistrationCar = new RegistrationCarDA();
                $sql = $RegistrationCar->RegistrationCarDataAccess("6202", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6200RegistrationCar                                         
        case 6203: {                                                              
                $RegistrationCar = new RegistrationCarDA();
                $sql = $RegistrationCar->RegistrationCarDataAccess("6203", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6200RegistrationCar                                      
        case 6204: {                                                              
                $RegistrationCar = new RegistrationCarDA();
                $sql = $RegistrationCar->RegistrationCarDataAccess("6204", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6200RegistrationCar    
        case 6205: {                                                              
                $RegistrationCar = new RegistrationCarDA();
                $sql = $RegistrationCar->RegistrationCarDataAccess("6205", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6200RegistrationCar                                   
        case 6206: {                                                              
                $RegistrationCar = new RegistrationCarDA();
                $sql = $RegistrationCar->RegistrationCarDataAccess("6206", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
