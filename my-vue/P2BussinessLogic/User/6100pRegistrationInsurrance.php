<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6100RegistrationInsurrance************************             
        // p6100RegistrationInsurrance(FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice)
                                                                                 
        // Get all data from p6100RegistrationInsurrance                                      
        case 6100: {                                                              
                $RegistrationInsurrance = new RegistrationInsurranceDA();
                $sql = $RegistrationInsurrance->RegistrationInsurranceDataAccess("6100", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6100RegistrationInsurrance                                         
        case 6101: {                                                              
                $RegistrationInsurrance = new RegistrationInsurranceDA();
                $sql = $RegistrationInsurrance->RegistrationInsurranceDataAccess("6101", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6100RegistrationInsurrance                                            
        case 6102: {                                                              
                $RegistrationInsurrance = new RegistrationInsurranceDA();
                $sql = $RegistrationInsurrance->RegistrationInsurranceDataAccess("6102", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6100RegistrationInsurrance                                         
        case 6103: {                                                              
                $RegistrationInsurrance = new RegistrationInsurranceDA();
                $sql = $RegistrationInsurrance->RegistrationInsurranceDataAccess("6103", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6100RegistrationInsurrance                                      
        case 6104: {                                                              
                $RegistrationInsurrance = new RegistrationInsurranceDA();
                $sql = $RegistrationInsurrance->RegistrationInsurranceDataAccess("6104", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6100RegistrationInsurrance    
        case 6105: {                                                              
                $RegistrationInsurrance = new RegistrationInsurranceDA();
                $sql = $RegistrationInsurrance->RegistrationInsurranceDataAccess("6105", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6100RegistrationInsurrance                                   
        case 6106: {                                                              
                $RegistrationInsurrance = new RegistrationInsurranceDA();
                $sql = $RegistrationInsurrance->RegistrationInsurranceDataAccess("6106", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
