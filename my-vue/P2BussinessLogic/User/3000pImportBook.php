<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3000ImportBook************************             
        // p3000ImportBook(IdBook,Total,CreatedAt)
                                                                                 
        // Get all data from p3000ImportBook                                      
        case 3000: {                                                              
                $ImportBook = new ImportBookDA();
                $sql = $ImportBook->ImportBookDataAccess("3000", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3000ImportBook                                         
        case 3001: {                                                              
                $ImportBook = new ImportBookDA();
                $sql = $ImportBook->ImportBookDataAccess("3001", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3000ImportBook                                            
        case 3002: {                                                              
                $ImportBook = new ImportBookDA();
                $sql = $ImportBook->ImportBookDataAccess("3002", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3000ImportBook                                         
        case 3003: {                                                              
                $ImportBook = new ImportBookDA();
                $sql = $ImportBook->ImportBookDataAccess("3003", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3000ImportBook                                      
        case 3004: {                                                              
                $ImportBook = new ImportBookDA();
                $sql = $ImportBook->ImportBookDataAccess("3004", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3000ImportBook    
        case 3005: {                                                              
                $ImportBook = new ImportBookDA();
                $sql = $ImportBook->ImportBookDataAccess("3005", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3000ImportBook                                   
        case 3006: {                                                              
                $ImportBook = new ImportBookDA();
                $sql = $ImportBook->ImportBookDataAccess("3006", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
