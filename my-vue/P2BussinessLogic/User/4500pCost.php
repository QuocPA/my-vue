<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4500Cost************************             
        // p4500Cost(IdBranch,IdCostCategories,Money,Note,CreatedAt)
                                                                                 
        // Get all data from p4500Cost                                      
        case 4500: {                                                              
                $Cost = new CostDA();
                $sql = $Cost->CostDataAccess("4500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4500Cost                                         
        case 4501: {                                                              
                $Cost = new CostDA();
                $sql = $Cost->CostDataAccess("4501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4500Cost                                            
        case 4502: {                                                              
                $Cost = new CostDA();
                $sql = $Cost->CostDataAccess("4502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4500Cost                                         
        case 4503: {                                                              
                $Cost = new CostDA();
                $sql = $Cost->CostDataAccess("4503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4500Cost                                      
        case 4504: {                                                              
                $Cost = new CostDA();
                $sql = $Cost->CostDataAccess("4504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4500Cost    
        case 4505: {                                                              
                $Cost = new CostDA();
                $sql = $Cost->CostDataAccess("4505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4500Cost                                   
        case 4506: {                                                              
                $Cost = new CostDA();
                $sql = $Cost->CostDataAccess("4506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
