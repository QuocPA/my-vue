<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6400NotificationType************************             
        // p6400NotificationType(Name)
                                                                                 
        // Get all data from p6400NotificationType                                      
        case 6400: {                                                              
                $NotificationType = new NotificationTypeDA();
                $sql = $NotificationType->NotificationTypeDataAccess("6400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6400NotificationType                                         
        case 6401: {                                                              
                $NotificationType = new NotificationTypeDA();
                $sql = $NotificationType->NotificationTypeDataAccess("6401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6400NotificationType                                            
        case 6402: {                                                              
                $NotificationType = new NotificationTypeDA();
                $sql = $NotificationType->NotificationTypeDataAccess("6402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6400NotificationType                                         
        case 6403: {                                                              
                $NotificationType = new NotificationTypeDA();
                $sql = $NotificationType->NotificationTypeDataAccess("6403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6400NotificationType                                      
        case 6404: {                                                              
                $NotificationType = new NotificationTypeDA();
                $sql = $NotificationType->NotificationTypeDataAccess("6404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6400NotificationType    
        case 6405: {                                                              
                $NotificationType = new NotificationTypeDA();
                $sql = $NotificationType->NotificationTypeDataAccess("6405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6400NotificationType                                   
        case 6406: {                                                              
                $NotificationType = new NotificationTypeDA();
                $sql = $NotificationType->NotificationTypeDataAccess("6406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
