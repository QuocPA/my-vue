<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p8300ReviewStudent************************             
        // p8300ReviewStudent(IdRegisterLearn,Rate,Content)
                                                                                 
        // Get all data from p8300ReviewStudent                                      
        case 8300: {                                                              
                $ReviewStudent = new ReviewStudentDA();
                $sql = $ReviewStudent->ReviewStudentDataAccess("8300", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p8300ReviewStudent                                         
        case 8301: {                                                              
                $ReviewStudent = new ReviewStudentDA();
                $sql = $ReviewStudent->ReviewStudentDataAccess("8301", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p8300ReviewStudent                                            
        case 8302: {                                                              
                $ReviewStudent = new ReviewStudentDA();
                $sql = $ReviewStudent->ReviewStudentDataAccess("8302", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p8300ReviewStudent                                         
        case 8303: {                                                              
                $ReviewStudent = new ReviewStudentDA();
                $sql = $ReviewStudent->ReviewStudentDataAccess("8303", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p8300ReviewStudent                                      
        case 8304: {                                                              
                $ReviewStudent = new ReviewStudentDA();
                $sql = $ReviewStudent->ReviewStudentDataAccess("8304", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p8300ReviewStudent    
        case 8305: {                                                              
                $ReviewStudent = new ReviewStudentDA();
                $sql = $ReviewStudent->ReviewStudentDataAccess("8305", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p8300ReviewStudent                                   
        case 8306: {                                                              
                $ReviewStudent = new ReviewStudentDA();
                $sql = $ReviewStudent->ReviewStudentDataAccess("8306", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
