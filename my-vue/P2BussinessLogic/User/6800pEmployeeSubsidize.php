<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6800EmployeeSubsidize************************             
        // p6800EmployeeSubsidize(IdEmployee,IdSubsidize,StartDate,EndDate)
                                                                                 
        // Get all data from p6800EmployeeSubsidize                                      
        case 6800: {                                                              
                $EmployeeSubsidize = new EmployeeSubsidizeDA();
                $sql = $EmployeeSubsidize->EmployeeSubsidizeDataAccess("6800", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6800EmployeeSubsidize                                         
        case 6801: {                                                              
                $EmployeeSubsidize = new EmployeeSubsidizeDA();
                $sql = $EmployeeSubsidize->EmployeeSubsidizeDataAccess("6801", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6800EmployeeSubsidize                                            
        case 6802: {                                                              
                $EmployeeSubsidize = new EmployeeSubsidizeDA();
                $sql = $EmployeeSubsidize->EmployeeSubsidizeDataAccess("6802", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6800EmployeeSubsidize                                         
        case 6803: {                                                              
                $EmployeeSubsidize = new EmployeeSubsidizeDA();
                $sql = $EmployeeSubsidize->EmployeeSubsidizeDataAccess("6803", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6800EmployeeSubsidize                                      
        case 6804: {                                                              
                $EmployeeSubsidize = new EmployeeSubsidizeDA();
                $sql = $EmployeeSubsidize->EmployeeSubsidizeDataAccess("6804", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6800EmployeeSubsidize    
        case 6805: {                                                              
                $EmployeeSubsidize = new EmployeeSubsidizeDA();
                $sql = $EmployeeSubsidize->EmployeeSubsidizeDataAccess("6805", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6800EmployeeSubsidize                                   
        case 6806: {                                                              
                $EmployeeSubsidize = new EmployeeSubsidizeDA();
                $sql = $EmployeeSubsidize->EmployeeSubsidizeDataAccess("6806", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
