<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4800StatusVehicleRegistration************************             
        // p4800StatusVehicleRegistration(Name)
                                                                                 
        // Get all data from p4800StatusVehicleRegistration                                      
        case 4800: {                                                              
                $StatusVehicleRegistration = new StatusVehicleRegistrationDA();
                $sql = $StatusVehicleRegistration->StatusVehicleRegistrationDataAccess("4800", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4800StatusVehicleRegistration                                         
        case 4801: {                                                              
                $StatusVehicleRegistration = new StatusVehicleRegistrationDA();
                $sql = $StatusVehicleRegistration->StatusVehicleRegistrationDataAccess("4801", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4800StatusVehicleRegistration                                            
        case 4802: {                                                              
                $StatusVehicleRegistration = new StatusVehicleRegistrationDA();
                $sql = $StatusVehicleRegistration->StatusVehicleRegistrationDataAccess("4802", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4800StatusVehicleRegistration                                         
        case 4803: {                                                              
                $StatusVehicleRegistration = new StatusVehicleRegistrationDA();
                $sql = $StatusVehicleRegistration->StatusVehicleRegistrationDataAccess("4803", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4800StatusVehicleRegistration                                      
        case 4804: {                                                              
                $StatusVehicleRegistration = new StatusVehicleRegistrationDA();
                $sql = $StatusVehicleRegistration->StatusVehicleRegistrationDataAccess("4804", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4800StatusVehicleRegistration    
        case 4805: {                                                              
                $StatusVehicleRegistration = new StatusVehicleRegistrationDA();
                $sql = $StatusVehicleRegistration->StatusVehicleRegistrationDataAccess("4805", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4800StatusVehicleRegistration                                   
        case 4806: {                                                              
                $StatusVehicleRegistration = new StatusVehicleRegistrationDA();
                $sql = $StatusVehicleRegistration->StatusVehicleRegistrationDataAccess("4806", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
