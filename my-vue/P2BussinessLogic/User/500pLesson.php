<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p500Lesson************************             
        // p500Lesson(Name)
                                                                                 
        // Get all data from p500Lesson                                      
        case 500: {                                                              
                $Lesson = new LessonDA();
                $sql = $Lesson->LessonDataAccess("500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p500Lesson                                         
        case 501: {                                                              
                $Lesson = new LessonDA();
                $sql = $Lesson->LessonDataAccess("501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p500Lesson                                            
        case 502: {                                                              
                $Lesson = new LessonDA();
                $sql = $Lesson->LessonDataAccess("502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p500Lesson                                         
        case 503: {                                                              
                $Lesson = new LessonDA();
                $sql = $Lesson->LessonDataAccess("503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p500Lesson                                      
        case 504: {                                                              
                $Lesson = new LessonDA();
                $sql = $Lesson->LessonDataAccess("504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p500Lesson    
        case 505: {                                                              
                $Lesson = new LessonDA();
                $sql = $Lesson->LessonDataAccess("505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p500Lesson                                   
        case 506: {                                                              
                $Lesson = new LessonDA();
                $sql = $Lesson->LessonDataAccess("506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
