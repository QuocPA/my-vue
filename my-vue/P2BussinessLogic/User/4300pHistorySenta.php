<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4300HistorySenta************************             
        // p4300HistorySenta(IdStudent,CreatedAt)
                                                                                 
        // Get all data from p4300HistorySenta                                      
        case 4300: {                                                              
                $HistorySenta = new HistorySentaDA();
                $sql = $HistorySenta->HistorySentaDataAccess("4300", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4300HistorySenta                                         
        case 4301: {                                                              
                $HistorySenta = new HistorySentaDA();
                $sql = $HistorySenta->HistorySentaDataAccess("4301", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4300HistorySenta                                            
        case 4302: {                                                              
                $HistorySenta = new HistorySentaDA();
                $sql = $HistorySenta->HistorySentaDataAccess("4302", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4300HistorySenta                                         
        case 4303: {                                                              
                $HistorySenta = new HistorySentaDA();
                $sql = $HistorySenta->HistorySentaDataAccess("4303", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4300HistorySenta                                      
        case 4304: {                                                              
                $HistorySenta = new HistorySentaDA();
                $sql = $HistorySenta->HistorySentaDataAccess("4304", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4300HistorySenta    
        case 4305: {                                                              
                $HistorySenta = new HistorySentaDA();
                $sql = $HistorySenta->HistorySentaDataAccess("4305", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4300HistorySenta                                   
        case 4306: {                                                              
                $HistorySenta = new HistorySentaDA();
                $sql = $HistorySenta->HistorySentaDataAccess("4306", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
