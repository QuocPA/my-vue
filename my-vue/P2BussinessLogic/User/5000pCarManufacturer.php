<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5000CarManufacturer************************             
        // p5000CarManufacturer(Name,Logo)
                                                                                 
        // Get all data from p5000CarManufacturer                                      
        case 5000: {                                                              
                $CarManufacturer = new CarManufacturerDA();
                $sql = $CarManufacturer->CarManufacturerDataAccess("5000", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5000CarManufacturer                                         
        case 5001: {                                                              
                $CarManufacturer = new CarManufacturerDA();
                $sql = $CarManufacturer->CarManufacturerDataAccess("5001", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5000CarManufacturer                                            
        case 5002: {                                                              
                $CarManufacturer = new CarManufacturerDA();
                $sql = $CarManufacturer->CarManufacturerDataAccess("5002", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5000CarManufacturer                                         
        case 5003: {                                                              
                $CarManufacturer = new CarManufacturerDA();
                $sql = $CarManufacturer->CarManufacturerDataAccess("5003", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5000CarManufacturer                                      
        case 5004: {                                                              
                $CarManufacturer = new CarManufacturerDA();
                $sql = $CarManufacturer->CarManufacturerDataAccess("5004", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5000CarManufacturer    
        case 5005: {                                                              
                $CarManufacturer = new CarManufacturerDA();
                $sql = $CarManufacturer->CarManufacturerDataAccess("5005", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5000CarManufacturer                                   
        case 5006: {                                                              
                $CarManufacturer = new CarManufacturerDA();
                $sql = $CarManufacturer->CarManufacturerDataAccess("5006", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
