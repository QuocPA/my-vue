<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3300NoticeBoard************************             
        // p3300NoticeBoard(IdTypeNoticeBoard,Name,Image,Description,Position)
                                                                                 
        // Get all data from p3300NoticeBoard                                      
        case 3300: {                                                              
                $NoticeBoard = new NoticeBoardDA();
                $sql = $NoticeBoard->NoticeBoardDataAccess("3300", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3300NoticeBoard                                         
        case 3301: {                                                              
                $NoticeBoard = new NoticeBoardDA();
                $sql = $NoticeBoard->NoticeBoardDataAccess("3301", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3300NoticeBoard                                            
        case 3302: {                                                              
                $NoticeBoard = new NoticeBoardDA();
                $sql = $NoticeBoard->NoticeBoardDataAccess("3302", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3300NoticeBoard                                         
        case 3303: {                                                              
                $NoticeBoard = new NoticeBoardDA();
                $sql = $NoticeBoard->NoticeBoardDataAccess("3303", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3300NoticeBoard                                      
        case 3304: {                                                              
                $NoticeBoard = new NoticeBoardDA();
                $sql = $NoticeBoard->NoticeBoardDataAccess("3304", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3300NoticeBoard    
        case 3305: {                                                              
                $NoticeBoard = new NoticeBoardDA();
                $sql = $NoticeBoard->NoticeBoardDataAccess("3305", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3300NoticeBoard                                   
        case 3306: {                                                              
                $NoticeBoard = new NoticeBoardDA();
                $sql = $NoticeBoard->NoticeBoardDataAccess("3306", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
