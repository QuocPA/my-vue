<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1300TypePackage************************             
        // p1300TypePackage(Name)
                                                                                 
        // Get all data from p1300TypePackage                                      
        case 1300: {                                                              
                $TypePackage = new TypePackageDA();
                $sql = $TypePackage->TypePackageDataAccess("1300", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1300TypePackage                                         
        case 1301: {                                                              
                $TypePackage = new TypePackageDA();
                $sql = $TypePackage->TypePackageDataAccess("1301", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1300TypePackage                                            
        case 1302: {                                                              
                $TypePackage = new TypePackageDA();
                $sql = $TypePackage->TypePackageDataAccess("1302", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1300TypePackage                                         
        case 1303: {                                                              
                $TypePackage = new TypePackageDA();
                $sql = $TypePackage->TypePackageDataAccess("1303", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1300TypePackage                                      
        case 1304: {                                                              
                $TypePackage = new TypePackageDA();
                $sql = $TypePackage->TypePackageDataAccess("1304", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1300TypePackage    
        case 1305: {                                                              
                $TypePackage = new TypePackageDA();
                $sql = $TypePackage->TypePackageDataAccess("1305", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1300TypePackage                                   
        case 1306: {                                                              
                $TypePackage = new TypePackageDA();
                $sql = $TypePackage->TypePackageDataAccess("1306", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
