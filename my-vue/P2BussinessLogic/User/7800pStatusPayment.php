<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7800StatusPayment************************             
        // p7800StatusPayment(Name)
                                                                                 
        // Get all data from p7800StatusPayment                                      
        case 7800: {                                                              
                $StatusPayment = new StatusPaymentDA();
                $sql = $StatusPayment->StatusPaymentDataAccess("7800", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7800StatusPayment                                         
        case 7801: {                                                              
                $StatusPayment = new StatusPaymentDA();
                $sql = $StatusPayment->StatusPaymentDataAccess("7801", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7800StatusPayment                                            
        case 7802: {                                                              
                $StatusPayment = new StatusPaymentDA();
                $sql = $StatusPayment->StatusPaymentDataAccess("7802", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7800StatusPayment                                         
        case 7803: {                                                              
                $StatusPayment = new StatusPaymentDA();
                $sql = $StatusPayment->StatusPaymentDataAccess("7803", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7800StatusPayment                                      
        case 7804: {                                                              
                $StatusPayment = new StatusPaymentDA();
                $sql = $StatusPayment->StatusPaymentDataAccess("7804", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7800StatusPayment    
        case 7805: {                                                              
                $StatusPayment = new StatusPaymentDA();
                $sql = $StatusPayment->StatusPaymentDataAccess("7805", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7800StatusPayment                                   
        case 7806: {                                                              
                $StatusPayment = new StatusPaymentDA();
                $sql = $StatusPayment->StatusPaymentDataAccess("7806", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
