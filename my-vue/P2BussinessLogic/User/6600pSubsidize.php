<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6600Subsidize************************             
        // p6600Subsidize(Name,Money)
                                                                                 
        // Get all data from p6600Subsidize                                      
        case 6600: {                                                              
                $Subsidize = new SubsidizeDA();
                $sql = $Subsidize->SubsidizeDataAccess("6600", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6600Subsidize                                         
        case 6601: {                                                              
                $Subsidize = new SubsidizeDA();
                $sql = $Subsidize->SubsidizeDataAccess("6601", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6600Subsidize                                            
        case 6602: {                                                              
                $Subsidize = new SubsidizeDA();
                $sql = $Subsidize->SubsidizeDataAccess("6602", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6600Subsidize                                         
        case 6603: {                                                              
                $Subsidize = new SubsidizeDA();
                $sql = $Subsidize->SubsidizeDataAccess("6603", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6600Subsidize                                      
        case 6604: {                                                              
                $Subsidize = new SubsidizeDA();
                $sql = $Subsidize->SubsidizeDataAccess("6604", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6600Subsidize    
        case 6605: {                                                              
                $Subsidize = new SubsidizeDA();
                $sql = $Subsidize->SubsidizeDataAccess("6605", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6600Subsidize                                   
        case 6606: {                                                              
                $Subsidize = new SubsidizeDA();
                $sql = $Subsidize->SubsidizeDataAccess("6606", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
