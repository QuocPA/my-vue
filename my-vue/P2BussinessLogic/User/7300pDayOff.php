<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7300DayOff************************             
        // p7300DayOff(IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt)
                                                                                 
        // Get all data from p7300DayOff                                      
        case 7300: {                                                              
                $DayOff = new DayOffDA();
                $sql = $DayOff->DayOffDataAccess("7300", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7300DayOff                                         
        case 7301: {                                                              
                $DayOff = new DayOffDA();
                $sql = $DayOff->DayOffDataAccess("7301", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7300DayOff                                            
        case 7302: {                                                              
                $DayOff = new DayOffDA();
                $sql = $DayOff->DayOffDataAccess("7302", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7300DayOff                                         
        case 7303: {                                                              
                $DayOff = new DayOffDA();
                $sql = $DayOff->DayOffDataAccess("7303", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7300DayOff                                      
        case 7304: {                                                              
                $DayOff = new DayOffDA();
                $sql = $DayOff->DayOffDataAccess("7304", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7300DayOff    
        case 7305: {                                                              
                $DayOff = new DayOffDA();
                $sql = $DayOff->DayOffDataAccess("7305", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7300DayOff                                   
        case 7306: {                                                              
                $DayOff = new DayOffDA();
                $sql = $DayOff->DayOffDataAccess("7306", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
