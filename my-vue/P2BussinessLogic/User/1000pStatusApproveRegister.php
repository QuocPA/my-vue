<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1000StatusApproveRegister************************             
        // p1000StatusApproveRegister(Name)
                                                                                 
        // Get all data from p1000StatusApproveRegister                                      
        case 1000: {                                                              
                $StatusApproveRegister = new StatusApproveRegisterDA();
                $sql = $StatusApproveRegister->StatusApproveRegisterDataAccess("1000", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1000StatusApproveRegister                                         
        case 1001: {                                                              
                $StatusApproveRegister = new StatusApproveRegisterDA();
                $sql = $StatusApproveRegister->StatusApproveRegisterDataAccess("1001", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1000StatusApproveRegister                                            
        case 1002: {                                                              
                $StatusApproveRegister = new StatusApproveRegisterDA();
                $sql = $StatusApproveRegister->StatusApproveRegisterDataAccess("1002", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1000StatusApproveRegister                                         
        case 1003: {                                                              
                $StatusApproveRegister = new StatusApproveRegisterDA();
                $sql = $StatusApproveRegister->StatusApproveRegisterDataAccess("1003", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1000StatusApproveRegister                                      
        case 1004: {                                                              
                $StatusApproveRegister = new StatusApproveRegisterDA();
                $sql = $StatusApproveRegister->StatusApproveRegisterDataAccess("1004", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1000StatusApproveRegister    
        case 1005: {                                                              
                $StatusApproveRegister = new StatusApproveRegisterDA();
                $sql = $StatusApproveRegister->StatusApproveRegisterDataAccess("1005", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1000StatusApproveRegister                                   
        case 1006: {                                                              
                $StatusApproveRegister = new StatusApproveRegisterDA();
                $sql = $StatusApproveRegister->StatusApproveRegisterDataAccess("1006", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
