<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1900TypeSource************************             
        // p1900TypeSource(Name)
                                                                                 
        // Get all data from p1900TypeSource                                      
        case 1900: {                                                              
                $TypeSource = new TypeSourceDA();
                $sql = $TypeSource->TypeSourceDataAccess("1900", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1900TypeSource                                         
        case 1901: {                                                              
                $TypeSource = new TypeSourceDA();
                $sql = $TypeSource->TypeSourceDataAccess("1901", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1900TypeSource                                            
        case 1902: {                                                              
                $TypeSource = new TypeSourceDA();
                $sql = $TypeSource->TypeSourceDataAccess("1902", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1900TypeSource                                         
        case 1903: {                                                              
                $TypeSource = new TypeSourceDA();
                $sql = $TypeSource->TypeSourceDataAccess("1903", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1900TypeSource                                      
        case 1904: {                                                              
                $TypeSource = new TypeSourceDA();
                $sql = $TypeSource->TypeSourceDataAccess("1904", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1900TypeSource    
        case 1905: {                                                              
                $TypeSource = new TypeSourceDA();
                $sql = $TypeSource->TypeSourceDataAccess("1905", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1900TypeSource                                   
        case 1906: {                                                              
                $TypeSource = new TypeSourceDA();
                $sql = $TypeSource->TypeSourceDataAccess("1906", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
