<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4100Question************************             
        // p4100Question(IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain)
                                                                                 
        // Get all data from p4100Question                                      
        case 4100: {                                                              
                $Question = new QuestionDA();
                $sql = $Question->QuestionDataAccess("4100", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4100Question                                         
        case 4101: {                                                              
                $Question = new QuestionDA();
                $sql = $Question->QuestionDataAccess("4101", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4100Question                                            
        case 4102: {                                                              
                $Question = new QuestionDA();
                $sql = $Question->QuestionDataAccess("4102", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4100Question                                         
        case 4103: {                                                              
                $Question = new QuestionDA();
                $sql = $Question->QuestionDataAccess("4103", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4100Question                                      
        case 4104: {                                                              
                $Question = new QuestionDA();
                $sql = $Question->QuestionDataAccess("4104", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4100Question    
        case 4105: {                                                              
                $Question = new QuestionDA();
                $sql = $Question->QuestionDataAccess("4105", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4100Question                                   
        case 4106: {                                                              
                $Question = new QuestionDA();
                $sql = $Question->QuestionDataAccess("4106", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
