<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1600RegisterPackage************************             
        // p1600RegisterPackage(IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice)
                                                                                 
        // Get all data from p1600RegisterPackage                                      
        case 1600: {                                                              
                $RegisterPackage = new RegisterPackageDA();
                $sql = $RegisterPackage->RegisterPackageDataAccess("1600", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1600RegisterPackage                                         
        case 1601: {                                                              
                $RegisterPackage = new RegisterPackageDA();
                $sql = $RegisterPackage->RegisterPackageDataAccess("1601", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1600RegisterPackage                                            
        case 1602: {                                                              
                $RegisterPackage = new RegisterPackageDA();
                $sql = $RegisterPackage->RegisterPackageDataAccess("1602", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1600RegisterPackage                                         
        case 1603: {                                                              
                $RegisterPackage = new RegisterPackageDA();
                $sql = $RegisterPackage->RegisterPackageDataAccess("1603", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1600RegisterPackage                                      
        case 1604: {                                                              
                $RegisterPackage = new RegisterPackageDA();
                $sql = $RegisterPackage->RegisterPackageDataAccess("1604", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1600RegisterPackage    
        case 1605: {                                                              
                $RegisterPackage = new RegisterPackageDA();
                $sql = $RegisterPackage->RegisterPackageDataAccess("1605", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1600RegisterPackage                                   
        case 1606: {                                                              
                $RegisterPackage = new RegisterPackageDA();
                $sql = $RegisterPackage->RegisterPackageDataAccess("1606", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
