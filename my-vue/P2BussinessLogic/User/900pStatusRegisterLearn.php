<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p900StatusRegisterLearn************************             
        // p900StatusRegisterLearn(Name)
                                                                                 
        // Get all data from p900StatusRegisterLearn                                      
        case 900: {                                                              
                $StatusRegisterLearn = new StatusRegisterLearnDA();
                $sql = $StatusRegisterLearn->StatusRegisterLearnDataAccess("900", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p900StatusRegisterLearn                                         
        case 901: {                                                              
                $StatusRegisterLearn = new StatusRegisterLearnDA();
                $sql = $StatusRegisterLearn->StatusRegisterLearnDataAccess("901", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p900StatusRegisterLearn                                            
        case 902: {                                                              
                $StatusRegisterLearn = new StatusRegisterLearnDA();
                $sql = $StatusRegisterLearn->StatusRegisterLearnDataAccess("902", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p900StatusRegisterLearn                                         
        case 903: {                                                              
                $StatusRegisterLearn = new StatusRegisterLearnDA();
                $sql = $StatusRegisterLearn->StatusRegisterLearnDataAccess("903", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p900StatusRegisterLearn                                      
        case 904: {                                                              
                $StatusRegisterLearn = new StatusRegisterLearnDA();
                $sql = $StatusRegisterLearn->StatusRegisterLearnDataAccess("904", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p900StatusRegisterLearn    
        case 905: {                                                              
                $StatusRegisterLearn = new StatusRegisterLearnDA();
                $sql = $StatusRegisterLearn->StatusRegisterLearnDataAccess("905", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p900StatusRegisterLearn                                   
        case 906: {                                                              
                $StatusRegisterLearn = new StatusRegisterLearnDA();
                $sql = $StatusRegisterLearn->StatusRegisterLearnDataAccess("906", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
