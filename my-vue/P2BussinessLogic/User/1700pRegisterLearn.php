<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1700RegisterLearn************************             
        // p1700RegisterLearn(IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt)
                                                                                 
        // Get all data from p1700RegisterLearn                                      
        case 1700: {                                                              
                $RegisterLearn = new RegisterLearnDA();
                $sql = $RegisterLearn->RegisterLearnDataAccess("1700", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1700RegisterLearn                                         
        case 1701: {                                                              
                $RegisterLearn = new RegisterLearnDA();
                $sql = $RegisterLearn->RegisterLearnDataAccess("1701", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1700RegisterLearn                                            
        case 1702: {                                                              
                $RegisterLearn = new RegisterLearnDA();
                $sql = $RegisterLearn->RegisterLearnDataAccess("1702", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1700RegisterLearn                                         
        case 1703: {                                                              
                $RegisterLearn = new RegisterLearnDA();
                $sql = $RegisterLearn->RegisterLearnDataAccess("1703", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1700RegisterLearn                                      
        case 1704: {                                                              
                $RegisterLearn = new RegisterLearnDA();
                $sql = $RegisterLearn->RegisterLearnDataAccess("1704", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1700RegisterLearn    
        case 1705: {                                                              
                $RegisterLearn = new RegisterLearnDA();
                $sql = $RegisterLearn->RegisterLearnDataAccess("1705", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1700RegisterLearn                                   
        case 1706: {                                                              
                $RegisterLearn = new RegisterLearnDA();
                $sql = $RegisterLearn->RegisterLearnDataAccess("1706", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
