<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6700EmployeeReward************************             
        // p6700EmployeeReward(IdEmployee,Name,Money,CreatedAt)
                                                                                 
        // Get all data from p6700EmployeeReward                                      
        case 6700: {                                                              
                $EmployeeReward = new EmployeeRewardDA();
                $sql = $EmployeeReward->EmployeeRewardDataAccess("6700", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6700EmployeeReward                                         
        case 6701: {                                                              
                $EmployeeReward = new EmployeeRewardDA();
                $sql = $EmployeeReward->EmployeeRewardDataAccess("6701", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6700EmployeeReward                                            
        case 6702: {                                                              
                $EmployeeReward = new EmployeeRewardDA();
                $sql = $EmployeeReward->EmployeeRewardDataAccess("6702", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6700EmployeeReward                                         
        case 6703: {                                                              
                $EmployeeReward = new EmployeeRewardDA();
                $sql = $EmployeeReward->EmployeeRewardDataAccess("6703", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6700EmployeeReward                                      
        case 6704: {                                                              
                $EmployeeReward = new EmployeeRewardDA();
                $sql = $EmployeeReward->EmployeeRewardDataAccess("6704", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6700EmployeeReward    
        case 6705: {                                                              
                $EmployeeReward = new EmployeeRewardDA();
                $sql = $EmployeeReward->EmployeeRewardDataAccess("6705", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6700EmployeeReward                                   
        case 6706: {                                                              
                $EmployeeReward = new EmployeeRewardDA();
                $sql = $EmployeeReward->EmployeeRewardDataAccess("6706", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
