<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2600Employee************************             
        // p2600Employee(IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch)
                                                                                 
        // Get all data from p2600Employee                                      
        case 2600: {                                                              
                $Employee = new EmployeeDA();
                $sql = $Employee->EmployeeDataAccess("2600", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2600Employee                                         
        case 2601: {                                                              
                $Employee = new EmployeeDA();
                $sql = $Employee->EmployeeDataAccess("2601", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2600Employee                                            
        case 2602: {                                                              
                $Employee = new EmployeeDA();
                $sql = $Employee->EmployeeDataAccess("2602", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2600Employee                                         
        case 2603: {                                                              
                $Employee = new EmployeeDA();
                $sql = $Employee->EmployeeDataAccess("2603", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2600Employee                                      
        case 2604: {                                                              
                $Employee = new EmployeeDA();
                $sql = $Employee->EmployeeDataAccess("2604", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2600Employee    
        case 2605: {                                                              
                $Employee = new EmployeeDA();
                $sql = $Employee->EmployeeDataAccess("2605", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2600Employee                                   
        case 2606: {                                                              
                $Employee = new EmployeeDA();
                $sql = $Employee->EmployeeDataAccess("2606", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
