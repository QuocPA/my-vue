<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5400Transport************************             
        // p5400Transport(Name)
                                                                                 
        // Get all data from p5400Transport                                      
        case 5400: {                                                              
                $Transport = new TransportDA();
                $sql = $Transport->TransportDataAccess("5400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5400Transport                                         
        case 5401: {                                                              
                $Transport = new TransportDA();
                $sql = $Transport->TransportDataAccess("5401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5400Transport                                            
        case 5402: {                                                              
                $Transport = new TransportDA();
                $sql = $Transport->TransportDataAccess("5402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5400Transport                                         
        case 5403: {                                                              
                $Transport = new TransportDA();
                $sql = $Transport->TransportDataAccess("5403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5400Transport                                      
        case 5404: {                                                              
                $Transport = new TransportDA();
                $sql = $Transport->TransportDataAccess("5404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5400Transport    
        case 5405: {                                                              
                $Transport = new TransportDA();
                $sql = $Transport->TransportDataAccess("5405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5400Transport                                   
        case 5406: {                                                              
                $Transport = new TransportDA();
                $sql = $Transport->TransportDataAccess("5406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
