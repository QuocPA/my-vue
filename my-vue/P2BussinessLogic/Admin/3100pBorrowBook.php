<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3100BorrowBook************************             
        // p3100BorrowBook(IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt)
                                                                                 
        // Get all data from p3100BorrowBook                                      
        case 3100: {                                                              
                $BorrowBook = new BorrowBookDA();
                $sql = $BorrowBook->BorrowBookDataAccess("3100", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3100BorrowBook                                         
        case 3101: {                                                              
                $BorrowBook = new BorrowBookDA();
                $sql = $BorrowBook->BorrowBookDataAccess("3101", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3100BorrowBook                                            
        case 3102: {                                                              
                $BorrowBook = new BorrowBookDA();
                $sql = $BorrowBook->BorrowBookDataAccess("3102", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3100BorrowBook                                         
        case 3103: {                                                              
                $BorrowBook = new BorrowBookDA();
                $sql = $BorrowBook->BorrowBookDataAccess("3103", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3100BorrowBook                                      
        case 3104: {                                                              
                $BorrowBook = new BorrowBookDA();
                $sql = $BorrowBook->BorrowBookDataAccess("3104", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3100BorrowBook    
        case 3105: {                                                              
                $BorrowBook = new BorrowBookDA();
                $sql = $BorrowBook->BorrowBookDataAccess("3105", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3100BorrowBook                                   
        case 3106: {                                                              
                $BorrowBook = new BorrowBookDA();
                $sql = $BorrowBook->BorrowBookDataAccess("3106", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
