<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5100Car************************             
        // p5100Car(IdCarCategories,IdCarManufacturer,NameCar,Color,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost)
                                                                                 
        // Get all data from p5100Car                                      
        case 5100: {                                                              
                $Car = new CarDA();
                $sql = $Car->CarDataAccess("5100", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5100Car                                         
        case 5101: {                                                              
                $Car = new CarDA();
                $sql = $Car->CarDataAccess("5101", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5100Car                                            
        case 5102: {                                                              
                $Car = new CarDA();
                $sql = $Car->CarDataAccess("5102", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5100Car                                         
        case 5103: {                                                              
                $Car = new CarDA();
                $sql = $Car->CarDataAccess("5103", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5100Car                                      
        case 5104: {                                                              
                $Car = new CarDA();
                $sql = $Car->CarDataAccess("5104", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5100Car    
        case 5105: {                                                              
                $Car = new CarDA();
                $sql = $Car->CarDataAccess("5105", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5100Car                                   
        case 5106: {                                                              
                $Car = new CarDA();
                $sql = $Car->CarDataAccess("5106", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
