<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7900StatusTransferCar************************             
        // p7900StatusTransferCar(Name)
                                                                                 
        // Get all data from p7900StatusTransferCar                                      
        case 7900: {                                                              
                $StatusTransferCar = new StatusTransferCarDA();
                $sql = $StatusTransferCar->StatusTransferCarDataAccess("7900", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7900StatusTransferCar                                         
        case 7901: {                                                              
                $StatusTransferCar = new StatusTransferCarDA();
                $sql = $StatusTransferCar->StatusTransferCarDataAccess("7901", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7900StatusTransferCar                                            
        case 7902: {                                                              
                $StatusTransferCar = new StatusTransferCarDA();
                $sql = $StatusTransferCar->StatusTransferCarDataAccess("7902", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7900StatusTransferCar                                         
        case 7903: {                                                              
                $StatusTransferCar = new StatusTransferCarDA();
                $sql = $StatusTransferCar->StatusTransferCarDataAccess("7903", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7900StatusTransferCar                                      
        case 7904: {                                                              
                $StatusTransferCar = new StatusTransferCarDA();
                $sql = $StatusTransferCar->StatusTransferCarDataAccess("7904", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7900StatusTransferCar    
        case 7905: {                                                              
                $StatusTransferCar = new StatusTransferCarDA();
                $sql = $StatusTransferCar->StatusTransferCarDataAccess("7905", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7900StatusTransferCar                                   
        case 7906: {                                                              
                $StatusTransferCar = new StatusTransferCarDA();
                $sql = $StatusTransferCar->StatusTransferCarDataAccess("7906", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
