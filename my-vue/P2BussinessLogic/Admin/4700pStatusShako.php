<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4700StatusShako************************             
        // p4700StatusShako(Name)
                                                                                 
        // Get all data from p4700StatusShako                                      
        case 4700: {                                                              
                $StatusShako = new StatusShakoDA();
                $sql = $StatusShako->StatusShakoDataAccess("4700", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4700StatusShako                                         
        case 4701: {                                                              
                $StatusShako = new StatusShakoDA();
                $sql = $StatusShako->StatusShakoDataAccess("4701", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4700StatusShako                                            
        case 4702: {                                                              
                $StatusShako = new StatusShakoDA();
                $sql = $StatusShako->StatusShakoDataAccess("4702", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4700StatusShako                                         
        case 4703: {                                                              
                $StatusShako = new StatusShakoDA();
                $sql = $StatusShako->StatusShakoDataAccess("4703", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4700StatusShako                                      
        case 4704: {                                                              
                $StatusShako = new StatusShakoDA();
                $sql = $StatusShako->StatusShakoDataAccess("4704", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4700StatusShako    
        case 4705: {                                                              
                $StatusShako = new StatusShakoDA();
                $sql = $StatusShako->StatusShakoDataAccess("4705", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4700StatusShako                                   
        case 4706: {                                                              
                $StatusShako = new StatusShakoDA();
                $sql = $StatusShako->StatusShakoDataAccess("4706", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
