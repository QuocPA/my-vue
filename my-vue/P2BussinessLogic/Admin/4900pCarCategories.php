<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4900CarCategories************************             
        // p4900CarCategories(Name)
                                                                                 
        // Get all data from p4900CarCategories                                      
        case 4900: {                                                              
                $CarCategories = new CarCategoriesDA();
                $sql = $CarCategories->CarCategoriesDataAccess("4900", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4900CarCategories                                         
        case 4901: {                                                              
                $CarCategories = new CarCategoriesDA();
                $sql = $CarCategories->CarCategoriesDataAccess("4901", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4900CarCategories                                            
        case 4902: {                                                              
                $CarCategories = new CarCategoriesDA();
                $sql = $CarCategories->CarCategoriesDataAccess("4902", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4900CarCategories                                         
        case 4903: {                                                              
                $CarCategories = new CarCategoriesDA();
                $sql = $CarCategories->CarCategoriesDataAccess("4903", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4900CarCategories                                      
        case 4904: {                                                              
                $CarCategories = new CarCategoriesDA();
                $sql = $CarCategories->CarCategoriesDataAccess("4904", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4900CarCategories    
        case 4905: {                                                              
                $CarCategories = new CarCategoriesDA();
                $sql = $CarCategories->CarCategoriesDataAccess("4905", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4900CarCategories                                   
        case 4906: {                                                              
                $CarCategories = new CarCategoriesDA();
                $sql = $CarCategories->CarCategoriesDataAccess("4906", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
