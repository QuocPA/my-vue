<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5300SourceCar************************             
        // p5300SourceCar(Name)
                                                                                 
        // Get all data from p5300SourceCar                                      
        case 5300: {                                                              
                $SourceCar = new SourceCarDA();
                $sql = $SourceCar->SourceCarDataAccess("5300", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5300SourceCar                                         
        case 5301: {                                                              
                $SourceCar = new SourceCarDA();
                $sql = $SourceCar->SourceCarDataAccess("5301", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5300SourceCar                                            
        case 5302: {                                                              
                $SourceCar = new SourceCarDA();
                $sql = $SourceCar->SourceCarDataAccess("5302", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5300SourceCar                                         
        case 5303: {                                                              
                $SourceCar = new SourceCarDA();
                $sql = $SourceCar->SourceCarDataAccess("5303", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5300SourceCar                                      
        case 5304: {                                                              
                $SourceCar = new SourceCarDA();
                $sql = $SourceCar->SourceCarDataAccess("5304", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5300SourceCar    
        case 5305: {                                                              
                $SourceCar = new SourceCarDA();
                $sql = $SourceCar->SourceCarDataAccess("5305", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5300SourceCar                                   
        case 5306: {                                                              
                $SourceCar = new SourceCarDA();
                $sql = $SourceCar->SourceCarDataAccess("5306", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
