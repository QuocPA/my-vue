<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3800TypeQuestion************************             
        // p3800TypeQuestion(Name)
                                                                                 
        // Get all data from p3800TypeQuestion                                      
        case 3800: {                                                              
                $TypeQuestion = new TypeQuestionDA();
                $sql = $TypeQuestion->TypeQuestionDataAccess("3800", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3800TypeQuestion                                         
        case 3801: {                                                              
                $TypeQuestion = new TypeQuestionDA();
                $sql = $TypeQuestion->TypeQuestionDataAccess("3801", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3800TypeQuestion                                            
        case 3802: {                                                              
                $TypeQuestion = new TypeQuestionDA();
                $sql = $TypeQuestion->TypeQuestionDataAccess("3802", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3800TypeQuestion                                         
        case 3803: {                                                              
                $TypeQuestion = new TypeQuestionDA();
                $sql = $TypeQuestion->TypeQuestionDataAccess("3803", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3800TypeQuestion                                      
        case 3804: {                                                              
                $TypeQuestion = new TypeQuestionDA();
                $sql = $TypeQuestion->TypeQuestionDataAccess("3804", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3800TypeQuestion    
        case 3805: {                                                              
                $TypeQuestion = new TypeQuestionDA();
                $sql = $TypeQuestion->TypeQuestionDataAccess("3805", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3800TypeQuestion                                   
        case 3806: {                                                              
                $TypeQuestion = new TypeQuestionDA();
                $sql = $TypeQuestion->TypeQuestionDataAccess("3806", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
