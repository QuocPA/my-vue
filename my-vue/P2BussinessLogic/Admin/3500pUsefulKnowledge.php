<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3500UsefulKnowledge************************             
        // p3500UsefulKnowledge(IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description)
                                                                                 
        // Get all data from p3500UsefulKnowledge                                      
        case 3500: {                                                              
                $UsefulKnowledge = new UsefulKnowledgeDA();
                $sql = $UsefulKnowledge->UsefulKnowledgeDataAccess("3500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3500UsefulKnowledge                                         
        case 3501: {                                                              
                $UsefulKnowledge = new UsefulKnowledgeDA();
                $sql = $UsefulKnowledge->UsefulKnowledgeDataAccess("3501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3500UsefulKnowledge                                            
        case 3502: {                                                              
                $UsefulKnowledge = new UsefulKnowledgeDA();
                $sql = $UsefulKnowledge->UsefulKnowledgeDataAccess("3502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3500UsefulKnowledge                                         
        case 3503: {                                                              
                $UsefulKnowledge = new UsefulKnowledgeDA();
                $sql = $UsefulKnowledge->UsefulKnowledgeDataAccess("3503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3500UsefulKnowledge                                      
        case 3504: {                                                              
                $UsefulKnowledge = new UsefulKnowledgeDA();
                $sql = $UsefulKnowledge->UsefulKnowledgeDataAccess("3504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3500UsefulKnowledge    
        case 3505: {                                                              
                $UsefulKnowledge = new UsefulKnowledgeDA();
                $sql = $UsefulKnowledge->UsefulKnowledgeDataAccess("3505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3500UsefulKnowledge                                   
        case 3506: {                                                              
                $UsefulKnowledge = new UsefulKnowledgeDA();
                $sql = $UsefulKnowledge->UsefulKnowledgeDataAccess("3506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
