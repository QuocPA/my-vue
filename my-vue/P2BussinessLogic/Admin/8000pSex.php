<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p8000Sex************************             
        // p8000Sex(Name)
                                                                                 
        // Get all data from p8000Sex                                      
        case 8000: {                                                              
                $Sex = new SexDA();
                $sql = $Sex->SexDataAccess("8000", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p8000Sex                                         
        case 8001: {                                                              
                $Sex = new SexDA();
                $sql = $Sex->SexDataAccess("8001", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p8000Sex                                            
        case 8002: {                                                              
                $Sex = new SexDA();
                $sql = $Sex->SexDataAccess("8002", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p8000Sex                                         
        case 8003: {                                                              
                $Sex = new SexDA();
                $sql = $Sex->SexDataAccess("8003", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p8000Sex                                      
        case 8004: {                                                              
                $Sex = new SexDA();
                $sql = $Sex->SexDataAccess("8004", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p8000Sex    
        case 8005: {                                                              
                $Sex = new SexDA();
                $sql = $Sex->SexDataAccess("8005", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p8000Sex                                   
        case 8006: {                                                              
                $Sex = new SexDA();
                $sql = $Sex->SexDataAccess("8006", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
