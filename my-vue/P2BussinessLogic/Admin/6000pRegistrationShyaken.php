<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6000RegistrationShyaken************************             
        // p6000RegistrationShyaken(FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice)
                                                                                 
        // Get all data from p6000RegistrationShyaken                                      
        case 6000: {                                                              
                $RegistrationShyaken = new RegistrationShyakenDA();
                $sql = $RegistrationShyaken->RegistrationShyakenDataAccess("6000", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6000RegistrationShyaken                                         
        case 6001: {                                                              
                $RegistrationShyaken = new RegistrationShyakenDA();
                $sql = $RegistrationShyaken->RegistrationShyakenDataAccess("6001", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6000RegistrationShyaken                                            
        case 6002: {                                                              
                $RegistrationShyaken = new RegistrationShyakenDA();
                $sql = $RegistrationShyaken->RegistrationShyakenDataAccess("6002", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6000RegistrationShyaken                                         
        case 6003: {                                                              
                $RegistrationShyaken = new RegistrationShyakenDA();
                $sql = $RegistrationShyaken->RegistrationShyakenDataAccess("6003", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6000RegistrationShyaken                                      
        case 6004: {                                                              
                $RegistrationShyaken = new RegistrationShyakenDA();
                $sql = $RegistrationShyaken->RegistrationShyakenDataAccess("6004", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6000RegistrationShyaken    
        case 6005: {                                                              
                $RegistrationShyaken = new RegistrationShyakenDA();
                $sql = $RegistrationShyaken->RegistrationShyakenDataAccess("6005", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6000RegistrationShyaken                                   
        case 6006: {                                                              
                $RegistrationShyaken = new RegistrationShyakenDA();
                $sql = $RegistrationShyaken->RegistrationShyakenDataAccess("6006", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
