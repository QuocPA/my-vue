<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7600PaymentType************************             
        // p7600PaymentType(Name)
                                                                                 
        // Get all data from p7600PaymentType                                      
        case 7600: {                                                              
                $PaymentType = new PaymentTypeDA();
                $sql = $PaymentType->PaymentTypeDataAccess("7600", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7600PaymentType                                         
        case 7601: {                                                              
                $PaymentType = new PaymentTypeDA();
                $sql = $PaymentType->PaymentTypeDataAccess("7601", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7600PaymentType                                            
        case 7602: {                                                              
                $PaymentType = new PaymentTypeDA();
                $sql = $PaymentType->PaymentTypeDataAccess("7602", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7600PaymentType                                         
        case 7603: {                                                              
                $PaymentType = new PaymentTypeDA();
                $sql = $PaymentType->PaymentTypeDataAccess("7603", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7600PaymentType                                      
        case 7604: {                                                              
                $PaymentType = new PaymentTypeDA();
                $sql = $PaymentType->PaymentTypeDataAccess("7604", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7600PaymentType    
        case 7605: {                                                              
                $PaymentType = new PaymentTypeDA();
                $sql = $PaymentType->PaymentTypeDataAccess("7605", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7600PaymentType                                   
        case 7606: {                                                              
                $PaymentType = new PaymentTypeDA();
                $sql = $PaymentType->PaymentTypeDataAccess("7606", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
