<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1400Package************************             
        // p1400Package(IdTypePackage,Name,PricePackage,NumberPeriod)
                                                                                 
        // Get all data from p1400Package                                      
        case 1400: {                                                              
                $Package = new PackageDA();
                $sql = $Package->PackageDataAccess("1400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1400Package                                         
        case 1401: {                                                              
                $Package = new PackageDA();
                $sql = $Package->PackageDataAccess("1401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1400Package                                            
        case 1402: {                                                              
                $Package = new PackageDA();
                $sql = $Package->PackageDataAccess("1402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1400Package                                         
        case 1403: {                                                              
                $Package = new PackageDA();
                $sql = $Package->PackageDataAccess("1403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1400Package                                      
        case 1404: {                                                              
                $Package = new PackageDA();
                $sql = $Package->PackageDataAccess("1404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1400Package    
        case 1405: {                                                              
                $Package = new PackageDA();
                $sql = $Package->PackageDataAccess("1405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1400Package                                   
        case 1406: {                                                              
                $Package = new PackageDA();
                $sql = $Package->PackageDataAccess("1406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
