<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p700LearnTime************************             
        // p700LearnTime(IdBranch,TimeStart,TimeEnd,Position)
                                                                                 
        // Get all data from p700LearnTime                                      
        case 700: {                                                              
                $LearnTime = new LearnTimeDA();
                $sql = $LearnTime->LearnTimeDataAccess("700", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p700LearnTime                                         
        case 701: {                                                              
                $LearnTime = new LearnTimeDA();
                $sql = $LearnTime->LearnTimeDataAccess("701", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p700LearnTime                                            
        case 702: {                                                              
                $LearnTime = new LearnTimeDA();
                $sql = $LearnTime->LearnTimeDataAccess("702", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p700LearnTime                                         
        case 703: {                                                              
                $LearnTime = new LearnTimeDA();
                $sql = $LearnTime->LearnTimeDataAccess("703", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p700LearnTime                                      
        case 704: {                                                              
                $LearnTime = new LearnTimeDA();
                $sql = $LearnTime->LearnTimeDataAccess("704", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p700LearnTime    
        case 705: {                                                              
                $LearnTime = new LearnTimeDA();
                $sql = $LearnTime->LearnTimeDataAccess("705", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p700LearnTime                                   
        case 706: {                                                              
                $LearnTime = new LearnTimeDA();
                $sql = $LearnTime->LearnTimeDataAccess("706", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
