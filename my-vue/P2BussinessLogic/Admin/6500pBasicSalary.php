<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6500BasicSalary************************             
        // p6500BasicSalary(Name,MoneyBasicInMonth,MoneyInHour)
                                                                                 
        // Get all data from p6500BasicSalary                                      
        case 6500: {                                                              
                $BasicSalary = new BasicSalaryDA();
                $sql = $BasicSalary->BasicSalaryDataAccess("6500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6500BasicSalary                                         
        case 6501: {                                                              
                $BasicSalary = new BasicSalaryDA();
                $sql = $BasicSalary->BasicSalaryDataAccess("6501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6500BasicSalary                                            
        case 6502: {                                                              
                $BasicSalary = new BasicSalaryDA();
                $sql = $BasicSalary->BasicSalaryDataAccess("6502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6500BasicSalary                                         
        case 6503: {                                                              
                $BasicSalary = new BasicSalaryDA();
                $sql = $BasicSalary->BasicSalaryDataAccess("6503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6500BasicSalary                                      
        case 6504: {                                                              
                $BasicSalary = new BasicSalaryDA();
                $sql = $BasicSalary->BasicSalaryDataAccess("6504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6500BasicSalary    
        case 6505: {                                                              
                $BasicSalary = new BasicSalaryDA();
                $sql = $BasicSalary->BasicSalaryDataAccess("6505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6500BasicSalary                                   
        case 6506: {                                                              
                $BasicSalary = new BasicSalaryDA();
                $sql = $BasicSalary->BasicSalaryDataAccess("6506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
