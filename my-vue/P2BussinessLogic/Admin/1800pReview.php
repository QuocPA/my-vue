<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1800Review************************             
        // p1800Review(IdRegisterLearn,Rate,Content)
                                                                                 
        // Get all data from p1800Review                                      
        case 1800: {                                                              
                $Review = new ReviewDA();
                $sql = $Review->ReviewDataAccess("1800", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1800Review                                         
        case 1801: {                                                              
                $Review = new ReviewDA();
                $sql = $Review->ReviewDataAccess("1801", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1800Review                                            
        case 1802: {                                                              
                $Review = new ReviewDA();
                $sql = $Review->ReviewDataAccess("1802", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1800Review                                         
        case 1803: {                                                              
                $Review = new ReviewDA();
                $sql = $Review->ReviewDataAccess("1803", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1800Review                                      
        case 1804: {                                                              
                $Review = new ReviewDA();
                $sql = $Review->ReviewDataAccess("1804", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1800Review    
        case 1805: {                                                              
                $Review = new ReviewDA();
                $sql = $Review->ReviewDataAccess("1805", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1800Review                                   
        case 1806: {                                                              
                $Review = new ReviewDA();
                $sql = $Review->ReviewDataAccess("1806", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
