<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2400Certification************************             
        // p2400Certification(Name,DateCertification)
                                                                                 
        // Get all data from p2400Certification                                      
        case 2400: {                                                              
                $Certification = new CertificationDA();
                $sql = $Certification->CertificationDataAccess("2400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2400Certification                                         
        case 2401: {                                                              
                $Certification = new CertificationDA();
                $sql = $Certification->CertificationDataAccess("2401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2400Certification                                            
        case 2402: {                                                              
                $Certification = new CertificationDA();
                $sql = $Certification->CertificationDataAccess("2402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2400Certification                                         
        case 2403: {                                                              
                $Certification = new CertificationDA();
                $sql = $Certification->CertificationDataAccess("2403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2400Certification                                      
        case 2404: {                                                              
                $Certification = new CertificationDA();
                $sql = $Certification->CertificationDataAccess("2404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2400Certification    
        case 2405: {                                                              
                $Certification = new CertificationDA();
                $sql = $Certification->CertificationDataAccess("2405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2400Certification                                   
        case 2406: {                                                              
                $Certification = new CertificationDA();
                $sql = $Certification->CertificationDataAccess("2406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
