<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2200TypeEmployee************************             
        // p2200TypeEmployee(Name)
                                                                                 
        // Get all data from p2200TypeEmployee                                      
        case 2200: {                                                              
                $TypeEmployee = new TypeEmployeeDA();
                $sql = $TypeEmployee->TypeEmployeeDataAccess("2200", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2200TypeEmployee                                         
        case 2201: {                                                              
                $TypeEmployee = new TypeEmployeeDA();
                $sql = $TypeEmployee->TypeEmployeeDataAccess("2201", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2200TypeEmployee                                            
        case 2202: {                                                              
                $TypeEmployee = new TypeEmployeeDA();
                $sql = $TypeEmployee->TypeEmployeeDataAccess("2202", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2200TypeEmployee                                         
        case 2203: {                                                              
                $TypeEmployee = new TypeEmployeeDA();
                $sql = $TypeEmployee->TypeEmployeeDataAccess("2203", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2200TypeEmployee                                      
        case 2204: {                                                              
                $TypeEmployee = new TypeEmployeeDA();
                $sql = $TypeEmployee->TypeEmployeeDataAccess("2204", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2200TypeEmployee    
        case 2205: {                                                              
                $TypeEmployee = new TypeEmployeeDA();
                $sql = $TypeEmployee->TypeEmployeeDataAccess("2205", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2200TypeEmployee                                   
        case 2206: {                                                              
                $TypeEmployee = new TypeEmployeeDA();
                $sql = $TypeEmployee->TypeEmployeeDataAccess("2206", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
