<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4600StatusChangeCompanny************************             
        // p4600StatusChangeCompanny(Name)
                                                                                 
        // Get all data from p4600StatusChangeCompanny                                      
        case 4600: {                                                              
                $StatusChangeCompanny = new StatusChangeCompannyDA();
                $sql = $StatusChangeCompanny->StatusChangeCompannyDataAccess("4600", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4600StatusChangeCompanny                                         
        case 4601: {                                                              
                $StatusChangeCompanny = new StatusChangeCompannyDA();
                $sql = $StatusChangeCompanny->StatusChangeCompannyDataAccess("4601", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4600StatusChangeCompanny                                            
        case 4602: {                                                              
                $StatusChangeCompanny = new StatusChangeCompannyDA();
                $sql = $StatusChangeCompanny->StatusChangeCompannyDataAccess("4602", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4600StatusChangeCompanny                                         
        case 4603: {                                                              
                $StatusChangeCompanny = new StatusChangeCompannyDA();
                $sql = $StatusChangeCompanny->StatusChangeCompannyDataAccess("4603", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4600StatusChangeCompanny                                      
        case 4604: {                                                              
                $StatusChangeCompanny = new StatusChangeCompannyDA();
                $sql = $StatusChangeCompanny->StatusChangeCompannyDataAccess("4604", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4600StatusChangeCompanny    
        case 4605: {                                                              
                $StatusChangeCompanny = new StatusChangeCompannyDA();
                $sql = $StatusChangeCompanny->StatusChangeCompannyDataAccess("4605", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4600StatusChangeCompanny                                   
        case 4606: {                                                              
                $StatusChangeCompanny = new StatusChangeCompannyDA();
                $sql = $StatusChangeCompanny->StatusChangeCompannyDataAccess("4606", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
