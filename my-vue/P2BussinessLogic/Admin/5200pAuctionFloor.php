<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5200AuctionFloor************************             
        // p5200AuctionFloor(NumberCode,Name)
                                                                                 
        // Get all data from p5200AuctionFloor                                      
        case 5200: {                                                              
                $AuctionFloor = new AuctionFloorDA();
                $sql = $AuctionFloor->AuctionFloorDataAccess("5200", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5200AuctionFloor                                         
        case 5201: {                                                              
                $AuctionFloor = new AuctionFloorDA();
                $sql = $AuctionFloor->AuctionFloorDataAccess("5201", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5200AuctionFloor                                            
        case 5202: {                                                              
                $AuctionFloor = new AuctionFloorDA();
                $sql = $AuctionFloor->AuctionFloorDataAccess("5202", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5200AuctionFloor                                         
        case 5203: {                                                              
                $AuctionFloor = new AuctionFloorDA();
                $sql = $AuctionFloor->AuctionFloorDataAccess("5203", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5200AuctionFloor                                      
        case 5204: {                                                              
                $AuctionFloor = new AuctionFloorDA();
                $sql = $AuctionFloor->AuctionFloorDataAccess("5204", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5200AuctionFloor    
        case 5205: {                                                              
                $AuctionFloor = new AuctionFloorDA();
                $sql = $AuctionFloor->AuctionFloorDataAccess("5205", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5200AuctionFloor                                   
        case 5206: {                                                              
                $AuctionFloor = new AuctionFloorDA();
                $sql = $AuctionFloor->AuctionFloorDataAccess("5206", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
