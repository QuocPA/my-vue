<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3700Video************************             
        // p3700Video(IdVideoCategories,Name,Link)
                                                                                 
        // Get all data from p3700Video                                      
        case 3700: {                                                              
                $Video = new VideoDA();
                $sql = $Video->VideoDataAccess("3700", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3700Video                                         
        case 3701: {                                                              
                $Video = new VideoDA();
                $sql = $Video->VideoDataAccess("3701", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3700Video                                            
        case 3702: {                                                              
                $Video = new VideoDA();
                $sql = $Video->VideoDataAccess("3702", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3700Video                                         
        case 3703: {                                                              
                $Video = new VideoDA();
                $sql = $Video->VideoDataAccess("3703", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3700Video                                      
        case 3704: {                                                              
                $Video = new VideoDA();
                $sql = $Video->VideoDataAccess("3704", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3700Video    
        case 3705: {                                                              
                $Video = new VideoDA();
                $sql = $Video->VideoDataAccess("3705", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3700Video                                   
        case 3706: {                                                              
                $Video = new VideoDA();
                $sql = $Video->VideoDataAccess("3706", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
