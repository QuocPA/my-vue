<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6300Notification************************             
        // p6300Notification(IdNotificationType,Title,Content,Author,DateCreate)
                                                                                 
        // Get all data from p6300Notification                                      
        case 6300: {                                                              
                $Notification = new NotificationDA();
                $sql = $Notification->NotificationDataAccess("6300", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6300Notification                                         
        case 6301: {                                                              
                $Notification = new NotificationDA();
                $sql = $Notification->NotificationDataAccess("6301", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6300Notification                                            
        case 6302: {                                                              
                $Notification = new NotificationDA();
                $sql = $Notification->NotificationDataAccess("6302", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6300Notification                                         
        case 6303: {                                                              
                $Notification = new NotificationDA();
                $sql = $Notification->NotificationDataAccess("6303", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6300Notification                                      
        case 6304: {                                                              
                $Notification = new NotificationDA();
                $sql = $Notification->NotificationDataAccess("6304", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6300Notification    
        case 6305: {                                                              
                $Notification = new NotificationDA();
                $sql = $Notification->NotificationDataAccess("6305", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6300Notification                                   
        case 6306: {                                                              
                $Notification = new NotificationDA();
                $sql = $Notification->NotificationDataAccess("6306", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
