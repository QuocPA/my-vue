<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3600VideoCategories************************             
        // p3600VideoCategories(Name,Image)
                                                                                 
        // Get all data from p3600VideoCategories                                      
        case 3600: {                                                              
                $VideoCategories = new VideoCategoriesDA();
                $sql = $VideoCategories->VideoCategoriesDataAccess("3600", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3600VideoCategories                                         
        case 3601: {                                                              
                $VideoCategories = new VideoCategoriesDA();
                $sql = $VideoCategories->VideoCategoriesDataAccess("3601", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3600VideoCategories                                            
        case 3602: {                                                              
                $VideoCategories = new VideoCategoriesDA();
                $sql = $VideoCategories->VideoCategoriesDataAccess("3602", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3600VideoCategories                                         
        case 3603: {                                                              
                $VideoCategories = new VideoCategoriesDA();
                $sql = $VideoCategories->VideoCategoriesDataAccess("3603", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3600VideoCategories                                      
        case 3604: {                                                              
                $VideoCategories = new VideoCategoriesDA();
                $sql = $VideoCategories->VideoCategoriesDataAccess("3604", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3600VideoCategories    
        case 3605: {                                                              
                $VideoCategories = new VideoCategoriesDA();
                $sql = $VideoCategories->VideoCategoriesDataAccess("3605", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3600VideoCategories                                   
        case 3606: {                                                              
                $VideoCategories = new VideoCategoriesDA();
                $sql = $VideoCategories->VideoCategoriesDataAccess("3606", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
