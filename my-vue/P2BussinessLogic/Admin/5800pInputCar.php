<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5800InputCar************************             
        // p5800InputCar(IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport)
                                                                                 
        // Get all data from p5800InputCar                                      
        case 5800: {                                                              
                $InputCar = new InputCarDA();
                $sql = $InputCar->InputCarDataAccess("5800", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5800InputCar                                         
        case 5801: {                                                              
                $InputCar = new InputCarDA();
                $sql = $InputCar->InputCarDataAccess("5801", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5800InputCar                                            
        case 5802: {                                                              
                $InputCar = new InputCarDA();
                $sql = $InputCar->InputCarDataAccess("5802", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5800InputCar                                         
        case 5803: {                                                              
                $InputCar = new InputCarDA();
                $sql = $InputCar->InputCarDataAccess("5803", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5800InputCar                                      
        case 5804: {                                                              
                $InputCar = new InputCarDA();
                $sql = $InputCar->InputCarDataAccess("5804", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5800InputCar    
        case 5805: {                                                              
                $InputCar = new InputCarDA();
                $sql = $InputCar->InputCarDataAccess("5805", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5800InputCar                                   
        case 5806: {                                                              
                $InputCar = new InputCarDA();
                $sql = $InputCar->InputCarDataAccess("5806", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
