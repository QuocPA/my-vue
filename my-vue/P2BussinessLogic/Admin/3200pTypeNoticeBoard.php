<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3200TypeNoticeBoard************************             
        // p3200TypeNoticeBoard(Name,Image)
                                                                                 
        // Get all data from p3200TypeNoticeBoard                                      
        case 3200: {                                                              
                $TypeNoticeBoard = new TypeNoticeBoardDA();
                $sql = $TypeNoticeBoard->TypeNoticeBoardDataAccess("3200", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3200TypeNoticeBoard                                         
        case 3201: {                                                              
                $TypeNoticeBoard = new TypeNoticeBoardDA();
                $sql = $TypeNoticeBoard->TypeNoticeBoardDataAccess("3201", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3200TypeNoticeBoard                                            
        case 3202: {                                                              
                $TypeNoticeBoard = new TypeNoticeBoardDA();
                $sql = $TypeNoticeBoard->TypeNoticeBoardDataAccess("3202", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3200TypeNoticeBoard                                         
        case 3203: {                                                              
                $TypeNoticeBoard = new TypeNoticeBoardDA();
                $sql = $TypeNoticeBoard->TypeNoticeBoardDataAccess("3203", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3200TypeNoticeBoard                                      
        case 3204: {                                                              
                $TypeNoticeBoard = new TypeNoticeBoardDA();
                $sql = $TypeNoticeBoard->TypeNoticeBoardDataAccess("3204", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3200TypeNoticeBoard    
        case 3205: {                                                              
                $TypeNoticeBoard = new TypeNoticeBoardDA();
                $sql = $TypeNoticeBoard->TypeNoticeBoardDataAccess("3205", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3200TypeNoticeBoard                                   
        case 3206: {                                                              
                $TypeNoticeBoard = new TypeNoticeBoardDA();
                $sql = $TypeNoticeBoard->TypeNoticeBoardDataAccess("3206", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
