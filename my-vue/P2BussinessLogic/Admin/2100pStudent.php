<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2100Student************************             
        // p2100Student(IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy)
                                                                                 
        // Get all data from p2100Student                                      
        case 2100: {                                                              
                $Student = new StudentDA();
                $sql = $Student->StudentDataAccess("2100", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2100Student                                         
        case 2101: {                                                              
                $Student = new StudentDA();
                $sql = $Student->StudentDataAccess("2101", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2100Student                                            
        case 2102: {                                                              
                $Student = new StudentDA();
                $sql = $Student->StudentDataAccess("2102", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2100Student                                         
        case 2103: {                                                              
                $Student = new StudentDA();
                $sql = $Student->StudentDataAccess("2103", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2100Student                                      
        case 2104: {                                                              
                $Student = new StudentDA();
                $sql = $Student->StudentDataAccess("2104", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2100Student    
        case 2105: {                                                              
                $Student = new StudentDA();
                $sql = $Student->StudentDataAccess("2105", $param);
                $sql_get_all = $Student->StudentDataAccess("2100", $param);
                
                $result['data_all'] = $baseQuery->execSQL($sql_get_all);                                                                 
                $result['data_panigation'] = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2100Student                                   
        case 2106: {                                                              
                $Student = new StudentDA();
                $sql = $Student->StudentDataAccess("2106", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
