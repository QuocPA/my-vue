<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p8200DrivingTest************************             
        // p8200DrivingTest(IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note)
                                                                                 
        // Get all data from p8200DrivingTest                                      
        case 8200: {                                                              
                $DrivingTest = new DrivingTestDA();
                $sql = $DrivingTest->DrivingTestDataAccess("8200", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p8200DrivingTest                                         
        case 8201: {                                                              
                $DrivingTest = new DrivingTestDA();
                $sql = $DrivingTest->DrivingTestDataAccess("8201", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p8200DrivingTest                                            
        case 8202: {                                                              
                $DrivingTest = new DrivingTestDA();
                $sql = $DrivingTest->DrivingTestDataAccess("8202", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p8200DrivingTest                                         
        case 8203: {                                                              
                $DrivingTest = new DrivingTestDA();
                $sql = $DrivingTest->DrivingTestDataAccess("8203", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p8200DrivingTest                                      
        case 8204: {                                                              
                $DrivingTest = new DrivingTestDA();
                $sql = $DrivingTest->DrivingTestDataAccess("8204", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p8200DrivingTest    
        case 8205: {                                                              
                $DrivingTest = new DrivingTestDA();
                $sql = $DrivingTest->DrivingTestDataAccess("8205", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p8200DrivingTest                                   
        case 8206: {                                                              
                $DrivingTest = new DrivingTestDA();
                $sql = $DrivingTest->DrivingTestDataAccess("8206", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
