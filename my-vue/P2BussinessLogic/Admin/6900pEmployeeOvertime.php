<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p6900EmployeeOvertime************************             
        // p6900EmployeeOvertime(IdEmployee,NumberHour,CreatedAt)
                                                                                 
        // Get all data from p6900EmployeeOvertime                                      
        case 6900: {                                                              
                $EmployeeOvertime = new EmployeeOvertimeDA();
                $sql = $EmployeeOvertime->EmployeeOvertimeDataAccess("6900", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p6900EmployeeOvertime                                         
        case 6901: {                                                              
                $EmployeeOvertime = new EmployeeOvertimeDA();
                $sql = $EmployeeOvertime->EmployeeOvertimeDataAccess("6901", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p6900EmployeeOvertime                                            
        case 6902: {                                                              
                $EmployeeOvertime = new EmployeeOvertimeDA();
                $sql = $EmployeeOvertime->EmployeeOvertimeDataAccess("6902", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p6900EmployeeOvertime                                         
        case 6903: {                                                              
                $EmployeeOvertime = new EmployeeOvertimeDA();
                $sql = $EmployeeOvertime->EmployeeOvertimeDataAccess("6903", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p6900EmployeeOvertime                                      
        case 6904: {                                                              
                $EmployeeOvertime = new EmployeeOvertimeDA();
                $sql = $EmployeeOvertime->EmployeeOvertimeDataAccess("6904", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p6900EmployeeOvertime    
        case 6905: {                                                              
                $EmployeeOvertime = new EmployeeOvertimeDA();
                $sql = $EmployeeOvertime->EmployeeOvertimeDataAccess("6905", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p6900EmployeeOvertime                                   
        case 6906: {                                                              
                $EmployeeOvertime = new EmployeeOvertimeDA();
                $sql = $EmployeeOvertime->EmployeeOvertimeDataAccess("6906", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
