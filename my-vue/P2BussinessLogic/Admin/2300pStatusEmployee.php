<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2300StatusEmployee************************             
        // p2300StatusEmployee(Name)
                                                                                 
        // Get all data from p2300StatusEmployee                                      
        case 2300: {                                                              
                $StatusEmployee = new StatusEmployeeDA();
                $sql = $StatusEmployee->StatusEmployeeDataAccess("2300", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2300StatusEmployee                                         
        case 2301: {                                                              
                $StatusEmployee = new StatusEmployeeDA();
                $sql = $StatusEmployee->StatusEmployeeDataAccess("2301", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2300StatusEmployee                                            
        case 2302: {                                                              
                $StatusEmployee = new StatusEmployeeDA();
                $sql = $StatusEmployee->StatusEmployeeDataAccess("2302", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2300StatusEmployee                                         
        case 2303: {                                                              
                $StatusEmployee = new StatusEmployeeDA();
                $sql = $StatusEmployee->StatusEmployeeDataAccess("2303", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2300StatusEmployee                                      
        case 2304: {                                                              
                $StatusEmployee = new StatusEmployeeDA();
                $sql = $StatusEmployee->StatusEmployeeDataAccess("2304", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2300StatusEmployee    
        case 2305: {                                                              
                $StatusEmployee = new StatusEmployeeDA();
                $sql = $StatusEmployee->StatusEmployeeDataAccess("2305", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2300StatusEmployee                                   
        case 2306: {                                                              
                $StatusEmployee = new StatusEmployeeDA();
                $sql = $StatusEmployee->StatusEmployeeDataAccess("2306", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
