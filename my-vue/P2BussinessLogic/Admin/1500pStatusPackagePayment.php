<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1500StatusPackagePayment************************             
        // p1500StatusPackagePayment(Name)
                                                                                 
        // Get all data from p1500StatusPackagePayment                                      
        case 1500: {                                                              
                $StatusPackagePayment = new StatusPackagePaymentDA();
                $sql = $StatusPackagePayment->StatusPackagePaymentDataAccess("1500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1500StatusPackagePayment                                         
        case 1501: {                                                              
                $StatusPackagePayment = new StatusPackagePaymentDA();
                $sql = $StatusPackagePayment->StatusPackagePaymentDataAccess("1501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1500StatusPackagePayment                                            
        case 1502: {                                                              
                $StatusPackagePayment = new StatusPackagePaymentDA();
                $sql = $StatusPackagePayment->StatusPackagePaymentDataAccess("1502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1500StatusPackagePayment                                         
        case 1503: {                                                              
                $StatusPackagePayment = new StatusPackagePaymentDA();
                $sql = $StatusPackagePayment->StatusPackagePaymentDataAccess("1503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1500StatusPackagePayment                                      
        case 1504: {                                                              
                $StatusPackagePayment = new StatusPackagePaymentDA();
                $sql = $StatusPackagePayment->StatusPackagePaymentDataAccess("1504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1500StatusPackagePayment    
        case 1505: {                                                              
                $StatusPackagePayment = new StatusPackagePaymentDA();
                $sql = $StatusPackagePayment->StatusPackagePaymentDataAccess("1505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1500StatusPackagePayment                                   
        case 1506: {                                                              
                $StatusPackagePayment = new StatusPackagePaymentDA();
                $sql = $StatusPackagePayment->StatusPackagePaymentDataAccess("1506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
