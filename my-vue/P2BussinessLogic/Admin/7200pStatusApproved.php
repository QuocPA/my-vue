<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7200StatusApproved************************             
        // p7200StatusApproved(Name)
                                                                                 
        // Get all data from p7200StatusApproved                                      
        case 7200: {                                                              
                $StatusApproved = new StatusApprovedDA();
                $sql = $StatusApproved->StatusApprovedDataAccess("7200", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7200StatusApproved                                         
        case 7201: {                                                              
                $StatusApproved = new StatusApprovedDA();
                $sql = $StatusApproved->StatusApprovedDataAccess("7201", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7200StatusApproved                                            
        case 7202: {                                                              
                $StatusApproved = new StatusApprovedDA();
                $sql = $StatusApproved->StatusApprovedDataAccess("7202", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7200StatusApproved                                         
        case 7203: {                                                              
                $StatusApproved = new StatusApprovedDA();
                $sql = $StatusApproved->StatusApprovedDataAccess("7203", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7200StatusApproved                                      
        case 7204: {                                                              
                $StatusApproved = new StatusApprovedDA();
                $sql = $StatusApproved->StatusApprovedDataAccess("7204", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7200StatusApproved    
        case 7205: {                                                              
                $StatusApproved = new StatusApprovedDA();
                $sql = $StatusApproved->StatusApprovedDataAccess("7205", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7200StatusApproved                                   
        case 7206: {                                                              
                $StatusApproved = new StatusApprovedDA();
                $sql = $StatusApproved->StatusApprovedDataAccess("7206", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
