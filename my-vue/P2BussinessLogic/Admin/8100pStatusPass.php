<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p8100StatusPass************************             
        // p8100StatusPass(Name)
                                                                                 
        // Get all data from p8100StatusPass                                      
        case 8100: {                                                              
                $StatusPass = new StatusPassDA();
                $sql = $StatusPass->StatusPassDataAccess("8100", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p8100StatusPass                                         
        case 8101: {                                                              
                $StatusPass = new StatusPassDA();
                $sql = $StatusPass->StatusPassDataAccess("8101", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p8100StatusPass                                            
        case 8102: {                                                              
                $StatusPass = new StatusPassDA();
                $sql = $StatusPass->StatusPassDataAccess("8102", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p8100StatusPass                                         
        case 8103: {                                                              
                $StatusPass = new StatusPassDA();
                $sql = $StatusPass->StatusPassDataAccess("8103", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p8100StatusPass                                      
        case 8104: {                                                              
                $StatusPass = new StatusPassDA();
                $sql = $StatusPass->StatusPassDataAccess("8104", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p8100StatusPass    
        case 8105: {                                                              
                $StatusPass = new StatusPassDA();
                $sql = $StatusPass->StatusPassDataAccess("8105", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p8100StatusPass                                   
        case 8106: {                                                              
                $StatusPass = new StatusPassDA();
                $sql = $StatusPass->StatusPassDataAccess("8106", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
