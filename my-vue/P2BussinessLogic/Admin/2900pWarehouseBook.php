<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2900WarehouseBook************************             
        // p2900WarehouseBook(IdBook,Total)
                                                                                 
        // Get all data from p2900WarehouseBook                                      
        case 2900: {                                                              
                $WarehouseBook = new WarehouseBookDA();
                $sql = $WarehouseBook->WarehouseBookDataAccess("2900", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2900WarehouseBook                                         
        case 2901: {                                                              
                $WarehouseBook = new WarehouseBookDA();
                $sql = $WarehouseBook->WarehouseBookDataAccess("2901", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2900WarehouseBook                                            
        case 2902: {                                                              
                $WarehouseBook = new WarehouseBookDA();
                $sql = $WarehouseBook->WarehouseBookDataAccess("2902", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2900WarehouseBook                                         
        case 2903: {                                                              
                $WarehouseBook = new WarehouseBookDA();
                $sql = $WarehouseBook->WarehouseBookDataAccess("2903", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2900WarehouseBook                                      
        case 2904: {                                                              
                $WarehouseBook = new WarehouseBookDA();
                $sql = $WarehouseBook->WarehouseBookDataAccess("2904", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2900WarehouseBook    
        case 2905: {                                                              
                $WarehouseBook = new WarehouseBookDA();
                $sql = $WarehouseBook->WarehouseBookDataAccess("2905", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2900WarehouseBook                                   
        case 2906: {                                                              
                $WarehouseBook = new WarehouseBookDA();
                $sql = $WarehouseBook->WarehouseBookDataAccess("2906", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
