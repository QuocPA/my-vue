<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p400Branch************************             
        // p400Branch(Name,Address)
                                                                                 
        // Get all data from p400Branch                                      
        case 400: {                                                              
                $Branch = new BranchDA();
                $sql = $Branch->BranchDataAccess("400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p400Branch                                         
        case 401: {                                                              
                $Branch = new BranchDA();
                $sql = $Branch->BranchDataAccess("401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p400Branch                                            
        case 402: {                                                              
                $Branch = new BranchDA();
                $sql = $Branch->BranchDataAccess("402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p400Branch                                         
        case 403: {                                                              
                $Branch = new BranchDA();
                $sql = $Branch->BranchDataAccess("403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p400Branch                                      
        case 404: {                                                              
                $Branch = new BranchDA();
                $sql = $Branch->BranchDataAccess("404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p400Branch    
        case 405: {                                                              
                $Branch = new BranchDA();
                $sql = $Branch->BranchDataAccess("405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p400Branch                                   
        case 406: {                                                              
                $Branch = new BranchDA();
                $sql = $Branch->BranchDataAccess("406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
