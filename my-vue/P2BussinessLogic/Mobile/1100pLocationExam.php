<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1100LocationExam************************             
        // p1100LocationExam(Name)
                                                                                 
        // Get all data from p1100LocationExam                                      
        case 1100: {                                                              
                $LocationExam = new LocationExamDA();
                $sql = $LocationExam->LocationExamDataAccess("1100", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1100LocationExam                                         
        case 1101: {                                                              
                $LocationExam = new LocationExamDA();
                $sql = $LocationExam->LocationExamDataAccess("1101", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1100LocationExam                                            
        case 1102: {                                                              
                $LocationExam = new LocationExamDA();
                $sql = $LocationExam->LocationExamDataAccess("1102", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1100LocationExam                                         
        case 1103: {                                                              
                $LocationExam = new LocationExamDA();
                $sql = $LocationExam->LocationExamDataAccess("1103", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1100LocationExam                                      
        case 1104: {                                                              
                $LocationExam = new LocationExamDA();
                $sql = $LocationExam->LocationExamDataAccess("1104", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1100LocationExam    
        case 1105: {                                                              
                $LocationExam = new LocationExamDA();
                $sql = $LocationExam->LocationExamDataAccess("1105", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1100LocationExam                                   
        case 1106: {                                                              
                $LocationExam = new LocationExamDA();
                $sql = $LocationExam->LocationExamDataAccess("1106", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
