<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7100StatusDayOf************************             
        // p7100StatusDayOf(Name)
                                                                                 
        // Get all data from p7100StatusDayOf                                      
        case 7100: {                                                              
                $StatusDayOf = new StatusDayOfDA();
                $sql = $StatusDayOf->StatusDayOfDataAccess("7100", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7100StatusDayOf                                         
        case 7101: {                                                              
                $StatusDayOf = new StatusDayOfDA();
                $sql = $StatusDayOf->StatusDayOfDataAccess("7101", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7100StatusDayOf                                            
        case 7102: {                                                              
                $StatusDayOf = new StatusDayOfDA();
                $sql = $StatusDayOf->StatusDayOfDataAccess("7102", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7100StatusDayOf                                         
        case 7103: {                                                              
                $StatusDayOf = new StatusDayOfDA();
                $sql = $StatusDayOf->StatusDayOfDataAccess("7103", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7100StatusDayOf                                      
        case 7104: {                                                              
                $StatusDayOf = new StatusDayOfDA();
                $sql = $StatusDayOf->StatusDayOfDataAccess("7104", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7100StatusDayOf    
        case 7105: {                                                              
                $StatusDayOf = new StatusDayOfDA();
                $sql = $StatusDayOf->StatusDayOfDataAccess("7105", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7100StatusDayOf                                   
        case 7106: {                                                              
                $StatusDayOf = new StatusDayOfDA();
                $sql = $StatusDayOf->StatusDayOfDataAccess("7106", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
