<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2500CertificationEmployee************************             
        // p2500CertificationEmployee(IdCertification,IdEmployee)
                                                                                 
        // Get all data from p2500CertificationEmployee                                      
        case 2500: {                                                              
                $CertificationEmployee = new CertificationEmployeeDA();
                $sql = $CertificationEmployee->CertificationEmployeeDataAccess("2500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2500CertificationEmployee                                         
        case 2501: {                                                              
                $CertificationEmployee = new CertificationEmployeeDA();
                $sql = $CertificationEmployee->CertificationEmployeeDataAccess("2501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2500CertificationEmployee                                            
        case 2502: {                                                              
                $CertificationEmployee = new CertificationEmployeeDA();
                $sql = $CertificationEmployee->CertificationEmployeeDataAccess("2502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2500CertificationEmployee                                         
        case 2503: {                                                              
                $CertificationEmployee = new CertificationEmployeeDA();
                $sql = $CertificationEmployee->CertificationEmployeeDataAccess("2503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2500CertificationEmployee                                      
        case 2504: {                                                              
                $CertificationEmployee = new CertificationEmployeeDA();
                $sql = $CertificationEmployee->CertificationEmployeeDataAccess("2504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2500CertificationEmployee    
        case 2505: {                                                              
                $CertificationEmployee = new CertificationEmployeeDA();
                $sql = $CertificationEmployee->CertificationEmployeeDataAccess("2505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2500CertificationEmployee                                   
        case 2506: {                                                              
                $CertificationEmployee = new CertificationEmployeeDA();
                $sql = $CertificationEmployee->CertificationEmployeeDataAccess("2506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
