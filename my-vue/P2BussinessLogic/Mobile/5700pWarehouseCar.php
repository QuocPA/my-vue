<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5700WarehouseCar************************             
        // p5700WarehouseCar(IdCar,Total,CreatedAt)
                                                                                 
        // Get all data from p5700WarehouseCar                                      
        case 5700: {                                                              
                $WarehouseCar = new WarehouseCarDA();
                $sql = $WarehouseCar->WarehouseCarDataAccess("5700", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5700WarehouseCar                                         
        case 5701: {                                                              
                $WarehouseCar = new WarehouseCarDA();
                $sql = $WarehouseCar->WarehouseCarDataAccess("5701", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5700WarehouseCar                                            
        case 5702: {                                                              
                $WarehouseCar = new WarehouseCarDA();
                $sql = $WarehouseCar->WarehouseCarDataAccess("5702", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5700WarehouseCar                                         
        case 5703: {                                                              
                $WarehouseCar = new WarehouseCarDA();
                $sql = $WarehouseCar->WarehouseCarDataAccess("5703", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5700WarehouseCar                                      
        case 5704: {                                                              
                $WarehouseCar = new WarehouseCarDA();
                $sql = $WarehouseCar->WarehouseCarDataAccess("5704", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5700WarehouseCar    
        case 5705: {                                                              
                $WarehouseCar = new WarehouseCarDA();
                $sql = $WarehouseCar->WarehouseCarDataAccess("5705", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5700WarehouseCar                                   
        case 5706: {                                                              
                $WarehouseCar = new WarehouseCarDA();
                $sql = $WarehouseCar->WarehouseCarDataAccess("5706", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
