<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p5900OutputCar************************             
        // p5900OutputCar(IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note)
                                                                                 
        // Get all data from p5900OutputCar                                      
        case 5900: {                                                              
                $OutputCar = new OutputCarDA();
                $sql = $OutputCar->OutputCarDataAccess("5900", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p5900OutputCar                                         
        case 5901: {                                                              
                $OutputCar = new OutputCarDA();
                $sql = $OutputCar->OutputCarDataAccess("5901", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p5900OutputCar                                            
        case 5902: {                                                              
                $OutputCar = new OutputCarDA();
                $sql = $OutputCar->OutputCarDataAccess("5902", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p5900OutputCar                                         
        case 5903: {                                                              
                $OutputCar = new OutputCarDA();
                $sql = $OutputCar->OutputCarDataAccess("5903", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p5900OutputCar                                      
        case 5904: {                                                              
                $OutputCar = new OutputCarDA();
                $sql = $OutputCar->OutputCarDataAccess("5904", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p5900OutputCar    
        case 5905: {                                                              
                $OutputCar = new OutputCarDA();
                $sql = $OutputCar->OutputCarDataAccess("5905", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p5900OutputCar                                   
        case 5906: {                                                              
                $OutputCar = new OutputCarDA();
                $sql = $OutputCar->OutputCarDataAccess("5906", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
