<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2000StatusStudent************************             
        // p2000StatusStudent(Name)
                                                                                 
        // Get all data from p2000StatusStudent                                      
        case 2000: {                                                              
                $StatusStudent = new StatusStudentDA();
                $sql = $StatusStudent->StatusStudentDataAccess("2000", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2000StatusStudent                                         
        case 2001: {                                                              
                $StatusStudent = new StatusStudentDA();
                $sql = $StatusStudent->StatusStudentDataAccess("2001", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2000StatusStudent                                            
        case 2002: {                                                              
                $StatusStudent = new StatusStudentDA();
                $sql = $StatusStudent->StatusStudentDataAccess("2002", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2000StatusStudent                                         
        case 2003: {                                                              
                $StatusStudent = new StatusStudentDA();
                $sql = $StatusStudent->StatusStudentDataAccess("2003", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2000StatusStudent                                      
        case 2004: {                                                              
                $StatusStudent = new StatusStudentDA();
                $sql = $StatusStudent->StatusStudentDataAccess("2004", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2000StatusStudent    
        case 2005: {                                                              
                $StatusStudent = new StatusStudentDA();
                $sql = $StatusStudent->StatusStudentDataAccess("2005", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2000StatusStudent                                   
        case 2006: {                                                              
                $StatusStudent = new StatusStudentDA();
                $sql = $StatusStudent->StatusStudentDataAccess("2006", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
