<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3900Topic************************             
        // p3900Topic(IdTypeQuestion,Name)
                                                                                 
        // Get all data from p3900Topic                                      
        case 3900: {                                                              
                $Topic = new TopicDA();
                $sql = $Topic->TopicDataAccess("3900", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3900Topic                                         
        case 3901: {                                                              
                $Topic = new TopicDA();
                $sql = $Topic->TopicDataAccess("3901", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3900Topic                                            
        case 3902: {                                                              
                $Topic = new TopicDA();
                $sql = $Topic->TopicDataAccess("3902", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3900Topic                                         
        case 3903: {                                                              
                $Topic = new TopicDA();
                $sql = $Topic->TopicDataAccess("3903", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3900Topic                                      
        case 3904: {                                                              
                $Topic = new TopicDA();
                $sql = $Topic->TopicDataAccess("3904", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3900Topic    
        case 3905: {                                                              
                $Topic = new TopicDA();
                $sql = $Topic->TopicDataAccess("3905", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3900Topic                                   
        case 3906: {                                                              
                $Topic = new TopicDA();
                $sql = $Topic->TopicDataAccess("3906", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
