<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p800WaitLocation************************             
        // p800WaitLocation(Name)
                                                                                 
        // Get all data from p800WaitLocation                                      
        case 800: {                                                              
                $WaitLocation = new WaitLocationDA();
                $sql = $WaitLocation->WaitLocationDataAccess("800", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p800WaitLocation                                         
        case 801: {                                                              
                $WaitLocation = new WaitLocationDA();
                $sql = $WaitLocation->WaitLocationDataAccess("801", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p800WaitLocation                                            
        case 802: {                                                              
                $WaitLocation = new WaitLocationDA();
                $sql = $WaitLocation->WaitLocationDataAccess("802", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p800WaitLocation                                         
        case 803: {                                                              
                $WaitLocation = new WaitLocationDA();
                $sql = $WaitLocation->WaitLocationDataAccess("803", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p800WaitLocation                                      
        case 804: {                                                              
                $WaitLocation = new WaitLocationDA();
                $sql = $WaitLocation->WaitLocationDataAccess("804", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p800WaitLocation    
        case 805: {                                                              
                $WaitLocation = new WaitLocationDA();
                $sql = $WaitLocation->WaitLocationDataAccess("805", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p800WaitLocation                                   
        case 806: {                                                              
                $WaitLocation = new WaitLocationDA();
                $sql = $WaitLocation->WaitLocationDataAccess("806", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
