<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2800Book************************             
        // p2800Book(IdBookCategories,Name,Image,Author,Price,CreatedAt)
                                                                                 
        // Get all data from p2800Book                                      
        case 2800: {                                                              
                $Book = new BookDA();
                $sql = $Book->BookDataAccess("2800", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2800Book                                         
        case 2801: {                                                              
                $Book = new BookDA();
                $sql = $Book->BookDataAccess("2801", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2800Book                                            
        case 2802: {                                                              
                $Book = new BookDA();
                $sql = $Book->BookDataAccess("2802", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2800Book                                         
        case 2803: {                                                              
                $Book = new BookDA();
                $sql = $Book->BookDataAccess("2803", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2800Book                                      
        case 2804: {                                                              
                $Book = new BookDA();
                $sql = $Book->BookDataAccess("2804", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2800Book    
        case 2805: {                                                              
                $Book = new BookDA();
                $sql = $Book->BookDataAccess("2805", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2800Book                                   
        case 2806: {                                                              
                $Book = new BookDA();
                $sql = $Book->BookDataAccess("2806", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
