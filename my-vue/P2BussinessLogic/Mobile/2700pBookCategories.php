<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p2700BookCategories************************             
        // p2700BookCategories(Name)
                                                                                 
        // Get all data from p2700BookCategories                                      
        case 2700: {                                                              
                $BookCategories = new BookCategoriesDA();
                $sql = $BookCategories->BookCategoriesDataAccess("2700", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p2700BookCategories                                         
        case 2701: {                                                              
                $BookCategories = new BookCategoriesDA();
                $sql = $BookCategories->BookCategoriesDataAccess("2701", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p2700BookCategories                                            
        case 2702: {                                                              
                $BookCategories = new BookCategoriesDA();
                $sql = $BookCategories->BookCategoriesDataAccess("2702", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p2700BookCategories                                         
        case 2703: {                                                              
                $BookCategories = new BookCategoriesDA();
                $sql = $BookCategories->BookCategoriesDataAccess("2703", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p2700BookCategories                                      
        case 2704: {                                                              
                $BookCategories = new BookCategoriesDA();
                $sql = $BookCategories->BookCategoriesDataAccess("2704", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p2700BookCategories    
        case 2705: {                                                              
                $BookCategories = new BookCategoriesDA();
                $sql = $BookCategories->BookCategoriesDataAccess("2705", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p2700BookCategories                                   
        case 2706: {                                                              
                $BookCategories = new BookCategoriesDA();
                $sql = $BookCategories->BookCategoriesDataAccess("2706", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
