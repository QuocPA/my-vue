<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4000TypeAnswerResult************************             
        // p4000TypeAnswerResult(Name)
                                                                                 
        // Get all data from p4000TypeAnswerResult                                      
        case 4000: {                                                              
                $TypeAnswerResult = new TypeAnswerResultDA();
                $sql = $TypeAnswerResult->TypeAnswerResultDataAccess("4000", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4000TypeAnswerResult                                         
        case 4001: {                                                              
                $TypeAnswerResult = new TypeAnswerResultDA();
                $sql = $TypeAnswerResult->TypeAnswerResultDataAccess("4001", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4000TypeAnswerResult                                            
        case 4002: {                                                              
                $TypeAnswerResult = new TypeAnswerResultDA();
                $sql = $TypeAnswerResult->TypeAnswerResultDataAccess("4002", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4000TypeAnswerResult                                         
        case 4003: {                                                              
                $TypeAnswerResult = new TypeAnswerResultDA();
                $sql = $TypeAnswerResult->TypeAnswerResultDataAccess("4003", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4000TypeAnswerResult                                      
        case 4004: {                                                              
                $TypeAnswerResult = new TypeAnswerResultDA();
                $sql = $TypeAnswerResult->TypeAnswerResultDataAccess("4004", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4000TypeAnswerResult    
        case 4005: {                                                              
                $TypeAnswerResult = new TypeAnswerResultDA();
                $sql = $TypeAnswerResult->TypeAnswerResultDataAccess("4005", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4000TypeAnswerResult                                   
        case 4006: {                                                              
                $TypeAnswerResult = new TypeAnswerResultDA();
                $sql = $TypeAnswerResult->TypeAnswerResultDataAccess("4006", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
