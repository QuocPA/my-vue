<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p600Subject************************             
        // p600Subject(Name)
                                                                                 
        // Get all data from p600Subject                                      
        case 600: {                                                              
                $Subject = new SubjectDA();
                $sql = $Subject->SubjectDataAccess("600", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p600Subject                                         
        case 601: {                                                              
                $Subject = new SubjectDA();
                $sql = $Subject->SubjectDataAccess("601", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p600Subject                                            
        case 602: {                                                              
                $Subject = new SubjectDA();
                $sql = $Subject->SubjectDataAccess("602", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p600Subject                                         
        case 603: {                                                              
                $Subject = new SubjectDA();
                $sql = $Subject->SubjectDataAccess("603", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p600Subject                                      
        case 604: {                                                              
                $Subject = new SubjectDA();
                $sql = $Subject->SubjectDataAccess("604", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p600Subject    
        case 605: {                                                              
                $Subject = new SubjectDA();
                $sql = $Subject->SubjectDataAccess("605", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p600Subject                                   
        case 606: {                                                              
                $Subject = new SubjectDA();
                $sql = $Subject->SubjectDataAccess("606", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
