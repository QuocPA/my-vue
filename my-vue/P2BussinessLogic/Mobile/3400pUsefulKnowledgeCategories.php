<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p3400UsefulKnowledgeCategories************************             
        // p3400UsefulKnowledgeCategories(Name,Image)
                                                                                 
        // Get all data from p3400UsefulKnowledgeCategories                                      
        case 3400: {                                                              
                $UsefulKnowledgeCategories = new UsefulKnowledgeCategoriesDA();
                $sql = $UsefulKnowledgeCategories->UsefulKnowledgeCategoriesDataAccess("3400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p3400UsefulKnowledgeCategories                                         
        case 3401: {                                                              
                $UsefulKnowledgeCategories = new UsefulKnowledgeCategoriesDA();
                $sql = $UsefulKnowledgeCategories->UsefulKnowledgeCategoriesDataAccess("3401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p3400UsefulKnowledgeCategories                                            
        case 3402: {                                                              
                $UsefulKnowledgeCategories = new UsefulKnowledgeCategoriesDA();
                $sql = $UsefulKnowledgeCategories->UsefulKnowledgeCategoriesDataAccess("3402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p3400UsefulKnowledgeCategories                                         
        case 3403: {                                                              
                $UsefulKnowledgeCategories = new UsefulKnowledgeCategoriesDA();
                $sql = $UsefulKnowledgeCategories->UsefulKnowledgeCategoriesDataAccess("3403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p3400UsefulKnowledgeCategories                                      
        case 3404: {                                                              
                $UsefulKnowledgeCategories = new UsefulKnowledgeCategoriesDA();
                $sql = $UsefulKnowledgeCategories->UsefulKnowledgeCategoriesDataAccess("3404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p3400UsefulKnowledgeCategories    
        case 3405: {                                                              
                $UsefulKnowledgeCategories = new UsefulKnowledgeCategoriesDA();
                $sql = $UsefulKnowledgeCategories->UsefulKnowledgeCategoriesDataAccess("3405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p3400UsefulKnowledgeCategories                                   
        case 3406: {                                                              
                $UsefulKnowledgeCategories = new UsefulKnowledgeCategoriesDA();
                $sql = $UsefulKnowledgeCategories->UsefulKnowledgeCategoriesDataAccess("3406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
