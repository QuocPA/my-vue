<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7400StatusTakeImage************************             
        // p7400StatusTakeImage(Name)
                                                                                 
        // Get all data from p7400StatusTakeImage                                      
        case 7400: {                                                              
                $StatusTakeImage = new StatusTakeImageDA();
                $sql = $StatusTakeImage->StatusTakeImageDataAccess("7400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7400StatusTakeImage                                         
        case 7401: {                                                              
                $StatusTakeImage = new StatusTakeImageDA();
                $sql = $StatusTakeImage->StatusTakeImageDataAccess("7401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7400StatusTakeImage                                            
        case 7402: {                                                              
                $StatusTakeImage = new StatusTakeImageDA();
                $sql = $StatusTakeImage->StatusTakeImageDataAccess("7402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7400StatusTakeImage                                         
        case 7403: {                                                              
                $StatusTakeImage = new StatusTakeImageDA();
                $sql = $StatusTakeImage->StatusTakeImageDataAccess("7403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7400StatusTakeImage                                      
        case 7404: {                                                              
                $StatusTakeImage = new StatusTakeImageDA();
                $sql = $StatusTakeImage->StatusTakeImageDataAccess("7404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7400StatusTakeImage    
        case 7405: {                                                              
                $StatusTakeImage = new StatusTakeImageDA();
                $sql = $StatusTakeImage->StatusTakeImageDataAccess("7405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7400StatusTakeImage                                   
        case 7406: {                                                              
                $StatusTakeImage = new StatusTakeImageDA();
                $sql = $StatusTakeImage->StatusTakeImageDataAccess("7406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
