<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7700Payment************************             
        // p7700Payment(Name)
                                                                                 
        // Get all data from p7700Payment                                      
        case 7700: {                                                              
                $Payment = new PaymentDA();
                $sql = $Payment->PaymentDataAccess("7700", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7700Payment                                         
        case 7701: {                                                              
                $Payment = new PaymentDA();
                $sql = $Payment->PaymentDataAccess("7701", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7700Payment                                            
        case 7702: {                                                              
                $Payment = new PaymentDA();
                $sql = $Payment->PaymentDataAccess("7702", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7700Payment                                         
        case 7703: {                                                              
                $Payment = new PaymentDA();
                $sql = $Payment->PaymentDataAccess("7703", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7700Payment                                      
        case 7704: {                                                              
                $Payment = new PaymentDA();
                $sql = $Payment->PaymentDataAccess("7704", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7700Payment    
        case 7705: {                                                              
                $Payment = new PaymentDA();
                $sql = $Payment->PaymentDataAccess("7705", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7700Payment                                   
        case 7706: {                                                              
                $Payment = new PaymentDA();
                $sql = $Payment->PaymentDataAccess("7706", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
