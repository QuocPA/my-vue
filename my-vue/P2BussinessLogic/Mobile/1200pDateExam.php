<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p1200DateExam************************             
        // p1200DateExam(IdLocationExam,StartDate)
                                                                                 
        // Get all data from p1200DateExam                                      
        case 1200: {                                                              
                $DateExam = new DateExamDA();
                $sql = $DateExam->DateExamDataAccess("1200", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p1200DateExam                                         
        case 1201: {                                                              
                $DateExam = new DateExamDA();
                $sql = $DateExam->DateExamDataAccess("1201", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p1200DateExam                                            
        case 1202: {                                                              
                $DateExam = new DateExamDA();
                $sql = $DateExam->DateExamDataAccess("1202", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p1200DateExam                                         
        case 1203: {                                                              
                $DateExam = new DateExamDA();
                $sql = $DateExam->DateExamDataAccess("1203", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p1200DateExam                                      
        case 1204: {                                                              
                $DateExam = new DateExamDA();
                $sql = $DateExam->DateExamDataAccess("1204", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p1200DateExam    
        case 1205: {                                                              
                $DateExam = new DateExamDA();
                $sql = $DateExam->DateExamDataAccess("1205", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p1200DateExam                                   
        case 1206: {                                                              
                $DateExam = new DateExamDA();
                $sql = $DateExam->DateExamDataAccess("1206", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
