<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4200QuestionHistory************************             
        // p4200QuestionHistory(IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent)
                                                                                 
        // Get all data from p4200QuestionHistory                                      
        case 4200: {                                                              
                $QuestionHistory = new QuestionHistoryDA();
                $sql = $QuestionHistory->QuestionHistoryDataAccess("4200", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4200QuestionHistory                                         
        case 4201: {                                                              
                $QuestionHistory = new QuestionHistoryDA();
                $sql = $QuestionHistory->QuestionHistoryDataAccess("4201", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4200QuestionHistory                                            
        case 4202: {                                                              
                $QuestionHistory = new QuestionHistoryDA();
                $sql = $QuestionHistory->QuestionHistoryDataAccess("4202", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4200QuestionHistory                                         
        case 4203: {                                                              
                $QuestionHistory = new QuestionHistoryDA();
                $sql = $QuestionHistory->QuestionHistoryDataAccess("4203", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4200QuestionHistory                                      
        case 4204: {                                                              
                $QuestionHistory = new QuestionHistoryDA();
                $sql = $QuestionHistory->QuestionHistoryDataAccess("4204", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4200QuestionHistory    
        case 4205: {                                                              
                $QuestionHistory = new QuestionHistoryDA();
                $sql = $QuestionHistory->QuestionHistoryDataAccess("4205", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4200QuestionHistory                                   
        case 4206: {                                                              
                $QuestionHistory = new QuestionHistoryDA();
                $sql = $QuestionHistory->QuestionHistoryDataAccess("4206", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
