<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7500StatusPost************************             
        // p7500StatusPost(Name)
                                                                                 
        // Get all data from p7500StatusPost                                      
        case 7500: {                                                              
                $StatusPost = new StatusPostDA();
                $sql = $StatusPost->StatusPostDataAccess("7500", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7500StatusPost                                         
        case 7501: {                                                              
                $StatusPost = new StatusPostDA();
                $sql = $StatusPost->StatusPostDataAccess("7501", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7500StatusPost                                            
        case 7502: {                                                              
                $StatusPost = new StatusPostDA();
                $sql = $StatusPost->StatusPostDataAccess("7502", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7500StatusPost                                         
        case 7503: {                                                              
                $StatusPost = new StatusPostDA();
                $sql = $StatusPost->StatusPostDataAccess("7503", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7500StatusPost                                      
        case 7504: {                                                              
                $StatusPost = new StatusPostDA();
                $sql = $StatusPost->StatusPostDataAccess("7504", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7500StatusPost    
        case 7505: {                                                              
                $StatusPost = new StatusPostDA();
                $sql = $StatusPost->StatusPostDataAccess("7505", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7500StatusPost                                   
        case 7506: {                                                              
                $StatusPost = new StatusPostDA();
                $sql = $StatusPost->StatusPostDataAccess("7506", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
