<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p4400CostCategories************************             
        // p4400CostCategories(Name)
                                                                                 
        // Get all data from p4400CostCategories                                      
        case 4400: {                                                              
                $CostCategories = new CostCategoriesDA();
                $sql = $CostCategories->CostCategoriesDataAccess("4400", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p4400CostCategories                                         
        case 4401: {                                                              
                $CostCategories = new CostCategoriesDA();
                $sql = $CostCategories->CostCategoriesDataAccess("4401", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p4400CostCategories                                            
        case 4402: {                                                              
                $CostCategories = new CostCategoriesDA();
                $sql = $CostCategories->CostCategoriesDataAccess("4402", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p4400CostCategories                                         
        case 4403: {                                                              
                $CostCategories = new CostCategoriesDA();
                $sql = $CostCategories->CostCategoriesDataAccess("4403", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p4400CostCategories                                      
        case 4404: {                                                              
                $CostCategories = new CostCategoriesDA();
                $sql = $CostCategories->CostCategoriesDataAccess("4404", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p4400CostCategories    
        case 4405: {                                                              
                $CostCategories = new CostCategoriesDA();
                $sql = $CostCategories->CostCategoriesDataAccess("4405", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p4400CostCategories                                   
        case 4406: {                                                              
                $CostCategories = new CostCategoriesDA();
                $sql = $CostCategories->CostCategoriesDataAccess("4406", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
