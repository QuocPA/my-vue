<?php                                                                            
// learn php basic: https://www.w3schools.com/php/default.asp                    
                                                                                 
switch ($param->what) {                                                          
        //******************p7000EmployeeBasicSalary************************             
        // p7000EmployeeBasicSalary(IdEmployee,IdBasicSalary,IdTypeEmployee)
                                                                                 
        // Get all data from p7000EmployeeBasicSalary                                      
        case 7000: {                                                              
                $EmployeeBasicSalary = new EmployeeBasicSalaryDA();
                $sql = $EmployeeBasicSalary->EmployeeBasicSalaryDataAccess("7000", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Insert data to p7000EmployeeBasicSalary                                         
        case 7001: {                                                              
                $EmployeeBasicSalary = new EmployeeBasicSalaryDA();
                $sql = $EmployeeBasicSalary->EmployeeBasicSalaryDataAccess("7001", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Update data p7000EmployeeBasicSalary                                            
        case 7002: {                                                              
                $EmployeeBasicSalary = new EmployeeBasicSalaryDA();
                $sql = $EmployeeBasicSalary->EmployeeBasicSalaryDataAccess("7002", $param);               
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Delete data of p7000EmployeeBasicSalary                                         
        case 7003: {                                                              
                $EmployeeBasicSalary = new EmployeeBasicSalaryDA();
                $sql = $EmployeeBasicSalary->EmployeeBasicSalaryDataAccess("7003", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Find data with id p7000EmployeeBasicSalary                                      
        case 7004: {                                                              
                $EmployeeBasicSalary = new EmployeeBasicSalaryDA();
                $sql = $EmployeeBasicSalary->EmployeeBasicSalaryDataAccess("7004", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Select with pagination(offset, number-item-in-page) p7000EmployeeBasicSalary    
        case 7005: {                                                              
                $EmployeeBasicSalary = new EmployeeBasicSalaryDA();
                $sql = $EmployeeBasicSalary->EmployeeBasicSalaryDataAccess("7005", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
        // Count number item of p7000EmployeeBasicSalary                                   
        case 7006: {                                                              
                $EmployeeBasicSalary = new EmployeeBasicSalaryDA();
                $sql = $EmployeeBasicSalary->EmployeeBasicSalaryDataAccess("7006", $param);               
                                                                                 
                $result = $baseQuery->execSQL($sql);                             
                                                                                 
                echo json_encode($result);                                       
                break;                                                           
        }                                                                        
                                                                                 
}                                                                                
