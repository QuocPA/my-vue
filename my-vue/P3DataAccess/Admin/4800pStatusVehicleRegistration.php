<?php                                                                                      
	class StatusVehicleRegistrationDA{				
		public function StatusVehicleRegistrationDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4800StatusVehicleRegistration************************             
                // p4800StatusVehicleRegistration(id,Name)
                // Get all data from p4800StatusVehicleRegistration
                case 4800: {                                                                        
                    return "SELECT * FROM p4800StatusVehicleRegistration";
                }                                                                                  
                                                                                                   
                // Insert data to p4800StatusVehicleRegistration
                case 4801: {                                                                        
                    return "INSERT INTO p4800StatusVehicleRegistration(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p4800StatusVehicleRegistration
                case 4802: {
                    return "UPDATE p4800StatusVehicleRegistration SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4800StatusVehicleRegistration
                case 4803: {                                                                        
                    return "DELETE FROM p4800StatusVehicleRegistration
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4800StatusVehicleRegistration
                case 4804: {                                                                        
                    return "SELECT * FROM p4800StatusVehicleRegistration
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4800StatusVehicleRegistration
                case 4805: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4800StatusVehicleRegistration $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4800StatusVehicleRegistration T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4800StatusVehicleRegistration
                case 4806: {                                                                        
                    return "SELECT COUNT(1) FROM p4800StatusVehicleRegistration $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
