<?php                                                                                      
	class PackageDA{				
		public function PackageDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1400Package************************             
                // p1400Package(id,IdTypePackage,Name,PricePackage,NumberPeriod)
                // Get all data from p1400Package
                case 1400: {                                                                        
                    return "SELECT * FROM p1400Package";
                }                                                                                  
                                                                                                   
                // Insert data to p1400Package
                case 1401: {                                                                        
                    return "INSERT INTO p1400Package(IdTypePackage,Name,PricePackage,NumberPeriod)
                            VALUES('$param->IdTypePackage','$param->Name','$param->PricePackage','$param->NumberPeriod')";                               
                }                                                                                  
                                                                                                   
                // Update data p1400Package
                case 1402: {
                    return "UPDATE p1400Package SET IdTypePackage='$param->IdTypePackage',Name='$param->Name',PricePackage='$param->PricePackage',NumberPeriod='$param->NumberPeriod'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1400Package
                case 1403: {                                                                        
                    return "DELETE FROM p1400Package
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1400Package
                case 1404: {                                                                        
                    return "SELECT * FROM p1400Package
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1400Package
                case 1405: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1400Package $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1400Package T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1400Package
                case 1406: {                                                                        
                    return "SELECT COUNT(1) FROM p1400Package $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
