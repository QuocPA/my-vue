<?php                                                                                      
	class CertificationEmployeeDA{				
		public function CertificationEmployeeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2500CertificationEmployee************************             
                // p2500CertificationEmployee(id,IdCertification,IdEmployee)
                // Get all data from p2500CertificationEmployee
                case 2500: {                                                                        
                    return "SELECT * FROM p2500CertificationEmployee";
                }                                                                                  
                                                                                                   
                // Insert data to p2500CertificationEmployee
                case 2501: {                                                                        
                    return "INSERT INTO p2500CertificationEmployee(IdCertification,IdEmployee)
                            VALUES('$param->IdCertification','$param->IdEmployee')";                               
                }                                                                                  
                                                                                                   
                // Update data p2500CertificationEmployee
                case 2502: {
                    return "UPDATE p2500CertificationEmployee SET IdCertification='$param->IdCertification',IdEmployee='$param->IdEmployee'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2500CertificationEmployee
                case 2503: {                                                                        
                    return "DELETE FROM p2500CertificationEmployee
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2500CertificationEmployee
                case 2504: {                                                                        
                    return "SELECT * FROM p2500CertificationEmployee
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2500CertificationEmployee
                case 2505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2500CertificationEmployee $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2500CertificationEmployee T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2500CertificationEmployee
                case 2506: {                                                                        
                    return "SELECT COUNT(1) FROM p2500CertificationEmployee $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
