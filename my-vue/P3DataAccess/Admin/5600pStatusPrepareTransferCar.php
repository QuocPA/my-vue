<?php                                                                                      
	class StatusPrepareTransferCarDA{				
		public function StatusPrepareTransferCarDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5600StatusPrepareTransferCar************************             
                // p5600StatusPrepareTransferCar(id,Name)
                // Get all data from p5600StatusPrepareTransferCar
                case 5600: {                                                                        
                    return "SELECT * FROM p5600StatusPrepareTransferCar";
                }                                                                                  
                                                                                                   
                // Insert data to p5600StatusPrepareTransferCar
                case 5601: {                                                                        
                    return "INSERT INTO p5600StatusPrepareTransferCar(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p5600StatusPrepareTransferCar
                case 5602: {
                    return "UPDATE p5600StatusPrepareTransferCar SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5600StatusPrepareTransferCar
                case 5603: {                                                                        
                    return "DELETE FROM p5600StatusPrepareTransferCar
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5600StatusPrepareTransferCar
                case 5604: {                                                                        
                    return "SELECT * FROM p5600StatusPrepareTransferCar
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5600StatusPrepareTransferCar
                case 5605: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5600StatusPrepareTransferCar $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5600StatusPrepareTransferCar T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5600StatusPrepareTransferCar
                case 5606: {                                                                        
                    return "SELECT COUNT(1) FROM p5600StatusPrepareTransferCar $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
