<?php                                                                                      
	class StatusPostDA{				
		public function StatusPostDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7500StatusPost************************             
                // p7500StatusPost(id,Name)
                // Get all data from p7500StatusPost
                case 7500: {                                                                        
                    return "SELECT * FROM p7500StatusPost";
                }                                                                                  
                                                                                                   
                // Insert data to p7500StatusPost
                case 7501: {                                                                        
                    return "INSERT INTO p7500StatusPost(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p7500StatusPost
                case 7502: {
                    return "UPDATE p7500StatusPost SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7500StatusPost
                case 7503: {                                                                        
                    return "DELETE FROM p7500StatusPost
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7500StatusPost
                case 7504: {                                                                        
                    return "SELECT * FROM p7500StatusPost
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7500StatusPost
                case 7505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7500StatusPost $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7500StatusPost T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7500StatusPost
                case 7506: {                                                                        
                    return "SELECT COUNT(1) FROM p7500StatusPost $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
