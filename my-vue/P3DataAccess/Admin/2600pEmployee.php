<?php                                                                                      
	class EmployeeDA{				
		public function EmployeeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2600Employee************************             
                // p2600Employee(id,IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch)
                // Get all data from p2600Employee
                case 2600: {                                                                        
                    return "SELECT * FROM p2600Employee";
                }                                                                                  
                                                                                                   
                // Insert data to p2600Employee
                case 2601: {                                                                        
                    return "INSERT INTO p2600Employee(IdTypeEmployee,IdStatusEmployee,FullName,Email,Password,Address,Phone,Avatar,Certification,CreatedAt,IdBranch)
                            VALUES('$param->IdTypeEmployee','$param->IdStatusEmployee','$param->FullName','$param->Email','$param->Password','$param->Address','$param->Phone','$param->Avatar','$param->Certification','$param->CreatedAt','$param->IdBranch')";                               
                }                                                                                  
                                                                                                   
                // Update data p2600Employee
                case 2602: {
                    return "UPDATE p2600Employee SET IdTypeEmployee='$param->IdTypeEmployee',IdStatusEmployee='$param->IdStatusEmployee',FullName='$param->FullName',Email='$param->Email',Password='$param->Password',Address='$param->Address',Phone='$param->Phone',Avatar='$param->Avatar',Certification='$param->Certification',CreatedAt='$param->CreatedAt',IdBranch='$param->IdBranch'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2600Employee
                case 2603: {                                                                        
                    return "DELETE FROM p2600Employee
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2600Employee
                case 2604: {                                                                        
                    return "SELECT * FROM p2600Employee
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2600Employee
                case 2605: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2600Employee $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2600Employee T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2600Employee
                case 2606: {                                                                        
                    return "SELECT COUNT(1) FROM p2600Employee $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
