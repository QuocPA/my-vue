<?php                                                                                      
	class StatusTransferCarDA{				
		public function StatusTransferCarDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7900StatusTransferCar************************             
                // p7900StatusTransferCar(id,Name)
                // Get all data from p7900StatusTransferCar
                case 7900: {                                                                        
                    return "SELECT * FROM p7900StatusTransferCar";
                }                                                                                  
                                                                                                   
                // Insert data to p7900StatusTransferCar
                case 7901: {                                                                        
                    return "INSERT INTO p7900StatusTransferCar(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p7900StatusTransferCar
                case 7902: {
                    return "UPDATE p7900StatusTransferCar SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7900StatusTransferCar
                case 7903: {                                                                        
                    return "DELETE FROM p7900StatusTransferCar
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7900StatusTransferCar
                case 7904: {                                                                        
                    return "SELECT * FROM p7900StatusTransferCar
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7900StatusTransferCar
                case 7905: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7900StatusTransferCar $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7900StatusTransferCar T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7900StatusTransferCar
                case 7906: {                                                                        
                    return "SELECT COUNT(1) FROM p7900StatusTransferCar $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
