<?php                                                                                      
	class LearnTimeDA{				
		public function LearnTimeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p700LearnTime************************             
                // p700LearnTime(id,IdBranch,TimeStart,TimeEnd,Position)
                // Get all data from p700LearnTime
                case 700: {                                                                        
                    return "SELECT * FROM p700LearnTime";
                }                                                                                  
                                                                                                   
                // Insert data to p700LearnTime
                case 701: {                                                                        
                    return "INSERT INTO p700LearnTime(IdBranch,TimeStart,TimeEnd,Position)
                            VALUES('$param->IdBranch','$param->TimeStart','$param->TimeEnd','$param->Position')";                               
                }                                                                                  
                                                                                                   
                // Update data p700LearnTime
                case 702: {
                    return "UPDATE p700LearnTime SET IdBranch='$param->IdBranch',TimeStart='$param->TimeStart',TimeEnd='$param->TimeEnd',Position='$param->Position'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p700LearnTime
                case 703: {                                                                        
                    return "DELETE FROM p700LearnTime
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p700LearnTime
                case 704: {                                                                        
                    return "SELECT * FROM p700LearnTime
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p700LearnTime
                case 705: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p700LearnTime $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p700LearnTime T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p700LearnTime
                case 706: {                                                                        
                    return "SELECT COUNT(1) FROM p700LearnTime $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
