<?php                                                                                      
	class CustomerDA{				
		public function CustomerDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5500Customer************************             
                // p5500Customer(id,FullName,Born,Address,Phone)
                // Get all data from p5500Customer
                case 5500: {                                                                        
                    return "SELECT * FROM p5500Customer";
                }                                                                                  
                                                                                                   
                // Insert data to p5500Customer
                case 5501: {                                                                        
                    return "INSERT INTO p5500Customer(FullName,Born,Address,Phone)
                            VALUES('$param->FullName','$param->Born','$param->Address','$param->Phone')";                               
                }                                                                                  
                                                                                                   
                // Update data p5500Customer
                case 5502: {
                    return "UPDATE p5500Customer SET FullName='$param->FullName',Born='$param->Born',Address='$param->Address',Phone='$param->Phone'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5500Customer
                case 5503: {                                                                        
                    return "DELETE FROM p5500Customer
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5500Customer
                case 5504: {                                                                        
                    return "SELECT * FROM p5500Customer
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5500Customer
                case 5505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5500Customer $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5500Customer T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5500Customer
                case 5506: {                                                                        
                    return "SELECT COUNT(1) FROM p5500Customer $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
