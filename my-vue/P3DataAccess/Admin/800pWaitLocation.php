<?php                                                                                      
	class WaitLocationDA{				
		public function WaitLocationDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p800WaitLocation************************             
                // p800WaitLocation(id,Name)
                // Get all data from p800WaitLocation
                case 800: {                                                                        
                    return "SELECT * FROM p800WaitLocation";
                }                                                                                  
                                                                                                   
                // Insert data to p800WaitLocation
                case 801: {                                                                        
                    return "INSERT INTO p800WaitLocation(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p800WaitLocation
                case 802: {
                    return "UPDATE p800WaitLocation SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p800WaitLocation
                case 803: {                                                                        
                    return "DELETE FROM p800WaitLocation
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p800WaitLocation
                case 804: {                                                                        
                    return "SELECT * FROM p800WaitLocation
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p800WaitLocation
                case 805: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p800WaitLocation $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p800WaitLocation T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p800WaitLocation
                case 806: {                                                                        
                    return "SELECT COUNT(1) FROM p800WaitLocation $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
