<?php                                                                                      
	class CarCategoriesDA{				
		public function CarCategoriesDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4900CarCategories************************             
                // p4900CarCategories(id,Name)
                // Get all data from p4900CarCategories
                case 4900: {                                                                        
                    return "SELECT * FROM p4900CarCategories";
                }                                                                                  
                                                                                                   
                // Insert data to p4900CarCategories
                case 4901: {                                                                        
                    return "INSERT INTO p4900CarCategories(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p4900CarCategories
                case 4902: {
                    return "UPDATE p4900CarCategories SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4900CarCategories
                case 4903: {                                                                        
                    return "DELETE FROM p4900CarCategories
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4900CarCategories
                case 4904: {                                                                        
                    return "SELECT * FROM p4900CarCategories
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4900CarCategories
                case 4905: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4900CarCategories $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4900CarCategories T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4900CarCategories
                case 4906: {                                                                        
                    return "SELECT COUNT(1) FROM p4900CarCategories $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
