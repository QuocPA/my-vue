<?php                                                                                      
	class ReviewStudentDA{				
		public function ReviewStudentDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p8300ReviewStudent************************             
                // p8300ReviewStudent(id,IdRegisterLearn,Rate,Content)
                // Get all data from p8300ReviewStudent
                case 8300: {                                                                        
                    return "SELECT * FROM p8300ReviewStudent";
                }                                                                                  
                                                                                                   
                // Insert data to p8300ReviewStudent
                case 8301: {                                                                        
                    return "INSERT INTO p8300ReviewStudent(IdRegisterLearn,Rate,Content)
                            VALUES('$param->IdRegisterLearn','$param->Rate','$param->Content')";                               
                }                                                                                  
                                                                                                   
                // Update data p8300ReviewStudent
                case 8302: {
                    return "UPDATE p8300ReviewStudent SET IdRegisterLearn='$param->IdRegisterLearn',Rate='$param->Rate',Content='$param->Content'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p8300ReviewStudent
                case 8303: {                                                                        
                    return "DELETE FROM p8300ReviewStudent
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p8300ReviewStudent
                case 8304: {                                                                        
                    return "SELECT * FROM p8300ReviewStudent
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p8300ReviewStudent
                case 8305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p8300ReviewStudent $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p8300ReviewStudent T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p8300ReviewStudent
                case 8306: {                                                                        
                    return "SELECT COUNT(1) FROM p8300ReviewStudent $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
