<?php                                                                                      
	class CostDA{				
		public function CostDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4500Cost************************             
                // p4500Cost(id,IdBranch,IdCostCategories,Money,Note,CreatedAt)
                // Get all data from p4500Cost
                case 4500: {                                                                        
                    return "SELECT * FROM p4500Cost";
                }                                                                                  
                                                                                                   
                // Insert data to p4500Cost
                case 4501: {                                                                        
                    return "INSERT INTO p4500Cost(IdBranch,IdCostCategories,Money,Note,CreatedAt)
                            VALUES('$param->IdBranch','$param->IdCostCategories','$param->Money','$param->Note','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p4500Cost
                case 4502: {
                    return "UPDATE p4500Cost SET IdBranch='$param->IdBranch',IdCostCategories='$param->IdCostCategories',Money='$param->Money',Note='$param->Note',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4500Cost
                case 4503: {                                                                        
                    return "DELETE FROM p4500Cost
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4500Cost
                case 4504: {                                                                        
                    return "SELECT * FROM p4500Cost
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4500Cost
                case 4505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4500Cost $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4500Cost T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4500Cost
                case 4506: {                                                                        
                    return "SELECT COUNT(1) FROM p4500Cost $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
