<?php                                                                                      
	class TypeEmployeeDA{				
		public function TypeEmployeeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2200TypeEmployee************************             
                // p2200TypeEmployee(id,Name)
                // Get all data from p2200TypeEmployee
                case 2200: {                                                                        
                    return "SELECT * FROM p2200TypeEmployee";
                }                                                                                  
                                                                                                   
                // Insert data to p2200TypeEmployee
                case 2201: {                                                                        
                    return "INSERT INTO p2200TypeEmployee(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p2200TypeEmployee
                case 2202: {
                    return "UPDATE p2200TypeEmployee SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2200TypeEmployee
                case 2203: {                                                                        
                    return "DELETE FROM p2200TypeEmployee
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2200TypeEmployee
                case 2204: {                                                                        
                    return "SELECT * FROM p2200TypeEmployee
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2200TypeEmployee
                case 2205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2200TypeEmployee $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2200TypeEmployee T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2200TypeEmployee
                case 2206: {                                                                        
                    return "SELECT COUNT(1) FROM p2200TypeEmployee $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
