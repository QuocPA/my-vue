<?php                                                                                      
	class BranchDA{				
		public function BranchDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p400Branch************************             
                // p400Branch(id,Name,Address)
                // Get all data from p400Branch
                case 400: {                                                                        
                    return "SELECT * FROM p400Branch";
                }                                                                                  
                                                                                                   
                // Insert data to p400Branch
                case 401: {                                                                        
                    return "INSERT INTO p400Branch(Name,Address)
                            VALUES('$param->Name','$param->Address')";                               
                }                                                                                  
                                                                                                   
                // Update data p400Branch
                case 402: {
                    return "UPDATE p400Branch SET Name='$param->Name',Address='$param->Address'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p400Branch
                case 403: {                                                                        
                    return "DELETE FROM p400Branch
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p400Branch
                case 404: {                                                                        
                    return "SELECT * FROM p400Branch
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p400Branch
                case 405: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p400Branch $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p400Branch T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p400Branch
                case 406: {                                                                        
                    return "SELECT COUNT(1) FROM p400Branch $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
