<?php                                                                                      
	class StatusDayOfDA{				
		public function StatusDayOfDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7100StatusDayOf************************             
                // p7100StatusDayOf(id,Name)
                // Get all data from p7100StatusDayOf
                case 7100: {                                                                        
                    return "SELECT * FROM p7100StatusDayOf";
                }                                                                                  
                                                                                                   
                // Insert data to p7100StatusDayOf
                case 7101: {                                                                        
                    return "INSERT INTO p7100StatusDayOf(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p7100StatusDayOf
                case 7102: {
                    return "UPDATE p7100StatusDayOf SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7100StatusDayOf
                case 7103: {                                                                        
                    return "DELETE FROM p7100StatusDayOf
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7100StatusDayOf
                case 7104: {                                                                        
                    return "SELECT * FROM p7100StatusDayOf
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7100StatusDayOf
                case 7105: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7100StatusDayOf $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7100StatusDayOf T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7100StatusDayOf
                case 7106: {                                                                        
                    return "SELECT COUNT(1) FROM p7100StatusDayOf $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
