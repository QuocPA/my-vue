<?php                                                                                      
	class StatusApprovedDA{				
		public function StatusApprovedDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7200StatusApproved************************             
                // p7200StatusApproved(id,Name)
                // Get all data from p7200StatusApproved
                case 7200: {                                                                        
                    return "SELECT * FROM p7200StatusApproved";
                }                                                                                  
                                                                                                   
                // Insert data to p7200StatusApproved
                case 7201: {                                                                        
                    return "INSERT INTO p7200StatusApproved(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p7200StatusApproved
                case 7202: {
                    return "UPDATE p7200StatusApproved SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7200StatusApproved
                case 7203: {                                                                        
                    return "DELETE FROM p7200StatusApproved
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7200StatusApproved
                case 7204: {                                                                        
                    return "SELECT * FROM p7200StatusApproved
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7200StatusApproved
                case 7205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7200StatusApproved $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7200StatusApproved T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7200StatusApproved
                case 7206: {                                                                        
                    return "SELECT COUNT(1) FROM p7200StatusApproved $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
