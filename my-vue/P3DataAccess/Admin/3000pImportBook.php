<?php                                                                                      
	class ImportBookDA{				
		public function ImportBookDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3000ImportBook************************             
                // p3000ImportBook(id,IdBook,Total,CreatedAt)
                // Get all data from p3000ImportBook
                case 3000: {                                                                        
                    return "SELECT * FROM p3000ImportBook";
                }                                                                                  
                                                                                                   
                // Insert data to p3000ImportBook
                case 3001: {                                                                        
                    return "INSERT INTO p3000ImportBook(IdBook,Total,CreatedAt)
                            VALUES('$param->IdBook','$param->Total','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p3000ImportBook
                case 3002: {
                    return "UPDATE p3000ImportBook SET IdBook='$param->IdBook',Total='$param->Total',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3000ImportBook
                case 3003: {                                                                        
                    return "DELETE FROM p3000ImportBook
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3000ImportBook
                case 3004: {                                                                        
                    return "SELECT * FROM p3000ImportBook
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3000ImportBook
                case 3005: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3000ImportBook $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3000ImportBook T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3000ImportBook
                case 3006: {                                                                        
                    return "SELECT COUNT(1) FROM p3000ImportBook $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
