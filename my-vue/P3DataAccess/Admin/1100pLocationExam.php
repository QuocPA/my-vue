<?php                                                                                      
	class LocationExamDA{				
		public function LocationExamDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1100LocationExam************************             
                // p1100LocationExam(id,Name)
                // Get all data from p1100LocationExam
                case 1100: {                                                                        
                    return "SELECT * FROM p1100LocationExam";
                }                                                                                  
                                                                                                   
                // Insert data to p1100LocationExam
                case 1101: {                                                                        
                    return "INSERT INTO p1100LocationExam(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p1100LocationExam
                case 1102: {
                    return "UPDATE p1100LocationExam SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1100LocationExam
                case 1103: {                                                                        
                    return "DELETE FROM p1100LocationExam
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1100LocationExam
                case 1104: {                                                                        
                    return "SELECT * FROM p1100LocationExam
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1100LocationExam
                case 1105: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1100LocationExam $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1100LocationExam T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1100LocationExam
                case 1106: {                                                                        
                    return "SELECT COUNT(1) FROM p1100LocationExam $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
