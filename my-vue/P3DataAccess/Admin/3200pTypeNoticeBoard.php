<?php                                                                                      
	class TypeNoticeBoardDA{				
		public function TypeNoticeBoardDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3200TypeNoticeBoard************************             
                // p3200TypeNoticeBoard(id,Name,Image)
                // Get all data from p3200TypeNoticeBoard
                case 3200: {                                                                        
                    return "SELECT * FROM p3200TypeNoticeBoard";
                }                                                                                  
                                                                                                   
                // Insert data to p3200TypeNoticeBoard
                case 3201: {                                                                        
                    return "INSERT INTO p3200TypeNoticeBoard(Name,Image)
                            VALUES('$param->Name','$param->Image')";                               
                }                                                                                  
                                                                                                   
                // Update data p3200TypeNoticeBoard
                case 3202: {
                    return "UPDATE p3200TypeNoticeBoard SET Name='$param->Name',Image='$param->Image'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3200TypeNoticeBoard
                case 3203: {                                                                        
                    return "DELETE FROM p3200TypeNoticeBoard
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3200TypeNoticeBoard
                case 3204: {                                                                        
                    return "SELECT * FROM p3200TypeNoticeBoard
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3200TypeNoticeBoard
                case 3205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3200TypeNoticeBoard $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3200TypeNoticeBoard T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3200TypeNoticeBoard
                case 3206: {                                                                        
                    return "SELECT COUNT(1) FROM p3200TypeNoticeBoard $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
