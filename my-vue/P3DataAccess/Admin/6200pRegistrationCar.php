<?php                                                                                      
	class RegistrationCarDA{				
		public function RegistrationCarDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6200RegistrationCar************************             
                // p6200RegistrationCar(id,FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited)
                // Get all data from p6200RegistrationCar
                case 6200: {                                                                        
                    return "SELECT * FROM p6200RegistrationCar";
                }                                                                                  
                                                                                                   
                // Insert data to p6200RegistrationCar
                case 6201: {                                                                        
                    return "INSERT INTO p6200RegistrationCar(FullName,Address,Phone,NumberCharCar,IdCarManufacturer,Color,DateCreate,Kilometer,Purpose,BeInvited)
                            VALUES('$param->FullName','$param->Address','$param->Phone','$param->NumberCharCar','$param->IdCarManufacturer','$param->Color','$param->DateCreate','$param->Kilometer','$param->Purpose','$param->BeInvited')";                               
                }                                                                                  
                                                                                                   
                // Update data p6200RegistrationCar
                case 6202: {
                    return "UPDATE p6200RegistrationCar SET FullName='$param->FullName',Address='$param->Address',Phone='$param->Phone',NumberCharCar='$param->NumberCharCar',IdCarManufacturer='$param->IdCarManufacturer',Color='$param->Color',DateCreate='$param->DateCreate',Kilometer='$param->Kilometer',Purpose='$param->Purpose',BeInvited='$param->BeInvited'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6200RegistrationCar
                case 6203: {                                                                        
                    return "DELETE FROM p6200RegistrationCar
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6200RegistrationCar
                case 6204: {                                                                        
                    return "SELECT * FROM p6200RegistrationCar
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6200RegistrationCar
                case 6205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6200RegistrationCar $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6200RegistrationCar T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6200RegistrationCar
                case 6206: {                                                                        
                    return "SELECT COUNT(1) FROM p6200RegistrationCar $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
