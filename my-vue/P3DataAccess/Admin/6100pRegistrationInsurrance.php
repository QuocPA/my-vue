<?php                                                                                      
	class RegistrationInsurranceDA{				
		public function RegistrationInsurranceDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6100RegistrationInsurrance************************             
                // p6100RegistrationInsurrance(id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice)
                // Get all data from p6100RegistrationInsurrance
                case 6100: {                                                                        
                    return "SELECT * FROM p6100RegistrationInsurrance";
                }                                                                                  
                                                                                                   
                // Insert data to p6100RegistrationInsurrance
                case 6101: {                                                                        
                    return "INSERT INTO p6100RegistrationInsurrance(FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice)
                            VALUES('$param->FullName','$param->Born','$param->Address','$param->Phone','$param->CarName','$param->TypeCar','$param->CreatedDate','$param->TotalPrice')";                               
                }                                                                                  
                                                                                                   
                // Update data p6100RegistrationInsurrance
                case 6102: {
                    return "UPDATE p6100RegistrationInsurrance SET FullName='$param->FullName',Born='$param->Born',Address='$param->Address',Phone='$param->Phone',CarName='$param->CarName',TypeCar='$param->TypeCar',CreatedDate='$param->CreatedDate',TotalPrice='$param->TotalPrice'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6100RegistrationInsurrance
                case 6103: {                                                                        
                    return "DELETE FROM p6100RegistrationInsurrance
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6100RegistrationInsurrance
                case 6104: {                                                                        
                    return "SELECT * FROM p6100RegistrationInsurrance
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6100RegistrationInsurrance
                case 6105: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6100RegistrationInsurrance $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6100RegistrationInsurrance T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6100RegistrationInsurrance
                case 6106: {                                                                        
                    return "SELECT COUNT(1) FROM p6100RegistrationInsurrance $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
