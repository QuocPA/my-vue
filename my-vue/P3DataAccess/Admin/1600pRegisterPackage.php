<?php                                                                                      
	class RegisterPackageDA{				
		public function RegisterPackageDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1600RegisterPackage************************             
                // p1600RegisterPackage(id,IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice)
                // Get all data from p1600RegisterPackage
                case 1600: {                                                                        
                    return "SELECT * FROM p1600RegisterPackage";
                }                                                                                  
                                                                                                   
                // Insert data to p1600RegisterPackage
                case 1601: {                                                                        
                    return "INSERT INTO p1600RegisterPackage(IdPackage,IdStudent,IdStatusPackagePayment,IdBranch,FullPrice)
                            VALUES('$param->IdPackage','$param->IdStudent','$param->IdStatusPackagePayment','$param->IdBranch','$param->FullPrice')";                               
                }                                                                                  
                                                                                                   
                // Update data p1600RegisterPackage
                case 1602: {
                    return "UPDATE p1600RegisterPackage SET IdPackage='$param->IdPackage',IdStudent='$param->IdStudent',IdStatusPackagePayment='$param->IdStatusPackagePayment',IdBranch='$param->IdBranch',FullPrice='$param->FullPrice'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1600RegisterPackage
                case 1603: {                                                                        
                    return "DELETE FROM p1600RegisterPackage
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1600RegisterPackage
                case 1604: {                                                                        
                    return "SELECT * FROM p1600RegisterPackage
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1600RegisterPackage
                case 1605: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1600RegisterPackage $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1600RegisterPackage T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1600RegisterPackage
                case 1606: {                                                                        
                    return "SELECT COUNT(1) FROM p1600RegisterPackage $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
