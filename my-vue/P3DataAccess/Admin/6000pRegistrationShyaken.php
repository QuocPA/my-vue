<?php                                                                                      
	class RegistrationShyakenDA{				
		public function RegistrationShyakenDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6000RegistrationShyaken************************             
                // p6000RegistrationShyaken(id,FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice)
                // Get all data from p6000RegistrationShyaken
                case 6000: {                                                                        
                    return "SELECT * FROM p6000RegistrationShyaken";
                }                                                                                  
                                                                                                   
                // Insert data to p6000RegistrationShyaken
                case 6001: {                                                                        
                    return "INSERT INTO p6000RegistrationShyaken(FullName,Born,Address,Phone,CarName,TypeCar,CreatedDate,TotalPrice)
                            VALUES('$param->FullName','$param->Born','$param->Address','$param->Phone','$param->CarName','$param->TypeCar','$param->CreatedDate','$param->TotalPrice')";                               
                }                                                                                  
                                                                                                   
                // Update data p6000RegistrationShyaken
                case 6002: {
                    return "UPDATE p6000RegistrationShyaken SET FullName='$param->FullName',Born='$param->Born',Address='$param->Address',Phone='$param->Phone',CarName='$param->CarName',TypeCar='$param->TypeCar',CreatedDate='$param->CreatedDate',TotalPrice='$param->TotalPrice'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6000RegistrationShyaken
                case 6003: {                                                                        
                    return "DELETE FROM p6000RegistrationShyaken
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6000RegistrationShyaken
                case 6004: {                                                                        
                    return "SELECT * FROM p6000RegistrationShyaken
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6000RegistrationShyaken
                case 6005: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6000RegistrationShyaken $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6000RegistrationShyaken T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6000RegistrationShyaken
                case 6006: {                                                                        
                    return "SELECT COUNT(1) FROM p6000RegistrationShyaken $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
