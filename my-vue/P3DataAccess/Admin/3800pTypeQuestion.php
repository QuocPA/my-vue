<?php                                                                                      
	class TypeQuestionDA{				
		public function TypeQuestionDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3800TypeQuestion************************             
                // p3800TypeQuestion(id,Name)
                // Get all data from p3800TypeQuestion
                case 3800: {                                                                        
                    return "SELECT * FROM p3800TypeQuestion";
                }                                                                                  
                                                                                                   
                // Insert data to p3800TypeQuestion
                case 3801: {                                                                        
                    return "INSERT INTO p3800TypeQuestion(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p3800TypeQuestion
                case 3802: {
                    return "UPDATE p3800TypeQuestion SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3800TypeQuestion
                case 3803: {                                                                        
                    return "DELETE FROM p3800TypeQuestion
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3800TypeQuestion
                case 3804: {                                                                        
                    return "SELECT * FROM p3800TypeQuestion
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3800TypeQuestion
                case 3805: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3800TypeQuestion $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3800TypeQuestion T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3800TypeQuestion
                case 3806: {                                                                        
                    return "SELECT COUNT(1) FROM p3800TypeQuestion $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
