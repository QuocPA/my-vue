<?php                                                                                      
	class PaymentTypeDA{				
		public function PaymentTypeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7600PaymentType************************             
                // p7600PaymentType(id,Name)
                // Get all data from p7600PaymentType
                case 7600: {                                                                        
                    return "SELECT * FROM p7600PaymentType";
                }                                                                                  
                                                                                                   
                // Insert data to p7600PaymentType
                case 7601: {                                                                        
                    return "INSERT INTO p7600PaymentType(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p7600PaymentType
                case 7602: {
                    return "UPDATE p7600PaymentType SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7600PaymentType
                case 7603: {                                                                        
                    return "DELETE FROM p7600PaymentType
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7600PaymentType
                case 7604: {                                                                        
                    return "SELECT * FROM p7600PaymentType
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7600PaymentType
                case 7605: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7600PaymentType $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7600PaymentType T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7600PaymentType
                case 7606: {                                                                        
                    return "SELECT COUNT(1) FROM p7600PaymentType $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
