<?php                                                                                      
	class NotificationDA{				
		public function NotificationDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6300Notification************************             
                // p6300Notification(id,IdNotificationType,Title,Content,Author,DateCreate)
                // Get all data from p6300Notification
                case 6300: {                                                                        
                    return "SELECT * FROM p6300Notification";
                }                                                                                  
                                                                                                   
                // Insert data to p6300Notification
                case 6301: {                                                                        
                    return "INSERT INTO p6300Notification(IdNotificationType,Title,Content,Author,DateCreate)
                            VALUES('$param->IdNotificationType','$param->Title','$param->Content','$param->Author','$param->DateCreate')";                               
                }                                                                                  
                                                                                                   
                // Update data p6300Notification
                case 6302: {
                    return "UPDATE p6300Notification SET IdNotificationType='$param->IdNotificationType',Title='$param->Title',Content='$param->Content',Author='$param->Author',DateCreate='$param->DateCreate'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6300Notification
                case 6303: {                                                                        
                    return "DELETE FROM p6300Notification
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6300Notification
                case 6304: {                                                                        
                    return "SELECT * FROM p6300Notification
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6300Notification
                case 6305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6300Notification $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6300Notification T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6300Notification
                case 6306: {                                                                        
                    return "SELECT COUNT(1) FROM p6300Notification $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
