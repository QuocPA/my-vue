<?php                                                                                      
	class StatusRegisterLearnDA{				
		public function StatusRegisterLearnDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p900StatusRegisterLearn************************             
                // p900StatusRegisterLearn(id,Name)
                // Get all data from p900StatusRegisterLearn
                case 900: {                                                                        
                    return "SELECT * FROM p900StatusRegisterLearn";
                }                                                                                  
                                                                                                   
                // Insert data to p900StatusRegisterLearn
                case 901: {                                                                        
                    return "INSERT INTO p900StatusRegisterLearn(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p900StatusRegisterLearn
                case 902: {
                    return "UPDATE p900StatusRegisterLearn SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p900StatusRegisterLearn
                case 903: {                                                                        
                    return "DELETE FROM p900StatusRegisterLearn
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p900StatusRegisterLearn
                case 904: {                                                                        
                    return "SELECT * FROM p900StatusRegisterLearn
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p900StatusRegisterLearn
                case 905: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p900StatusRegisterLearn $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p900StatusRegisterLearn T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p900StatusRegisterLearn
                case 906: {                                                                        
                    return "SELECT COUNT(1) FROM p900StatusRegisterLearn $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
