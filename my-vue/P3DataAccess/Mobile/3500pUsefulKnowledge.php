<?php                                                                                      
	class UsefulKnowledgeDA{				
		public function UsefulKnowledgeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3500UsefulKnowledge************************             
                // p3500UsefulKnowledge(id,IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description)
                // Get all data from p3500UsefulKnowledge
                case 3500: {                                                                        
                    return "SELECT * FROM p3500UsefulKnowledge";
                }                                                                                  
                                                                                                   
                // Insert data to p3500UsefulKnowledge
                case 3501: {                                                                        
                    return "INSERT INTO p3500UsefulKnowledge(IdUsefulKnowledgeCategories,Name,Image,Title,Content,Description)
                            VALUES('$param->IdUsefulKnowledgeCategories','$param->Name','$param->Image','$param->Title','$param->Content','$param->Description')";                               
                }                                                                                  
                                                                                                   
                // Update data p3500UsefulKnowledge
                case 3502: {
                    return "UPDATE p3500UsefulKnowledge SET IdUsefulKnowledgeCategories='$param->IdUsefulKnowledgeCategories',Name='$param->Name',Image='$param->Image',Title='$param->Title',Content='$param->Content',Description='$param->Description'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3500UsefulKnowledge
                case 3503: {                                                                        
                    return "DELETE FROM p3500UsefulKnowledge
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3500UsefulKnowledge
                case 3504: {                                                                        
                    return "SELECT * FROM p3500UsefulKnowledge
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3500UsefulKnowledge
                case 3505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3500UsefulKnowledge $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3500UsefulKnowledge T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3500UsefulKnowledge
                case 3506: {                                                                        
                    return "SELECT COUNT(1) FROM p3500UsefulKnowledge $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
