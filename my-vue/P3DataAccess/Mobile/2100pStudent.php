<?php                                                                                      
	class StudentDA{				
		public function StudentDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2100Student************************             
                // p2100Student(id,IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy)
                // Get all data from p2100Student
                case 2100: {                                                                        
                    return "SELECT * FROM p2100Student";
                }                                                                                  
                                                                                                   
                // Insert data to p2100Student
                case 2101: {                                                                        
                    return "INSERT INTO p2100Student(IdStatusStudent,IdTypeSource,FullName,Email,Password,Address,Phone,Avatar,CreatedAt,Note,Born,IdSex,DateStartStudy)
                            VALUES('$param->IdStatusStudent','$param->IdTypeSource','$param->FullName','$param->Email','$param->Password','$param->Address','$param->Phone','$param->Avatar','$param->CreatedAt','$param->Note','$param->Born','$param->IdSex','$param->DateStartStudy')";                               
                }                                                                                  
                                                                                                   
                // Update data p2100Student
                case 2102: {
                    return "UPDATE p2100Student SET IdStatusStudent='$param->IdStatusStudent',IdTypeSource='$param->IdTypeSource',FullName='$param->FullName',Email='$param->Email',Password='$param->Password',Address='$param->Address',Phone='$param->Phone',Avatar='$param->Avatar',CreatedAt='$param->CreatedAt',Note='$param->Note',Born='$param->Born',IdSex='$param->IdSex',DateStartStudy='$param->DateStartStudy'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2100Student
                case 2103: {                                                                        
                    return "DELETE FROM p2100Student
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2100Student
                case 2104: {                                                                        
                    return "SELECT * FROM p2100Student
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2100Student
                case 2105: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2100Student $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2100Student T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2100Student
                case 2106: {                                                                        
                    return "SELECT COUNT(1) FROM p2100Student $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
