<?php                                                                                      
	class TypePackageDA{				
		public function TypePackageDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1300TypePackage************************             
                // p1300TypePackage(id,Name)
                // Get all data from p1300TypePackage
                case 1300: {                                                                        
                    return "SELECT * FROM p1300TypePackage";
                }                                                                                  
                                                                                                   
                // Insert data to p1300TypePackage
                case 1301: {                                                                        
                    return "INSERT INTO p1300TypePackage(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p1300TypePackage
                case 1302: {
                    return "UPDATE p1300TypePackage SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1300TypePackage
                case 1303: {                                                                        
                    return "DELETE FROM p1300TypePackage
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1300TypePackage
                case 1304: {                                                                        
                    return "SELECT * FROM p1300TypePackage
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1300TypePackage
                case 1305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1300TypePackage $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1300TypePackage T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1300TypePackage
                case 1306: {                                                                        
                    return "SELECT COUNT(1) FROM p1300TypePackage $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
