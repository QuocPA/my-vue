<?php                                                                                      
	class OutputCarDA{				
		public function OutputCarDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5900OutputCar************************             
                // p5900OutputCar(id,IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note)
                // Get all data from p5900OutputCar
                case 5900: {                                                                        
                    return "SELECT * FROM p5900OutputCar";
                }                                                                                  
                                                                                                   
                // Insert data to p5900OutputCar
                case 5901: {                                                                        
                    return "INSERT INTO p5900OutputCar(IdCar,IdEmployee,IdCustomer,PriceRecieved,PriceLost,IdPaymentType,IdStatusPrepareTransferCar,DateExperiedTransfer,IdStatusTransferCar,NumberPayment,Note)
                            VALUES('$param->IdCar','$param->IdEmployee','$param->IdCustomer','$param->PriceRecieved','$param->PriceLost','$param->IdPaymentType','$param->IdStatusPrepareTransferCar','$param->DateExperiedTransfer','$param->IdStatusTransferCar','$param->NumberPayment','$param->Note')";                               
                }                                                                                  
                                                                                                   
                // Update data p5900OutputCar
                case 5902: {
                    return "UPDATE p5900OutputCar SET IdCar='$param->IdCar',IdEmployee='$param->IdEmployee',IdCustomer='$param->IdCustomer',PriceRecieved='$param->PriceRecieved',PriceLost='$param->PriceLost',IdPaymentType='$param->IdPaymentType',IdStatusPrepareTransferCar='$param->IdStatusPrepareTransferCar',DateExperiedTransfer='$param->DateExperiedTransfer',IdStatusTransferCar='$param->IdStatusTransferCar',NumberPayment='$param->NumberPayment',Note='$param->Note'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5900OutputCar
                case 5903: {                                                                        
                    return "DELETE FROM p5900OutputCar
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5900OutputCar
                case 5904: {                                                                        
                    return "SELECT * FROM p5900OutputCar
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5900OutputCar
                case 5905: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5900OutputCar $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5900OutputCar T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5900OutputCar
                case 5906: {                                                                        
                    return "SELECT COUNT(1) FROM p5900OutputCar $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
