<?php                                                                                      
	class ReviewDA{				
		public function ReviewDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1800Review************************             
                // p1800Review(id,IdRegisterLearn,Rate,Content)
                // Get all data from p1800Review
                case 1800: {                                                                        
                    return "SELECT * FROM p1800Review";
                }                                                                                  
                                                                                                   
                // Insert data to p1800Review
                case 1801: {                                                                        
                    return "INSERT INTO p1800Review(IdRegisterLearn,Rate,Content)
                            VALUES('$param->IdRegisterLearn','$param->Rate','$param->Content')";                               
                }                                                                                  
                                                                                                   
                // Update data p1800Review
                case 1802: {
                    return "UPDATE p1800Review SET IdRegisterLearn='$param->IdRegisterLearn',Rate='$param->Rate',Content='$param->Content'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1800Review
                case 1803: {                                                                        
                    return "DELETE FROM p1800Review
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1800Review
                case 1804: {                                                                        
                    return "SELECT * FROM p1800Review
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1800Review
                case 1805: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1800Review $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1800Review T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1800Review
                case 1806: {                                                                        
                    return "SELECT COUNT(1) FROM p1800Review $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
