<?php                                                                                      
	class SubjectDA{				
		public function SubjectDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p600Subject************************             
                // p600Subject(id,Name)
                // Get all data from p600Subject
                case 600: {                                                                        
                    return "SELECT * FROM p600Subject";
                }                                                                                  
                                                                                                   
                // Insert data to p600Subject
                case 601: {                                                                        
                    return "INSERT INTO p600Subject(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p600Subject
                case 602: {
                    return "UPDATE p600Subject SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p600Subject
                case 603: {                                                                        
                    return "DELETE FROM p600Subject
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p600Subject
                case 604: {                                                                        
                    return "SELECT * FROM p600Subject
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p600Subject
                case 605: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p600Subject $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p600Subject T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p600Subject
                case 606: {                                                                        
                    return "SELECT COUNT(1) FROM p600Subject $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
