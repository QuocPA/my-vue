<?php                                                                                      
	class StatusApproveRegisterDA{				
		public function StatusApproveRegisterDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1000StatusApproveRegister************************             
                // p1000StatusApproveRegister(id,Name)
                // Get all data from p1000StatusApproveRegister
                case 1000: {                                                                        
                    return "SELECT * FROM p1000StatusApproveRegister";
                }                                                                                  
                                                                                                   
                // Insert data to p1000StatusApproveRegister
                case 1001: {                                                                        
                    return "INSERT INTO p1000StatusApproveRegister(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p1000StatusApproveRegister
                case 1002: {
                    return "UPDATE p1000StatusApproveRegister SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1000StatusApproveRegister
                case 1003: {                                                                        
                    return "DELETE FROM p1000StatusApproveRegister
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1000StatusApproveRegister
                case 1004: {                                                                        
                    return "SELECT * FROM p1000StatusApproveRegister
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1000StatusApproveRegister
                case 1005: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1000StatusApproveRegister $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1000StatusApproveRegister T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1000StatusApproveRegister
                case 1006: {                                                                        
                    return "SELECT COUNT(1) FROM p1000StatusApproveRegister $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
