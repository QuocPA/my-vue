<?php                                                                                      
	class RegisterLearnDA{				
		public function RegisterLearnDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1700RegisterLearn************************             
                // p1700RegisterLearn(id,IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt)
                // Get all data from p1700RegisterLearn
                case 1700: {                                                                        
                    return "SELECT * FROM p1700RegisterLearn";
                }                                                                                  
                                                                                                   
                // Insert data to p1700RegisterLearn
                case 1701: {                                                                        
                    return "INSERT INTO p1700RegisterLearn(IdBranch,IdLesson,IdSubject,IdLearnTime,IdWaitLocation,IdStatusRegisterLearn,IdStatusApproveRegister,IdStudent,IdEmployee,IdDateExam,Note,LearnDate,CreatedAt)
                            VALUES('$param->IdBranch','$param->IdLesson','$param->IdSubject','$param->IdLearnTime','$param->IdWaitLocation','$param->IdStatusRegisterLearn','$param->IdStatusApproveRegister','$param->IdStudent','$param->IdEmployee','$param->IdDateExam','$param->Note','$param->LearnDate','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p1700RegisterLearn
                case 1702: {
                    return "UPDATE p1700RegisterLearn SET IdBranch='$param->IdBranch',IdLesson='$param->IdLesson',IdSubject='$param->IdSubject',IdLearnTime='$param->IdLearnTime',IdWaitLocation='$param->IdWaitLocation',IdStatusRegisterLearn='$param->IdStatusRegisterLearn',IdStatusApproveRegister='$param->IdStatusApproveRegister',IdStudent='$param->IdStudent',IdEmployee='$param->IdEmployee',IdDateExam='$param->IdDateExam',Note='$param->Note',LearnDate='$param->LearnDate',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1700RegisterLearn
                case 1703: {                                                                        
                    return "DELETE FROM p1700RegisterLearn
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1700RegisterLearn
                case 1704: {                                                                        
                    return "SELECT * FROM p1700RegisterLearn
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1700RegisterLearn
                case 1705: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1700RegisterLearn $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1700RegisterLearn T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1700RegisterLearn
                case 1706: {                                                                        
                    return "SELECT COUNT(1) FROM p1700RegisterLearn $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
