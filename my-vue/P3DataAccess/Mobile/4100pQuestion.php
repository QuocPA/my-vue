<?php                                                                                      
	class QuestionDA{				
		public function QuestionDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4100Question************************             
                // p4100Question(id,IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain)
                // Get all data from p4100Question
                case 4100: {                                                                        
                    return "SELECT * FROM p4100Question";
                }                                                                                  
                                                                                                   
                // Insert data to p4100Question
                case 4101: {                                                                        
                    return "INSERT INTO p4100Question(IdTypeAnswerResult,IdTopic,QuestionName,QuestionExplain)
                            VALUES('$param->IdTypeAnswerResult','$param->IdTopic','$param->QuestionName','$param->QuestionExplain')";                               
                }                                                                                  
                                                                                                   
                // Update data p4100Question
                case 4102: {
                    return "UPDATE p4100Question SET IdTypeAnswerResult='$param->IdTypeAnswerResult',IdTopic='$param->IdTopic',QuestionName='$param->QuestionName',QuestionExplain='$param->QuestionExplain'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4100Question
                case 4103: {                                                                        
                    return "DELETE FROM p4100Question
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4100Question
                case 4104: {                                                                        
                    return "SELECT * FROM p4100Question
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4100Question
                case 4105: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4100Question $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4100Question T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4100Question
                case 4106: {                                                                        
                    return "SELECT COUNT(1) FROM p4100Question $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
