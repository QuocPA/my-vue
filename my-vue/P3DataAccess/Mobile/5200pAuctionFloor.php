<?php                                                                                      
	class AuctionFloorDA{				
		public function AuctionFloorDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5200AuctionFloor************************             
                // p5200AuctionFloor(id,NumberCode,Name)
                // Get all data from p5200AuctionFloor
                case 5200: {                                                                        
                    return "SELECT * FROM p5200AuctionFloor";
                }                                                                                  
                                                                                                   
                // Insert data to p5200AuctionFloor
                case 5201: {                                                                        
                    return "INSERT INTO p5200AuctionFloor(NumberCode,Name)
                            VALUES('$param->NumberCode','$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p5200AuctionFloor
                case 5202: {
                    return "UPDATE p5200AuctionFloor SET NumberCode='$param->NumberCode',Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5200AuctionFloor
                case 5203: {                                                                        
                    return "DELETE FROM p5200AuctionFloor
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5200AuctionFloor
                case 5204: {                                                                        
                    return "SELECT * FROM p5200AuctionFloor
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5200AuctionFloor
                case 5205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5200AuctionFloor $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5200AuctionFloor T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5200AuctionFloor
                case 5206: {                                                                        
                    return "SELECT COUNT(1) FROM p5200AuctionFloor $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
