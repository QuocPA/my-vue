<?php                                                                                      
	class BorrowBookDA{				
		public function BorrowBookDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3100BorrowBook************************             
                // p3100BorrowBook(id,IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt)
                // Get all data from p3100BorrowBook
                case 3100: {                                                                        
                    return "SELECT * FROM p3100BorrowBook";
                }                                                                                  
                                                                                                   
                // Insert data to p3100BorrowBook
                case 3101: {                                                                        
                    return "INSERT INTO p3100BorrowBook(IdBook,IdStudent,IdEmployee,DateBegin,DateEnd,CreatedAt)
                            VALUES('$param->IdBook','$param->IdStudent','$param->IdEmployee','$param->DateBegin','$param->DateEnd','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p3100BorrowBook
                case 3102: {
                    return "UPDATE p3100BorrowBook SET IdBook='$param->IdBook',IdStudent='$param->IdStudent',IdEmployee='$param->IdEmployee',DateBegin='$param->DateBegin',DateEnd='$param->DateEnd',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3100BorrowBook
                case 3103: {                                                                        
                    return "DELETE FROM p3100BorrowBook
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3100BorrowBook
                case 3104: {                                                                        
                    return "SELECT * FROM p3100BorrowBook
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3100BorrowBook
                case 3105: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3100BorrowBook $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3100BorrowBook T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3100BorrowBook
                case 3106: {                                                                        
                    return "SELECT COUNT(1) FROM p3100BorrowBook $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
