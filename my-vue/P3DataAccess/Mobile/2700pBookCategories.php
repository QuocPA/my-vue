<?php                                                                                      
	class BookCategoriesDA{				
		public function BookCategoriesDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2700BookCategories************************             
                // p2700BookCategories(id,Name)
                // Get all data from p2700BookCategories
                case 2700: {                                                                        
                    return "SELECT * FROM p2700BookCategories";
                }                                                                                  
                                                                                                   
                // Insert data to p2700BookCategories
                case 2701: {                                                                        
                    return "INSERT INTO p2700BookCategories(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p2700BookCategories
                case 2702: {
                    return "UPDATE p2700BookCategories SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2700BookCategories
                case 2703: {                                                                        
                    return "DELETE FROM p2700BookCategories
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2700BookCategories
                case 2704: {                                                                        
                    return "SELECT * FROM p2700BookCategories
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2700BookCategories
                case 2705: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2700BookCategories $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2700BookCategories T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2700BookCategories
                case 2706: {                                                                        
                    return "SELECT COUNT(1) FROM p2700BookCategories $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
