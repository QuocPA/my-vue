<?php                                                                                      
	class DrivingTestDA{				
		public function DrivingTestDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p8200DrivingTest************************             
                // p8200DrivingTest(id,IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note)
                // Get all data from p8200DrivingTest
                case 8200: {                                                                        
                    return "SELECT * FROM p8200DrivingTest";
                }                                                                                  
                                                                                                   
                // Insert data to p8200DrivingTest
                case 8201: {                                                                        
                    return "INSERT INTO p8200DrivingTest(IdBranch,IdLesson,IdStudent,IdDateExam,IdStatusPass,DateRegister,Result,Note)
                            VALUES('$param->IdBranch','$param->IdLesson','$param->IdStudent','$param->IdDateExam','$param->IdStatusPass','$param->DateRegister','$param->Result','$param->Note')";                               
                }                                                                                  
                                                                                                   
                // Update data p8200DrivingTest
                case 8202: {
                    return "UPDATE p8200DrivingTest SET IdBranch='$param->IdBranch',IdLesson='$param->IdLesson',IdStudent='$param->IdStudent',IdDateExam='$param->IdDateExam',IdStatusPass='$param->IdStatusPass',DateRegister='$param->DateRegister',Result='$param->Result',Note='$param->Note'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p8200DrivingTest
                case 8203: {                                                                        
                    return "DELETE FROM p8200DrivingTest
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p8200DrivingTest
                case 8204: {                                                                        
                    return "SELECT * FROM p8200DrivingTest
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p8200DrivingTest
                case 8205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p8200DrivingTest $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p8200DrivingTest T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p8200DrivingTest
                case 8206: {                                                                        
                    return "SELECT COUNT(1) FROM p8200DrivingTest $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
