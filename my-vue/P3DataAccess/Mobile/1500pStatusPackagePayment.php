<?php                                                                                      
	class StatusPackagePaymentDA{				
		public function StatusPackagePaymentDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1500StatusPackagePayment************************             
                // p1500StatusPackagePayment(id,Name)
                // Get all data from p1500StatusPackagePayment
                case 1500: {                                                                        
                    return "SELECT * FROM p1500StatusPackagePayment";
                }                                                                                  
                                                                                                   
                // Insert data to p1500StatusPackagePayment
                case 1501: {                                                                        
                    return "INSERT INTO p1500StatusPackagePayment(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p1500StatusPackagePayment
                case 1502: {
                    return "UPDATE p1500StatusPackagePayment SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1500StatusPackagePayment
                case 1503: {                                                                        
                    return "DELETE FROM p1500StatusPackagePayment
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1500StatusPackagePayment
                case 1504: {                                                                        
                    return "SELECT * FROM p1500StatusPackagePayment
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1500StatusPackagePayment
                case 1505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1500StatusPackagePayment $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1500StatusPackagePayment T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1500StatusPackagePayment
                case 1506: {                                                                        
                    return "SELECT COUNT(1) FROM p1500StatusPackagePayment $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
