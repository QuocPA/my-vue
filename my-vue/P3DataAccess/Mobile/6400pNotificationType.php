<?php                                                                                      
	class NotificationTypeDA{				
		public function NotificationTypeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6400NotificationType************************             
                // p6400NotificationType(id,Name)
                // Get all data from p6400NotificationType
                case 6400: {                                                                        
                    return "SELECT * FROM p6400NotificationType";
                }                                                                                  
                                                                                                   
                // Insert data to p6400NotificationType
                case 6401: {                                                                        
                    return "INSERT INTO p6400NotificationType(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p6400NotificationType
                case 6402: {
                    return "UPDATE p6400NotificationType SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6400NotificationType
                case 6403: {                                                                        
                    return "DELETE FROM p6400NotificationType
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6400NotificationType
                case 6404: {                                                                        
                    return "SELECT * FROM p6400NotificationType
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6400NotificationType
                case 6405: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6400NotificationType $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6400NotificationType T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6400NotificationType
                case 6406: {                                                                        
                    return "SELECT COUNT(1) FROM p6400NotificationType $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
