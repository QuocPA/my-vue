<?php                                                                                      
	class StatusChangeCompannyDA{				
		public function StatusChangeCompannyDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4600StatusChangeCompanny************************             
                // p4600StatusChangeCompanny(id,Name)
                // Get all data from p4600StatusChangeCompanny
                case 4600: {                                                                        
                    return "SELECT * FROM p4600StatusChangeCompanny";
                }                                                                                  
                                                                                                   
                // Insert data to p4600StatusChangeCompanny
                case 4601: {                                                                        
                    return "INSERT INTO p4600StatusChangeCompanny(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p4600StatusChangeCompanny
                case 4602: {
                    return "UPDATE p4600StatusChangeCompanny SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4600StatusChangeCompanny
                case 4603: {                                                                        
                    return "DELETE FROM p4600StatusChangeCompanny
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4600StatusChangeCompanny
                case 4604: {                                                                        
                    return "SELECT * FROM p4600StatusChangeCompanny
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4600StatusChangeCompanny
                case 4605: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4600StatusChangeCompanny $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4600StatusChangeCompanny T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4600StatusChangeCompanny
                case 4606: {                                                                        
                    return "SELECT COUNT(1) FROM p4600StatusChangeCompanny $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
