<?php                                                                                      
	class CertificationDA{				
		public function CertificationDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2400Certification************************             
                // p2400Certification(id,Name,DateCertification)
                // Get all data from p2400Certification
                case 2400: {                                                                        
                    return "SELECT * FROM p2400Certification";
                }                                                                                  
                                                                                                   
                // Insert data to p2400Certification
                case 2401: {                                                                        
                    return "INSERT INTO p2400Certification(Name,DateCertification)
                            VALUES('$param->Name','$param->DateCertification')";                               
                }                                                                                  
                                                                                                   
                // Update data p2400Certification
                case 2402: {
                    return "UPDATE p2400Certification SET Name='$param->Name',DateCertification='$param->DateCertification'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2400Certification
                case 2403: {                                                                        
                    return "DELETE FROM p2400Certification
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2400Certification
                case 2404: {                                                                        
                    return "SELECT * FROM p2400Certification
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2400Certification
                case 2405: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2400Certification $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2400Certification T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2400Certification
                case 2406: {                                                                        
                    return "SELECT COUNT(1) FROM p2400Certification $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
