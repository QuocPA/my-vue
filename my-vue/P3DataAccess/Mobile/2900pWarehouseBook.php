<?php                                                                                      
	class WarehouseBookDA{				
		public function WarehouseBookDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2900WarehouseBook************************             
                // p2900WarehouseBook(id,IdBook,Total)
                // Get all data from p2900WarehouseBook
                case 2900: {                                                                        
                    return "SELECT * FROM p2900WarehouseBook";
                }                                                                                  
                                                                                                   
                // Insert data to p2900WarehouseBook
                case 2901: {                                                                        
                    return "INSERT INTO p2900WarehouseBook(IdBook,Total)
                            VALUES('$param->IdBook','$param->Total')";                               
                }                                                                                  
                                                                                                   
                // Update data p2900WarehouseBook
                case 2902: {
                    return "UPDATE p2900WarehouseBook SET IdBook='$param->IdBook',Total='$param->Total'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2900WarehouseBook
                case 2903: {                                                                        
                    return "DELETE FROM p2900WarehouseBook
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2900WarehouseBook
                case 2904: {                                                                        
                    return "SELECT * FROM p2900WarehouseBook
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2900WarehouseBook
                case 2905: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2900WarehouseBook $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2900WarehouseBook T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2900WarehouseBook
                case 2906: {                                                                        
                    return "SELECT COUNT(1) FROM p2900WarehouseBook $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
