<?php                                                                                      
	class StatusShakoDA{				
		public function StatusShakoDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4700StatusShako************************             
                // p4700StatusShako(id,Name)
                // Get all data from p4700StatusShako
                case 4700: {                                                                        
                    return "SELECT * FROM p4700StatusShako";
                }                                                                                  
                                                                                                   
                // Insert data to p4700StatusShako
                case 4701: {                                                                        
                    return "INSERT INTO p4700StatusShako(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p4700StatusShako
                case 4702: {
                    return "UPDATE p4700StatusShako SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4700StatusShako
                case 4703: {                                                                        
                    return "DELETE FROM p4700StatusShako
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4700StatusShako
                case 4704: {                                                                        
                    return "SELECT * FROM p4700StatusShako
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4700StatusShako
                case 4705: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4700StatusShako $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4700StatusShako T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4700StatusShako
                case 4706: {                                                                        
                    return "SELECT COUNT(1) FROM p4700StatusShako $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
