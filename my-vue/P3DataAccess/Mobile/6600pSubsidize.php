<?php                                                                                      
	class SubsidizeDA{				
		public function SubsidizeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6600Subsidize************************             
                // p6600Subsidize(id,Name,Money)
                // Get all data from p6600Subsidize
                case 6600: {                                                                        
                    return "SELECT * FROM p6600Subsidize";
                }                                                                                  
                                                                                                   
                // Insert data to p6600Subsidize
                case 6601: {                                                                        
                    return "INSERT INTO p6600Subsidize(Name,Money)
                            VALUES('$param->Name','$param->Money')";                               
                }                                                                                  
                                                                                                   
                // Update data p6600Subsidize
                case 6602: {
                    return "UPDATE p6600Subsidize SET Name='$param->Name',Money='$param->Money'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6600Subsidize
                case 6603: {                                                                        
                    return "DELETE FROM p6600Subsidize
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6600Subsidize
                case 6604: {                                                                        
                    return "SELECT * FROM p6600Subsidize
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6600Subsidize
                case 6605: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6600Subsidize $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6600Subsidize T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6600Subsidize
                case 6606: {                                                                        
                    return "SELECT COUNT(1) FROM p6600Subsidize $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
