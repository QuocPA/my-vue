<?php                                                                                      
	class VideoDA{				
		public function VideoDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3700Video************************             
                // p3700Video(id,IdVideoCategories,Name,Link)
                // Get all data from p3700Video
                case 3700: {                                                                        
                    return "SELECT * FROM p3700Video";
                }                                                                                  
                                                                                                   
                // Insert data to p3700Video
                case 3701: {                                                                        
                    return "INSERT INTO p3700Video(IdVideoCategories,Name,Link)
                            VALUES('$param->IdVideoCategories','$param->Name','$param->Link')";                               
                }                                                                                  
                                                                                                   
                // Update data p3700Video
                case 3702: {
                    return "UPDATE p3700Video SET IdVideoCategories='$param->IdVideoCategories',Name='$param->Name',Link='$param->Link'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3700Video
                case 3703: {                                                                        
                    return "DELETE FROM p3700Video
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3700Video
                case 3704: {                                                                        
                    return "SELECT * FROM p3700Video
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3700Video
                case 3705: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3700Video $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3700Video T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3700Video
                case 3706: {                                                                        
                    return "SELECT COUNT(1) FROM p3700Video $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
