<?php                                                                                      
	class CarManufacturerDA{				
		public function CarManufacturerDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5000CarManufacturer************************             
                // p5000CarManufacturer(id,Name,Logo)
                // Get all data from p5000CarManufacturer
                case 5000: {                                                                        
                    return "SELECT * FROM p5000CarManufacturer";
                }                                                                                  
                                                                                                   
                // Insert data to p5000CarManufacturer
                case 5001: {                                                                        
                    return "INSERT INTO p5000CarManufacturer(Name,Logo)
                            VALUES('$param->Name','$param->Logo')";                               
                }                                                                                  
                                                                                                   
                // Update data p5000CarManufacturer
                case 5002: {
                    return "UPDATE p5000CarManufacturer SET Name='$param->Name',Logo='$param->Logo'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5000CarManufacturer
                case 5003: {                                                                        
                    return "DELETE FROM p5000CarManufacturer
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5000CarManufacturer
                case 5004: {                                                                        
                    return "SELECT * FROM p5000CarManufacturer
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5000CarManufacturer
                case 5005: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5000CarManufacturer $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5000CarManufacturer T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5000CarManufacturer
                case 5006: {                                                                        
                    return "SELECT COUNT(1) FROM p5000CarManufacturer $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
