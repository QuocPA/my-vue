<?php                                                                                      
	class DayOffDA{				
		public function DayOffDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7300DayOff************************             
                // p7300DayOff(id,IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt)
                // Get all data from p7300DayOff
                case 7300: {                                                                        
                    return "SELECT * FROM p7300DayOff";
                }                                                                                  
                                                                                                   
                // Insert data to p7300DayOff
                case 7301: {                                                                        
                    return "INSERT INTO p7300DayOff(IdEmployee,IdStatusDayOf,IdStatusApproved,StartDate,EndDate,CreatedAt)
                            VALUES('$param->IdEmployee','$param->IdStatusDayOf','$param->IdStatusApproved','$param->StartDate','$param->EndDate','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p7300DayOff
                case 7302: {
                    return "UPDATE p7300DayOff SET IdEmployee='$param->IdEmployee',IdStatusDayOf='$param->IdStatusDayOf',IdStatusApproved='$param->IdStatusApproved',StartDate='$param->StartDate',EndDate='$param->EndDate',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7300DayOff
                case 7303: {                                                                        
                    return "DELETE FROM p7300DayOff
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7300DayOff
                case 7304: {                                                                        
                    return "SELECT * FROM p7300DayOff
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7300DayOff
                case 7305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7300DayOff $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7300DayOff T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7300DayOff
                case 7306: {                                                                        
                    return "SELECT COUNT(1) FROM p7300DayOff $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
