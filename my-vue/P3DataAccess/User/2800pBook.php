<?php                                                                                      
	class BookDA{				
		public function BookDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2800Book************************             
                // p2800Book(id,IdBookCategories,Name,Image,Author,Price,CreatedAt)
                // Get all data from p2800Book
                case 2800: {                                                                        
                    return "SELECT * FROM p2800Book";
                }                                                                                  
                                                                                                   
                // Insert data to p2800Book
                case 2801: {                                                                        
                    return "INSERT INTO p2800Book(IdBookCategories,Name,Image,Author,Price,CreatedAt)
                            VALUES('$param->IdBookCategories','$param->Name','$param->Image','$param->Author','$param->Price','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p2800Book
                case 2802: {
                    return "UPDATE p2800Book SET IdBookCategories='$param->IdBookCategories',Name='$param->Name',Image='$param->Image',Author='$param->Author',Price='$param->Price',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2800Book
                case 2803: {                                                                        
                    return "DELETE FROM p2800Book
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2800Book
                case 2804: {                                                                        
                    return "SELECT * FROM p2800Book
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2800Book
                case 2805: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2800Book $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2800Book T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2800Book
                case 2806: {                                                                        
                    return "SELECT COUNT(1) FROM p2800Book $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
