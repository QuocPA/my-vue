<?php                                                                                      
	class NoticeBoardDA{				
		public function NoticeBoardDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3300NoticeBoard************************             
                // p3300NoticeBoard(id,IdTypeNoticeBoard,Name,Image,Description,Position)
                // Get all data from p3300NoticeBoard
                case 3300: {                                                                        
                    return "SELECT * FROM p3300NoticeBoard";
                }                                                                                  
                                                                                                   
                // Insert data to p3300NoticeBoard
                case 3301: {                                                                        
                    return "INSERT INTO p3300NoticeBoard(IdTypeNoticeBoard,Name,Image,Description,Position)
                            VALUES('$param->IdTypeNoticeBoard','$param->Name','$param->Image','$param->Description','$param->Position')";                               
                }                                                                                  
                                                                                                   
                // Update data p3300NoticeBoard
                case 3302: {
                    return "UPDATE p3300NoticeBoard SET IdTypeNoticeBoard='$param->IdTypeNoticeBoard',Name='$param->Name',Image='$param->Image',Description='$param->Description',Position='$param->Position'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3300NoticeBoard
                case 3303: {                                                                        
                    return "DELETE FROM p3300NoticeBoard
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3300NoticeBoard
                case 3304: {                                                                        
                    return "SELECT * FROM p3300NoticeBoard
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3300NoticeBoard
                case 3305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3300NoticeBoard $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3300NoticeBoard T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3300NoticeBoard
                case 3306: {                                                                        
                    return "SELECT COUNT(1) FROM p3300NoticeBoard $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
