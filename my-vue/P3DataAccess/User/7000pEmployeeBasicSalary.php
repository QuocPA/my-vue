<?php                                                                                      
	class EmployeeBasicSalaryDA{				
		public function EmployeeBasicSalaryDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7000EmployeeBasicSalary************************             
                // p7000EmployeeBasicSalary(id,IdEmployee,IdBasicSalary,IdTypeEmployee)
                // Get all data from p7000EmployeeBasicSalary
                case 7000: {                                                                        
                    return "SELECT * FROM p7000EmployeeBasicSalary";
                }                                                                                  
                                                                                                   
                // Insert data to p7000EmployeeBasicSalary
                case 7001: {                                                                        
                    return "INSERT INTO p7000EmployeeBasicSalary(IdEmployee,IdBasicSalary,IdTypeEmployee)
                            VALUES('$param->IdEmployee','$param->IdBasicSalary','$param->IdTypeEmployee')";                               
                }                                                                                  
                                                                                                   
                // Update data p7000EmployeeBasicSalary
                case 7002: {
                    return "UPDATE p7000EmployeeBasicSalary SET IdEmployee='$param->IdEmployee',IdBasicSalary='$param->IdBasicSalary',IdTypeEmployee='$param->IdTypeEmployee'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7000EmployeeBasicSalary
                case 7003: {                                                                        
                    return "DELETE FROM p7000EmployeeBasicSalary
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7000EmployeeBasicSalary
                case 7004: {                                                                        
                    return "SELECT * FROM p7000EmployeeBasicSalary
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7000EmployeeBasicSalary
                case 7005: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7000EmployeeBasicSalary $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7000EmployeeBasicSalary T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7000EmployeeBasicSalary
                case 7006: {                                                                        
                    return "SELECT COUNT(1) FROM p7000EmployeeBasicSalary $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
