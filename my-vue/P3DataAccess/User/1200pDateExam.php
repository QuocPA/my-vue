<?php                                                                                      
	class DateExamDA{				
		public function DateExamDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1200DateExam************************             
                // p1200DateExam(id,IdLocationExam,StartDate)
                // Get all data from p1200DateExam
                case 1200: {                                                                        
                    return "SELECT * FROM p1200DateExam";
                }                                                                                  
                                                                                                   
                // Insert data to p1200DateExam
                case 1201: {                                                                        
                    return "INSERT INTO p1200DateExam(IdLocationExam,StartDate)
                            VALUES('$param->IdLocationExam','$param->StartDate')";                               
                }                                                                                  
                                                                                                   
                // Update data p1200DateExam
                case 1202: {
                    return "UPDATE p1200DateExam SET IdLocationExam='$param->IdLocationExam',StartDate='$param->StartDate'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1200DateExam
                case 1203: {                                                                        
                    return "DELETE FROM p1200DateExam
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1200DateExam
                case 1204: {                                                                        
                    return "SELECT * FROM p1200DateExam
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1200DateExam
                case 1205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1200DateExam $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1200DateExam T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1200DateExam
                case 1206: {                                                                        
                    return "SELECT COUNT(1) FROM p1200DateExam $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
