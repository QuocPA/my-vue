<?php                                                                                      
	class EmployeeOvertimeDA{				
		public function EmployeeOvertimeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6900EmployeeOvertime************************             
                // p6900EmployeeOvertime(id,IdEmployee,NumberHour,CreatedAt)
                // Get all data from p6900EmployeeOvertime
                case 6900: {                                                                        
                    return "SELECT * FROM p6900EmployeeOvertime";
                }                                                                                  
                                                                                                   
                // Insert data to p6900EmployeeOvertime
                case 6901: {                                                                        
                    return "INSERT INTO p6900EmployeeOvertime(IdEmployee,NumberHour,CreatedAt)
                            VALUES('$param->IdEmployee','$param->NumberHour','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p6900EmployeeOvertime
                case 6902: {
                    return "UPDATE p6900EmployeeOvertime SET IdEmployee='$param->IdEmployee',NumberHour='$param->NumberHour',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6900EmployeeOvertime
                case 6903: {                                                                        
                    return "DELETE FROM p6900EmployeeOvertime
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6900EmployeeOvertime
                case 6904: {                                                                        
                    return "SELECT * FROM p6900EmployeeOvertime
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6900EmployeeOvertime
                case 6905: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6900EmployeeOvertime $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6900EmployeeOvertime T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6900EmployeeOvertime
                case 6906: {                                                                        
                    return "SELECT COUNT(1) FROM p6900EmployeeOvertime $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
