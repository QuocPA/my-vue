<?php                                                                                      
	class SexDA{				
		public function SexDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p8000Sex************************             
                // p8000Sex(id,Name)
                // Get all data from p8000Sex
                case 8000: {                                                                        
                    return "SELECT * FROM p8000Sex";
                }                                                                                  
                                                                                                   
                // Insert data to p8000Sex
                case 8001: {                                                                        
                    return "INSERT INTO p8000Sex(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p8000Sex
                case 8002: {
                    return "UPDATE p8000Sex SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p8000Sex
                case 8003: {                                                                        
                    return "DELETE FROM p8000Sex
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p8000Sex
                case 8004: {                                                                        
                    return "SELECT * FROM p8000Sex
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p8000Sex
                case 8005: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p8000Sex $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p8000Sex T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p8000Sex
                case 8006: {                                                                        
                    return "SELECT COUNT(1) FROM p8000Sex $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
