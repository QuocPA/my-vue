<?php                                                                                      
	class EmployeeRewardDA{				
		public function EmployeeRewardDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6700EmployeeReward************************             
                // p6700EmployeeReward(id,IdEmployee,Name,Money,CreatedAt)
                // Get all data from p6700EmployeeReward
                case 6700: {                                                                        
                    return "SELECT * FROM p6700EmployeeReward";
                }                                                                                  
                                                                                                   
                // Insert data to p6700EmployeeReward
                case 6701: {                                                                        
                    return "INSERT INTO p6700EmployeeReward(IdEmployee,Name,Money,CreatedAt)
                            VALUES('$param->IdEmployee','$param->Name','$param->Money','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p6700EmployeeReward
                case 6702: {
                    return "UPDATE p6700EmployeeReward SET IdEmployee='$param->IdEmployee',Name='$param->Name',Money='$param->Money',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6700EmployeeReward
                case 6703: {                                                                        
                    return "DELETE FROM p6700EmployeeReward
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6700EmployeeReward
                case 6704: {                                                                        
                    return "SELECT * FROM p6700EmployeeReward
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6700EmployeeReward
                case 6705: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6700EmployeeReward $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6700EmployeeReward T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6700EmployeeReward
                case 6706: {                                                                        
                    return "SELECT COUNT(1) FROM p6700EmployeeReward $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
