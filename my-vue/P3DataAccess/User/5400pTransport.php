<?php                                                                                      
	class TransportDA{				
		public function TransportDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5400Transport************************             
                // p5400Transport(id,Name)
                // Get all data from p5400Transport
                case 5400: {                                                                        
                    return "SELECT * FROM p5400Transport";
                }                                                                                  
                                                                                                   
                // Insert data to p5400Transport
                case 5401: {                                                                        
                    return "INSERT INTO p5400Transport(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p5400Transport
                case 5402: {
                    return "UPDATE p5400Transport SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5400Transport
                case 5403: {                                                                        
                    return "DELETE FROM p5400Transport
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5400Transport
                case 5404: {                                                                        
                    return "SELECT * FROM p5400Transport
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5400Transport
                case 5405: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5400Transport $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5400Transport T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5400Transport
                case 5406: {                                                                        
                    return "SELECT COUNT(1) FROM p5400Transport $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
