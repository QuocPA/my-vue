<?php                                                                                      
	class TypeAnswerResultDA{				
		public function TypeAnswerResultDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4000TypeAnswerResult************************             
                // p4000TypeAnswerResult(id,Name)
                // Get all data from p4000TypeAnswerResult
                case 4000: {                                                                        
                    return "SELECT * FROM p4000TypeAnswerResult";
                }                                                                                  
                                                                                                   
                // Insert data to p4000TypeAnswerResult
                case 4001: {                                                                        
                    return "INSERT INTO p4000TypeAnswerResult(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p4000TypeAnswerResult
                case 4002: {
                    return "UPDATE p4000TypeAnswerResult SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4000TypeAnswerResult
                case 4003: {                                                                        
                    return "DELETE FROM p4000TypeAnswerResult
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4000TypeAnswerResult
                case 4004: {                                                                        
                    return "SELECT * FROM p4000TypeAnswerResult
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4000TypeAnswerResult
                case 4005: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4000TypeAnswerResult $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4000TypeAnswerResult T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4000TypeAnswerResult
                case 4006: {                                                                        
                    return "SELECT COUNT(1) FROM p4000TypeAnswerResult $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
