<?php                                                                                      
	class StatusPassDA{				
		public function StatusPassDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p8100StatusPass************************             
                // p8100StatusPass(id,Name)
                // Get all data from p8100StatusPass
                case 8100: {                                                                        
                    return "SELECT * FROM p8100StatusPass";
                }                                                                                  
                                                                                                   
                // Insert data to p8100StatusPass
                case 8101: {                                                                        
                    return "INSERT INTO p8100StatusPass(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p8100StatusPass
                case 8102: {
                    return "UPDATE p8100StatusPass SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p8100StatusPass
                case 8103: {                                                                        
                    return "DELETE FROM p8100StatusPass
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p8100StatusPass
                case 8104: {                                                                        
                    return "SELECT * FROM p8100StatusPass
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p8100StatusPass
                case 8105: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p8100StatusPass $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p8100StatusPass T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p8100StatusPass
                case 8106: {                                                                        
                    return "SELECT COUNT(1) FROM p8100StatusPass $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
