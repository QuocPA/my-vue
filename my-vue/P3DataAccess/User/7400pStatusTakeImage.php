<?php                                                                                      
	class StatusTakeImageDA{				
		public function StatusTakeImageDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7400StatusTakeImage************************             
                // p7400StatusTakeImage(id,Name)
                // Get all data from p7400StatusTakeImage
                case 7400: {                                                                        
                    return "SELECT * FROM p7400StatusTakeImage";
                }                                                                                  
                                                                                                   
                // Insert data to p7400StatusTakeImage
                case 7401: {                                                                        
                    return "INSERT INTO p7400StatusTakeImage(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p7400StatusTakeImage
                case 7402: {
                    return "UPDATE p7400StatusTakeImage SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7400StatusTakeImage
                case 7403: {                                                                        
                    return "DELETE FROM p7400StatusTakeImage
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7400StatusTakeImage
                case 7404: {                                                                        
                    return "SELECT * FROM p7400StatusTakeImage
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7400StatusTakeImage
                case 7405: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7400StatusTakeImage $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7400StatusTakeImage T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7400StatusTakeImage
                case 7406: {                                                                        
                    return "SELECT COUNT(1) FROM p7400StatusTakeImage $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
