<?php                                                                                      
	class StatusPaymentDA{				
		public function StatusPaymentDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7800StatusPayment************************             
                // p7800StatusPayment(id,Name)
                // Get all data from p7800StatusPayment
                case 7800: {                                                                        
                    return "SELECT * FROM p7800StatusPayment";
                }                                                                                  
                                                                                                   
                // Insert data to p7800StatusPayment
                case 7801: {                                                                        
                    return "INSERT INTO p7800StatusPayment(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p7800StatusPayment
                case 7802: {
                    return "UPDATE p7800StatusPayment SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7800StatusPayment
                case 7803: {                                                                        
                    return "DELETE FROM p7800StatusPayment
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7800StatusPayment
                case 7804: {                                                                        
                    return "SELECT * FROM p7800StatusPayment
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7800StatusPayment
                case 7805: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7800StatusPayment $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7800StatusPayment T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7800StatusPayment
                case 7806: {                                                                        
                    return "SELECT COUNT(1) FROM p7800StatusPayment $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
