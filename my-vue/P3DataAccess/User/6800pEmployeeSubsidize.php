<?php                                                                                      
	class EmployeeSubsidizeDA{				
		public function EmployeeSubsidizeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6800EmployeeSubsidize************************             
                // p6800EmployeeSubsidize(id,IdEmployee,IdSubsidize,StartDate,EndDate)
                // Get all data from p6800EmployeeSubsidize
                case 6800: {                                                                        
                    return "SELECT * FROM p6800EmployeeSubsidize";
                }                                                                                  
                                                                                                   
                // Insert data to p6800EmployeeSubsidize
                case 6801: {                                                                        
                    return "INSERT INTO p6800EmployeeSubsidize(IdEmployee,IdSubsidize,StartDate,EndDate)
                            VALUES('$param->IdEmployee','$param->IdSubsidize','$param->StartDate','$param->EndDate')";                               
                }                                                                                  
                                                                                                   
                // Update data p6800EmployeeSubsidize
                case 6802: {
                    return "UPDATE p6800EmployeeSubsidize SET IdEmployee='$param->IdEmployee',IdSubsidize='$param->IdSubsidize',StartDate='$param->StartDate',EndDate='$param->EndDate'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6800EmployeeSubsidize
                case 6803: {                                                                        
                    return "DELETE FROM p6800EmployeeSubsidize
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6800EmployeeSubsidize
                case 6804: {                                                                        
                    return "SELECT * FROM p6800EmployeeSubsidize
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6800EmployeeSubsidize
                case 6805: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6800EmployeeSubsidize $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6800EmployeeSubsidize T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6800EmployeeSubsidize
                case 6806: {                                                                        
                    return "SELECT COUNT(1) FROM p6800EmployeeSubsidize $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
