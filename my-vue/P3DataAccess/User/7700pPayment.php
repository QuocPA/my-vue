<?php                                                                                      
	class PaymentDA{				
		public function PaymentDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p7700Payment************************             
                // p7700Payment(id,Name)
                // Get all data from p7700Payment
                case 7700: {                                                                        
                    return "SELECT * FROM p7700Payment";
                }                                                                                  
                                                                                                   
                // Insert data to p7700Payment
                case 7701: {                                                                        
                    return "INSERT INTO p7700Payment(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p7700Payment
                case 7702: {
                    return "UPDATE p7700Payment SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p7700Payment
                case 7703: {                                                                        
                    return "DELETE FROM p7700Payment
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p7700Payment
                case 7704: {                                                                        
                    return "SELECT * FROM p7700Payment
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p7700Payment
                case 7705: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p7700Payment $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p7700Payment T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p7700Payment
                case 7706: {                                                                        
                    return "SELECT COUNT(1) FROM p7700Payment $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
