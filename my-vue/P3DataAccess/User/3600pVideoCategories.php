<?php                                                                                      
	class VideoCategoriesDA{				
		public function VideoCategoriesDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3600VideoCategories************************             
                // p3600VideoCategories(id,Name,Image)
                // Get all data from p3600VideoCategories
                case 3600: {                                                                        
                    return "SELECT * FROM p3600VideoCategories";
                }                                                                                  
                                                                                                   
                // Insert data to p3600VideoCategories
                case 3601: {                                                                        
                    return "INSERT INTO p3600VideoCategories(Name,Image)
                            VALUES('$param->Name','$param->Image')";                               
                }                                                                                  
                                                                                                   
                // Update data p3600VideoCategories
                case 3602: {
                    return "UPDATE p3600VideoCategories SET Name='$param->Name',Image='$param->Image'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3600VideoCategories
                case 3603: {                                                                        
                    return "DELETE FROM p3600VideoCategories
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3600VideoCategories
                case 3604: {                                                                        
                    return "SELECT * FROM p3600VideoCategories
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3600VideoCategories
                case 3605: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3600VideoCategories $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3600VideoCategories T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3600VideoCategories
                case 3606: {                                                                        
                    return "SELECT COUNT(1) FROM p3600VideoCategories $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
