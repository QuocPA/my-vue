<?php                                                                                      
	class CostCategoriesDA{				
		public function CostCategoriesDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4400CostCategories************************             
                // p4400CostCategories(id,Name)
                // Get all data from p4400CostCategories
                case 4400: {                                                                        
                    return "SELECT * FROM p4400CostCategories";
                }                                                                                  
                                                                                                   
                // Insert data to p4400CostCategories
                case 4401: {                                                                        
                    return "INSERT INTO p4400CostCategories(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p4400CostCategories
                case 4402: {
                    return "UPDATE p4400CostCategories SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4400CostCategories
                case 4403: {                                                                        
                    return "DELETE FROM p4400CostCategories
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4400CostCategories
                case 4404: {                                                                        
                    return "SELECT * FROM p4400CostCategories
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4400CostCategories
                case 4405: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4400CostCategories $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4400CostCategories T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4400CostCategories
                case 4406: {                                                                        
                    return "SELECT COUNT(1) FROM p4400CostCategories $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
