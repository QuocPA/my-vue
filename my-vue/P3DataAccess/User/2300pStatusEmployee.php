<?php                                                                                      
	class StatusEmployeeDA{				
		public function StatusEmployeeDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2300StatusEmployee************************             
                // p2300StatusEmployee(id,Name)
                // Get all data from p2300StatusEmployee
                case 2300: {                                                                        
                    return "SELECT * FROM p2300StatusEmployee";
                }                                                                                  
                                                                                                   
                // Insert data to p2300StatusEmployee
                case 2301: {                                                                        
                    return "INSERT INTO p2300StatusEmployee(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p2300StatusEmployee
                case 2302: {
                    return "UPDATE p2300StatusEmployee SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2300StatusEmployee
                case 2303: {                                                                        
                    return "DELETE FROM p2300StatusEmployee
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2300StatusEmployee
                case 2304: {                                                                        
                    return "SELECT * FROM p2300StatusEmployee
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2300StatusEmployee
                case 2305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2300StatusEmployee $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2300StatusEmployee T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2300StatusEmployee
                case 2306: {                                                                        
                    return "SELECT COUNT(1) FROM p2300StatusEmployee $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
