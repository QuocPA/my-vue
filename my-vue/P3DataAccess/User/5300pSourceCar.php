<?php                                                                                      
	class SourceCarDA{				
		public function SourceCarDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5300SourceCar************************             
                // p5300SourceCar(id,Name)
                // Get all data from p5300SourceCar
                case 5300: {                                                                        
                    return "SELECT * FROM p5300SourceCar";
                }                                                                                  
                                                                                                   
                // Insert data to p5300SourceCar
                case 5301: {                                                                        
                    return "INSERT INTO p5300SourceCar(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p5300SourceCar
                case 5302: {
                    return "UPDATE p5300SourceCar SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5300SourceCar
                case 5303: {                                                                        
                    return "DELETE FROM p5300SourceCar
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5300SourceCar
                case 5304: {                                                                        
                    return "SELECT * FROM p5300SourceCar
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5300SourceCar
                case 5305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5300SourceCar $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5300SourceCar T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5300SourceCar
                case 5306: {                                                                        
                    return "SELECT COUNT(1) FROM p5300SourceCar $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
