<?php                                                                                      
	class StatusStudentDA{				
		public function StatusStudentDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p2000StatusStudent************************             
                // p2000StatusStudent(id,Name)
                // Get all data from p2000StatusStudent
                case 2000: {                                                                        
                    return "SELECT * FROM p2000StatusStudent";
                }                                                                                  
                                                                                                   
                // Insert data to p2000StatusStudent
                case 2001: {                                                                        
                    return "INSERT INTO p2000StatusStudent(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p2000StatusStudent
                case 2002: {
                    return "UPDATE p2000StatusStudent SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p2000StatusStudent
                case 2003: {                                                                        
                    return "DELETE FROM p2000StatusStudent
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p2000StatusStudent
                case 2004: {                                                                        
                    return "SELECT * FROM p2000StatusStudent
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p2000StatusStudent
                case 2005: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p2000StatusStudent $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p2000StatusStudent T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p2000StatusStudent
                case 2006: {                                                                        
                    return "SELECT COUNT(1) FROM p2000StatusStudent $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
