<?php                                                                                      
	class InputCarDA{				
		public function InputCarDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5800InputCar************************             
                // p5800InputCar(id,IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport)
                // Get all data from p5800InputCar
                case 5800: {                                                                        
                    return "SELECT * FROM p5800InputCar";
                }                                                                                  
                                                                                                   
                // Insert data to p5800InputCar
                case 5801: {                                                                        
                    return "INSERT INTO p5800InputCar(IdCar,IdAuctionFloor,IdSourceCar,DateAuction,DateReceiveCar,IdTransport,NameTransport)
                            VALUES('$param->IdCar','$param->IdAuctionFloor','$param->IdSourceCar','$param->DateAuction','$param->DateReceiveCar','$param->IdTransport','$param->NameTransport')";                               
                }                                                                                  
                                                                                                   
                // Update data p5800InputCar
                case 5802: {
                    return "UPDATE p5800InputCar SET IdCar='$param->IdCar',IdAuctionFloor='$param->IdAuctionFloor',IdSourceCar='$param->IdSourceCar',DateAuction='$param->DateAuction',DateReceiveCar='$param->DateReceiveCar',IdTransport='$param->IdTransport',NameTransport='$param->NameTransport'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5800InputCar
                case 5803: {                                                                        
                    return "DELETE FROM p5800InputCar
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5800InputCar
                case 5804: {                                                                        
                    return "SELECT * FROM p5800InputCar
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5800InputCar
                case 5805: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5800InputCar $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5800InputCar T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5800InputCar
                case 5806: {                                                                        
                    return "SELECT COUNT(1) FROM p5800InputCar $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
