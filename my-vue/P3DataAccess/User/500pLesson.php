<?php                                                                                      
	class LessonDA{				
		public function LessonDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p500Lesson************************             
                // p500Lesson(id,Name)
                // Get all data from p500Lesson
                case 500: {                                                                        
                    return "SELECT * FROM p500Lesson";
                }                                                                                  
                                                                                                   
                // Insert data to p500Lesson
                case 501: {                                                                        
                    return "INSERT INTO p500Lesson(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p500Lesson
                case 502: {
                    return "UPDATE p500Lesson SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p500Lesson
                case 503: {                                                                        
                    return "DELETE FROM p500Lesson
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p500Lesson
                case 504: {                                                                        
                    return "SELECT * FROM p500Lesson
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p500Lesson
                case 505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p500Lesson $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p500Lesson T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p500Lesson
                case 506: {                                                                        
                    return "SELECT COUNT(1) FROM p500Lesson $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
