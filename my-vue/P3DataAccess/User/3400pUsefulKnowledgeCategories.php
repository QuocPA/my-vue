<?php                                                                                      
	class UsefulKnowledgeCategoriesDA{				
		public function UsefulKnowledgeCategoriesDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3400UsefulKnowledgeCategories************************             
                // p3400UsefulKnowledgeCategories(id,Name,Image)
                // Get all data from p3400UsefulKnowledgeCategories
                case 3400: {                                                                        
                    return "SELECT * FROM p3400UsefulKnowledgeCategories";
                }                                                                                  
                                                                                                   
                // Insert data to p3400UsefulKnowledgeCategories
                case 3401: {                                                                        
                    return "INSERT INTO p3400UsefulKnowledgeCategories(Name,Image)
                            VALUES('$param->Name','$param->Image')";                               
                }                                                                                  
                                                                                                   
                // Update data p3400UsefulKnowledgeCategories
                case 3402: {
                    return "UPDATE p3400UsefulKnowledgeCategories SET Name='$param->Name',Image='$param->Image'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3400UsefulKnowledgeCategories
                case 3403: {                                                                        
                    return "DELETE FROM p3400UsefulKnowledgeCategories
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3400UsefulKnowledgeCategories
                case 3404: {                                                                        
                    return "SELECT * FROM p3400UsefulKnowledgeCategories
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3400UsefulKnowledgeCategories
                case 3405: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3400UsefulKnowledgeCategories $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3400UsefulKnowledgeCategories T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3400UsefulKnowledgeCategories
                case 3406: {                                                                        
                    return "SELECT COUNT(1) FROM p3400UsefulKnowledgeCategories $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
