<?php                                                                                      
	class BasicSalaryDA{				
		public function BasicSalaryDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p6500BasicSalary************************             
                // p6500BasicSalary(id,Name,MoneyBasicInMonth,MoneyInHour)
                // Get all data from p6500BasicSalary
                case 6500: {                                                                        
                    return "SELECT * FROM p6500BasicSalary";
                }                                                                                  
                                                                                                   
                // Insert data to p6500BasicSalary
                case 6501: {                                                                        
                    return "INSERT INTO p6500BasicSalary(Name,MoneyBasicInMonth,MoneyInHour)
                            VALUES('$param->Name','$param->MoneyBasicInMonth','$param->MoneyInHour')";                               
                }                                                                                  
                                                                                                   
                // Update data p6500BasicSalary
                case 6502: {
                    return "UPDATE p6500BasicSalary SET Name='$param->Name',MoneyBasicInMonth='$param->MoneyBasicInMonth',MoneyInHour='$param->MoneyInHour'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p6500BasicSalary
                case 6503: {                                                                        
                    return "DELETE FROM p6500BasicSalary
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p6500BasicSalary
                case 6504: {                                                                        
                    return "SELECT * FROM p6500BasicSalary
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p6500BasicSalary
                case 6505: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p6500BasicSalary $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p6500BasicSalary T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p6500BasicSalary
                case 6506: {                                                                        
                    return "SELECT COUNT(1) FROM p6500BasicSalary $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
