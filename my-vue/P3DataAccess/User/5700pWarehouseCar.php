<?php                                                                                      
	class WarehouseCarDA{				
		public function WarehouseCarDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5700WarehouseCar************************             
                // p5700WarehouseCar(id,IdCar,Total,CreatedAt)
                // Get all data from p5700WarehouseCar
                case 5700: {                                                                        
                    return "SELECT * FROM p5700WarehouseCar";
                }                                                                                  
                                                                                                   
                // Insert data to p5700WarehouseCar
                case 5701: {                                                                        
                    return "INSERT INTO p5700WarehouseCar(IdCar,Total,CreatedAt)
                            VALUES('$param->IdCar','$param->Total','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p5700WarehouseCar
                case 5702: {
                    return "UPDATE p5700WarehouseCar SET IdCar='$param->IdCar',Total='$param->Total',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5700WarehouseCar
                case 5703: {                                                                        
                    return "DELETE FROM p5700WarehouseCar
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5700WarehouseCar
                case 5704: {                                                                        
                    return "SELECT * FROM p5700WarehouseCar
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5700WarehouseCar
                case 5705: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5700WarehouseCar $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5700WarehouseCar T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5700WarehouseCar
                case 5706: {                                                                        
                    return "SELECT COUNT(1) FROM p5700WarehouseCar $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
