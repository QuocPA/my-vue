<?php                                                                                      
	class HistorySentaDA{				
		public function HistorySentaDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4300HistorySenta************************             
                // p4300HistorySenta(id,IdStudent,CreatedAt)
                // Get all data from p4300HistorySenta
                case 4300: {                                                                        
                    return "SELECT * FROM p4300HistorySenta";
                }                                                                                  
                                                                                                   
                // Insert data to p4300HistorySenta
                case 4301: {                                                                        
                    return "INSERT INTO p4300HistorySenta(IdStudent,CreatedAt)
                            VALUES('$param->IdStudent','$param->CreatedAt')";                               
                }                                                                                  
                                                                                                   
                // Update data p4300HistorySenta
                case 4302: {
                    return "UPDATE p4300HistorySenta SET IdStudent='$param->IdStudent',CreatedAt='$param->CreatedAt'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4300HistorySenta
                case 4303: {                                                                        
                    return "DELETE FROM p4300HistorySenta
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4300HistorySenta
                case 4304: {                                                                        
                    return "SELECT * FROM p4300HistorySenta
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4300HistorySenta
                case 4305: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4300HistorySenta $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4300HistorySenta T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4300HistorySenta
                case 4306: {                                                                        
                    return "SELECT COUNT(1) FROM p4300HistorySenta $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
