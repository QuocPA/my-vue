<?php                                                                                      
	class CarDA{				
		public function CarDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p5100Car************************             
                // p5100Car(id,IdCarCategories,IdCarManufacturer,NameCar,Color,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost)
                // Get all data from p5100Car
                case 5100: {                                                                        
                    return "SELECT * FROM p5100Car";
                }                                                                                  
                                                                                                   
                // Insert data to p5100Car
                case 5101: {                                                                        
                    return "INSERT INTO p5100Car(IdCarCategories,IdCarManufacturer,NameCar,Color,ChassisNumber,YearOfManufacture,Kilometer,Shaken,InputPrice,OutputPricePlan,IdStatusVehicleRegistration,IdStatusShako,ExpirationChangeCompany,IdStatusChangeCompanny,IdStatusTakeImage,IdStatusPost)
                            VALUES('$param->IdCarCategories','$param->IdCarManufacturer','$param->NameCar','$param->Color','$param->ChassisNumber','$param->YearOfManufacture','$param->Kilometer','$param->Shaken','$param->InputPrice','$param->OutputPricePlan','$param->IdStatusVehicleRegistration','$param->IdStatusShako','$param->ExpirationChangeCompany','$param->IdStatusChangeCompanny','$param->IdStatusTakeImage','$param->IdStatusPost')";                               
                }                                                                                  
                                                                                                   
                // Update data p5100Car
                case 5102: {
                    return "UPDATE p5100Car SET IdCarCategories='$param->IdCarCategories',IdCarManufacturer='$param->IdCarManufacturer',NameCar='$param->NameCar',Color='$param->Color',ChassisNumber='$param->ChassisNumber',YearOfManufacture='$param->YearOfManufacture',Kilometer='$param->Kilometer',Shaken='$param->Shaken',InputPrice='$param->InputPrice',OutputPricePlan='$param->OutputPricePlan',IdStatusVehicleRegistration='$param->IdStatusVehicleRegistration',IdStatusShako='$param->IdStatusShako',ExpirationChangeCompany='$param->ExpirationChangeCompany',IdStatusChangeCompanny='$param->IdStatusChangeCompanny',IdStatusTakeImage='$param->IdStatusTakeImage',IdStatusPost='$param->IdStatusPost'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p5100Car
                case 5103: {                                                                        
                    return "DELETE FROM p5100Car
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p5100Car
                case 5104: {                                                                        
                    return "SELECT * FROM p5100Car
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p5100Car
                case 5105: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p5100Car $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p5100Car T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p5100Car
                case 5106: {                                                                        
                    return "SELECT COUNT(1) FROM p5100Car $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
