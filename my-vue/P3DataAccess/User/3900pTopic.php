<?php                                                                                      
	class TopicDA{				
		public function TopicDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p3900Topic************************             
                // p3900Topic(id,IdTypeQuestion,Name)
                // Get all data from p3900Topic
                case 3900: {                                                                        
                    return "SELECT * FROM p3900Topic";
                }                                                                                  
                                                                                                   
                // Insert data to p3900Topic
                case 3901: {                                                                        
                    return "INSERT INTO p3900Topic(IdTypeQuestion,Name)
                            VALUES('$param->IdTypeQuestion','$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p3900Topic
                case 3902: {
                    return "UPDATE p3900Topic SET IdTypeQuestion='$param->IdTypeQuestion',Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p3900Topic
                case 3903: {                                                                        
                    return "DELETE FROM p3900Topic
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p3900Topic
                case 3904: {                                                                        
                    return "SELECT * FROM p3900Topic
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p3900Topic
                case 3905: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p3900Topic $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p3900Topic T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p3900Topic
                case 3906: {                                                                        
                    return "SELECT COUNT(1) FROM p3900Topic $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
