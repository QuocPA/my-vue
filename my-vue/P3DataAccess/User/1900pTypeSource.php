<?php                                                                                      
	class TypeSourceDA{				
		public function TypeSourceDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p1900TypeSource************************             
                // p1900TypeSource(id,Name)
                // Get all data from p1900TypeSource
                case 1900: {                                                                        
                    return "SELECT * FROM p1900TypeSource";
                }                                                                                  
                                                                                                   
                // Insert data to p1900TypeSource
                case 1901: {                                                                        
                    return "INSERT INTO p1900TypeSource(Name)
                            VALUES('$param->Name')";                               
                }                                                                                  
                                                                                                   
                // Update data p1900TypeSource
                case 1902: {
                    return "UPDATE p1900TypeSource SET Name='$param->Name'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p1900TypeSource
                case 1903: {                                                                        
                    return "DELETE FROM p1900TypeSource
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p1900TypeSource
                case 1904: {                                                                        
                    return "SELECT * FROM p1900TypeSource
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p1900TypeSource
                case 1905: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p1900TypeSource $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p1900TypeSource T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p1900TypeSource
                case 1906: {                                                                        
                    return "SELECT COUNT(1) FROM p1900TypeSource $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
