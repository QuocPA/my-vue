<?php                                                                                      
	class QuestionHistoryDA{				
		public function QuestionHistoryDataAccess($what, $param){ 			
            switch ($what) {                                                                 
                //******************p4200QuestionHistory************************             
                // p4200QuestionHistory(id,IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent)
                // Get all data from p4200QuestionHistory
                case 4200: {                                                                        
                    return "SELECT * FROM p4200QuestionHistory";
                }                                                                                  
                                                                                                   
                // Insert data to p4200QuestionHistory
                case 4201: {                                                                        
                    return "INSERT INTO p4200QuestionHistory(IdQuestion,IdTypeAnswerResult,IdTopic,IdStudent)
                            VALUES('$param->IdQuestion','$param->IdTypeAnswerResult','$param->IdTopic','$param->IdStudent')";                               
                }                                                                                  
                                                                                                   
                // Update data p4200QuestionHistory
                case 4202: {
                    return "UPDATE p4200QuestionHistory SET IdQuestion='$param->IdQuestion',IdTypeAnswerResult='$param->IdTypeAnswerResult',IdTopic='$param->IdTopic',IdStudent='$param->IdStudent'
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Delete data of p4200QuestionHistory
                case 4203: {                                                                        
                    return "DELETE FROM p4200QuestionHistory
                            WHERE id IN($param->listid)";                                           
                }                                                                                  
                                                                                                   
                // Find data with id p4200QuestionHistory
                case 4204: {                                                                        
                    return "SELECT * FROM p4200QuestionHistory
                            WHERE id='$param->id'";                                                 
                }                                                                                  
                                                                                                   
                // Select with pagination(offset, number-item-in-page) p4200QuestionHistory
                case 4205: {                                                                        
                    return "SELECT *                                                                              
                            FROM (SELECT id FROM p4200QuestionHistory $param->condition ORDER BY id LIMIT $param->offset, $param->limit) T1     
                            INNER JOIN p4200QuestionHistory T2 ON T1.id = T2.id";                                     
                }                                                                                                 
                                                                                                   
                // Count number item of p4200QuestionHistory
                case 4206: {                                                                        
                    return "SELECT COUNT(1) FROM p4200QuestionHistory $param->condition";
                }                                                                                  
            }                                                                                      
		}                                                                                      
	}                                                                                      
?>                                                                                         
